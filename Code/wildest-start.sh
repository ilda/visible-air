#!/bin/bash

LOGIN="wild"
# PROGHOME="/media/ssd/Demos/VisibleAir"
PROGHOME="/home/wild/Demos/VisibleAir"
EXEC="VisibleAir.x86_64"
EXECW="Build/BuildServeurWindows/VisibleAir.exe"
LIP="157"
XINEMARA="" #-xinebezel"
REST="" #-xinebezel"

accum_rest=0

for i in "$@"
do
case $i in
    -n=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
    	 NAME="${i#*=}"
      fi
    ;;
    -e=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
    	   EXEC="${i#*=}"
      fi
    ;;
    -ip=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
        LIP="${i#*=}"
      fi
    ;;
    -l=*)
      if [ $accum_rest == 1 ]; 
        then REST="$REST $i";
      else
        LOGIN="${i#*=}"
      fi
    ;;
    -xine)
      REST="$REST -xine";
      XINEMARA=-xine
    ;;
    -xinebezel)
      REST="$REST -xinebezel";
      XINEMARA=-xinebezel
    ;;
    --)
  		accum_rest=1
    ;;
    *)
		if [ $accum_rest == 1 ]; 
			then REST="$REST $i";
		fi
    ;;
esac
#shift
done

echo "LOGIN: " $LOGIN
echo "EXEC: " $EXEC
echo "PROGHOME: " $PROGHOME
echo "REST: " $REST
echo "LIP: " $LIP
echo "XINERAMA: " $XINERAMA



#function colNum {
#  case "$1" in
#          "a" ) return 0;;
#          "b" ) return 2;;
#  esac
#}

#function colIP {
#  case "$1" in
#          "a" ) return 0;;
#          "b" ) return 1;;
#  esac
#}

function colNum {
  case "$1" in 
      "a" ) return 0;;
      "b" ) return 1;;
      "c" ) return 2;;
      "d" ) return 3;;
  esac
}

INSTANCES=1
NUMCLIENTS=16
LEFTBEZEL=60
TOPBEZEL=60
BEZELWIDTH=126
BEZELHEIGHT=154
WIDTH=7680
HEIGHT=2160
FULLONESCREENWIDTH=7932
FULLONESCREENHEIGHT=2314
CLUSTERWIDTH=31722 # 3840*8 +7*126 + 120
CLUSTERHEIGHT=9222 # 2160*4 + 3*154 + 120

## No Bezels -- Xinemara one instance
if [ $XINEMARA == "-xine" ]; then
  INSTANCES=1
  NUMCLIENTS=16
  LEFTBEZEL=0
  TOPBEZEL=0
  BEZELWIDTH=0
  BEZELHEIGHT=0
  WIDTH=7680
  HEIGHT=2160
  FULLONESCREENWIDTH=7680
  FULLONESCREENHEIGHT=2160
  CLUSTERWIDTH=30720
  CLUSTERHEIGHT=8640
fi

## Bezels -- Xinemara one instance
if [ $XINEMARA == "-xinebezel" ]; then
  INSTANCES=1
  NUMCLIENTS=16
  LEFTBEZEL=60
  TOPBEZEL=60
  BEZELWIDTH=126
  BEZELHEIGHT=154
  WIDTH=7680
  HEIGHT=2160
  FULLONESCREENWIDTH=7932
  FULLONESCREENHEIGHT=2314
  CLUSTERWIDTH=31722 # 3840*8 +7*126 + 120
  CLUSTERHEIGHT=9222 # 2160*4 + 3*154 + 120
fi 

echo "./$EXECW -screen-width 1440 -screen-height 388 -nc $NUMCLIENTS
 -wall WILDEST -logfile log.txt $REST  &"
./$EXECW -screen-width 1440 -screen-height 480 -nc $NUMCLIENTS -wall WILDEST -logfile log.txt $REST &
sleep 1

#exit 
#start client nodes
for col in {a..d}
do
    for row in {1..4}
      do
        colNum $col
        startIp=`expr 32 + $? \* 4 + $row - 1`
        
        ROW0=`expr $row - 1`
        Y=`expr $TOPBEZEL + $ROW0 \* $FULLONESCREENHEIGHT`
        colNum $col
        X=`expr $LEFTBEZEL + $? \* $FULLONESCREENWIDTH`
        X1=`expr $X + $FULLONESCREENWIDTH`
        
        echo " "
        echo " "$col$row.wild.lri.fr
        chost="192.168.0.$startIp"
        echo "$LOGIN@$col$row"
        echo "start on \"$chost\" "
        
        echo "ssh $LOGIN@$chost cd $PROGHOME ; DISPLAY=:0.0 ./$EXEC -x $X -y $Y -screen-width $WIDTH -screen-height $HEIGHT -wall WILDEST -logfile log.txt -s \"192.168.0.$LIP\" " $REST

        ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "cd $PROGHOME ; DISPLAY=:0.0 ./$EXEC -x $X -y $Y -popupwindow -screen-fullscreen 0 -screen-width $WIDTH -screen-height $HEIGHT  -wall WILDEST -logfile log-0.txt" -s \"192.168.0.$LIP\" $REST &

        #if [ $INSTANCES == 2 ]; then
        #  echo "---- "
        #  echo "ssh $LOGIN@$chost cd $PROGHOME ; DISPLAY=$DPY2 ./$EXEC -x $X1 -y $Y -wx $WX -screen-width $WIDTH -screen-height $HEIGHT -wall WILDEST -logfile log.txt -s \"192.168.0.$LIP\" " $REST

         # ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "cd $PROGHOME ; DISPLAY=$DPY2 ./$EXEC  -x $X1 -y $Y -popupwindow -screen-fullscreen 1 -screen-width $WIDTH -screen-height $HEIGHT  -logfile log-1.txt" -s \"192.168.2.$LIP\" $REST &
        #fi
        #exit
        
      done
done       