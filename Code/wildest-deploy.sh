#!/bin/sh

#media
PROGHOME="/media/ssd/Demos/UnityARTangiWall-wall"
PROGHOME="/home/wild/Demos/VisibleAir"
BUILDHOME="Build/BuildLinux/"
EXEC="VisibleAir.x86_64"
DATA="VisibleAir_Data"
LIB="UnityPlayer.so"
BUNDELS="Bundels"
CLEAN="yes"
CPDATA="yes"
CPBUNDELS="no"
CLEANBUNDELS="no"
FRONTAL="no"

LOGIN="wild"

for i in "$@"
do
case $i in
    -nc)
	CLEAN="no"
	 ;;
	 -nd)
	CPDATA="no"
    CLEAN="no"
	 ;;
     -b)
    CPBUNDELS="yes"
     ;;
     -f)
        FRONTAL="yes"
    ;;
esac
#shift
done

function colIP {
  case "$1" in
          "a" ) return 0;;
          "b" ) return 1;;
  esac
}

echo "CLEAN:" $CLEAN
echo "CPDATA:" $CPDATA
echo "CPBUNDELS:" $CPBUNDELS


function colNum {
  case "$1" in 
      "a" ) return 0;;
      "b" ) return 1;;
      "c" ) return 2;;
      "d" ) return 3;;
  esac
}

cd $BUILDHOME

#start client nodes
for col in {a..d}
do
    for row in {1..4}
      do
          colNum $col 
          SLAVENUM1=`expr $? \* 8 + $row - 1`
          SLAVENUM2=`expr $SLAVENUM1 + 4`
                  colNum $col
          startIp=`expr 32 + $? \* 4 + $row - 1` 
    	echo " "
        echo " "$col$row.wild.lri.fr $LOGIN@$chost:~/.ssh/
        chost="192.168.0.$startIp"
        ### ssh-copy-id $LOGIN@$chost
    	if [ $CLEAN == "yes" ]; then
            # clean 
        	echo "ssh $LOGIN@$chost ; rm -f $PROGHOME/$EXEC; rm -rf $PROGHOME/$DATA"
        	ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "rm -f $PROGHOME/$EXEC; rm -rf $PROGHOME/$DATA"
        fi
        echo "ssh $LOGIN@$chost; mkdir -p $PROGHOME"
        ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "mkdir -p $PROGHOME"
        echo "scp $EXEC $LOGIN@$chost:$PROGHOME"
        scp $EXEC $LOGIN@$chost:$PROGHOME
        echo "scp -r $DATA $LOGIN@$chost:$PROGHOME"
        scp -r $DATA $LOGIN@$chost:$PROGHOME
        echo "scp $LIB $LOGIN@$chost:$PROGHOME"
        scp $LIB $LOGIN@$chost:$PROGHOME
        if [ $CLEANBUNDELS == "yes" ]; then
            echo "ssh $LOGIN@$chost ; rm -rf $BUNDELS"
            ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "rm -rf $BUNDELS"
        fi
        if [ $CPBUNDELS == "yes" ]; then
            echo "scp -r $BUNDELS $LOGIN@$chost:$PROGHOME"
            scp -r $BUNDELS $LOGIN@$chost:$PROGHOME
        fi
		#sleep 2
        #exit
      done
done

if [ $FRONTAL == "yes" ]; then
    startIp=2 
    echo " "
    echo "FRONTAL"
    chost="192.168.0.$startIp"

    if [ $CLEAN == "yes" ]; then
            # clean 
            echo "ssh $LOGIN@$chost ; rm -f $PROGHOME/$EXEC; rm -rf $PROGHOME/$DATA"
            ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "rm -f $PROGHOME/$EXEC; rm -rf $PROGHOME/$DATA"
    fi
    echo "ssh $LOGIN@$chost; mkdir -p $PROGHOME"
    ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "mkdir -p $PROGHOME"
    echo "scp $EXEC $LOGIN@$chost:$PROGHOME"
    scp $EXEC $LOGIN@$chost:$PROGHOME
    echo "scp -r $DATA $LOGIN@$chost:$PROGHOME"
    scp -r $DATA $LOGIN@$chost:$PROGHOME
    echo "scp $LIB $LOGIN@$chost:$PROGHOME"
    scp $LIB $LOGIN@$chost:$PROGHOME
    if [ $CLEANBUNDELS == "yes" ]; then
            echo "ssh $LOGIN@$chost ; rm -rf $BUNDELS"
            ssh $LOGIN@$chost -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "rm -rf $BUNDELS"
    fi
    if [ $CPBUNDELS == "yes" ]; then
        echo "scp -r $BUNDELS $LOGIN@$chost:$PROGHOME"
        scp -r $BUNDELS $LOGIN@$chost:$PROGHOME
    fi
    scp wildest-start.sh  wild@192.168.0.2:$PROGHOME
    ##scp -r Assets/Resources/Experiments/   wild@192.168.0.2:/home/wild/Demos/UnityWallVis/Assets/Resources/
fi

cd -