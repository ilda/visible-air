using UnityEngine;
using System;

public static class UTex
{

static Texture2D BuildUTex(Color main, int w, bool up){
    int cw = (int)(3*w);
    int h = cw;
    Texture2D u = new Texture2D(cw, h);
    Color trans = new Color(1,1,1,0);
    for (int y = 0; y < u.height; y++)
    {
        for (int x = 0; x < u.width; x++)
        {
            Color color = trans;
            if (x < w || x >=2*w){
                color = main;
            }
            else if (up && y > h-w){
                 color = main;
            }
            else if (!up && y < w){
                 color = main;
            }
            u.SetPixel(x, y, color);
        }
    }
    u.Apply();

    return u;
}
static Texture2D BuildETex(Color main, int w, string dir, int num_lines = 5){
    int cw = (int)(num_lines*w);
    int h = cw;
    //Maybe without alpha
    Texture2D u = new Texture2D(cw, h, TextureFormat.RGB24, false);
    Color trans = new Color(1-main.r,1-main.g,1-main.b);
    //trans = new Color(0.6f, 0.6f, 0.6f);
    //main = new Color(0.4f, 0.4f, 0.4f); 
    int main_pixels = 0;
    int trans_pixels = 0;
    bool main_flag = false;
    if (dir == "left"){
        for (int y = 0; y < u.height; y++)
        {
            for (int x = 0; x < u.width; x++)
            {
                Color color = trans;
                if (x < w){
                    color = main;
                }
                else {
                    for(int i = 0; i < num_lines; i=i+2){
                        // 0, 2, 4, 6
                        if (y >= i*w && y < (i+1)*w) {
                            color = main;
                        } 
                    }
                }
                u.SetPixel(x, y, color);
            }
        }    
    }
    else if (dir == "right"){
        for (int y = 0; y < u.height; y++)
        {
            for (int x = 0; x < u.width; x++)
            {
                Color color = trans;
                if (x >= (num_lines-1)*w){
                    color = main;
                }
                else {
                    for(int i = 0; i < num_lines; i=i+2){
                        if (y >= i*w && y < (i+1)*w) {
                            color = main;
                        } 
                    }
                }
                u.SetPixel(x, y, color);
            }
        }
    }
    else if (dir == "hormid"){
        for (int y = 0; y < u.height; y++)
        {
            for (int x = 0; x < u.width; x++)
            {
                Color color = trans;
                if (x >= ((num_lines-1)/2)*w && x < ((num_lines+1)/2)*w){
                    color = main;
                }
                else {
                    for(int i = 0; i < num_lines; i=i+2){
                        if (y >= i*w && y < (i+1)*w) {
                            color = main;
                        } 
                    }
                }
                u.SetPixel(x, y, color);
            }
        }
    }
    else if (dir == "hornone"){
        for (int y = 0; y < u.height; y++)
        {
            for (int x = 0; x < u.width; x++)
            {
                Color color = trans;
                for(int i = 0; i < num_lines; i=i+2){
                    if (y >= i*w && y < (i+1)*w) {
                        color = main;
                    } 
                }
                if(main_flag){
                    main_pixels+=1;
                } else {
                    trans_pixels+=1;
                }
                u.SetPixel(x, y, color);
            }
        }
    }
    else if (dir == "down"){
        for (int y = 0; y < u.height; y++)
        {
            for (int x = 0; x < u.width; x++)
            {
                Color color = trans;
                if (y < w){
                    color = main;
                }
                else {
                    for(int i = 0; i < num_lines; i=i+2){
                        if (x >= i*w && x < (i+1)*w) {
                            color = main;
                        } 
                    }
                }
                u.SetPixel(x, y, color);
            }
        }
    }
    else if (dir == "up"){
        for (int y = 0; y < u.height; y++)
        {
            for (int x = 0; x < u.width; x++)
            {
                Color color = trans;
                if (y >= 4*w){
                    color = main;
                }
                else {
                    for(int i = 0; i < num_lines; i=i+2){
                        if (x >= i*w && x < (i+1)*w) {
                            color = main;
                        } 
                    }
                }
                u.SetPixel(x, y, color);
            }
        }
    }
    else if (dir == "vertmid"){
        for (int y = 0; y < u.height; y++)
        {
            for (int x = 0; x < u.width; x++)
            {
                Color color = trans;
                if (y >= ((num_lines-1)/2)*w && y < ((num_lines+1)/2)*w){
                    color = main;
                }
                else {
                    for(int i = 0; i < num_lines; i=i+2){
                        if (x >= i*w && x < (i+1)*w) {
                            color = main;
                        } 
                    }
                }
                u.SetPixel(x, y, color);
            }
        }
    }
    else if (dir == "vertnone"){
        for (int y = 0; y < u.height; y++)
        {
            for (int x = 0; x < u.width; x++)
            {
                Color color = trans;
                for(int i = 0; i < num_lines; i=i+2){
                    if (x >= i*w && x < (i+1)*w) {
                        color = main;
                    } 
                }
                u.SetPixel(x, y, color);
            }
        }
    }

    u.filterMode = FilterMode.Point;    
    u.Apply(false);
    

    return u;
}
public static Texture2D GetUTex(Color main, int w, string what, int num_lines = 5){
    Texture2D tex;
    if (what == "uup"){
        tex = BuildUTex(main, w, true);
    }
    else if (what == "udown"){
        tex = BuildUTex(main, w, false);
    } else if (what[0] == 'e'){
        
        tex = BuildETex(main, w, what.Substring(1), num_lines);
    }
    else {
         tex = BuildUTex(main, w, true);
    }
    return tex;
}

}