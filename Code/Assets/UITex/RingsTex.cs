using UnityEngine;

public static class RingsTex
{

public static Texture2D SimpleRing(Color main, Color black, int r1, int r2, int b){
	int cw = 2 * (r2+b);
	Texture2D ring = new Texture2D(cw, cw);
	Color trans = new Color(1,1,1,0);
	int cx = (r2+b);
	int cy = cx;
	for (int y = 0; y < ring.height; y++)
    {
        for (int x = 0; x < ring.width; x++)
        {
        	Color color = trans;
        	int tx = (x-cx);
        	int ty = (y-cy);

        	int p = tx*tx + ty*ty;
        	if (p < (r1-b)*(r1-b)){
        		// ok
        	}
        	else if (p < r1*r1){
        		color = black;
        	}
        	else if (p <= r2*r2){
        		color = main;
        	}
        	else if (p <= (r2+b)*(r2+b))
        	{
        		color = black;
        	}
            ring.SetPixel(x, y, color);
        }
    }
    ring.Apply();

    return ring;
}


    public static Texture2D RingWithArrow()
    {
        Texture2D ring = (Texture2D)Resources.Load("rotate");
       
        return ring;
    }

    public static Texture2D ResizeArrow()
    {
        Texture2D ring = (Texture2D)Resources.Load("resize");

        return ring;
    }
}