using System;
using UnityEngine;
using System.Collections;

public class WallDesktop : Wall {

	struct  wilder_screen
	{	
		public Vector3  orig;
		public Vector3  vectX;
		public Vector3  vectY;
		public Vector3  vectZ;
	}


	// center down 81.4853 -234.001 28.5381 (x,y,z)
	// in the x dir 0.302188 0.935625 -0.18245
	// in the y dir -0.187787 0.946128 -0.263775
	// in the z dir 	0.0627962 -0.239286 0.968916
	static wilder_screen wilderScreen = new wilder_screen {
		//orig = new Vector3(2974.2597f, 8.09393995f, 2215.8808f),  // x,z,y
		orig = new Vector3(2947.85f,  -28.5381f , 2286.79f),
		// from wilder but multiply for pix density (4K 140dpi to dpi)
		vectX = new Vector3(-0.250734f, 0.00142941f, -0.000593762f)*0.714f,
		vectY = new Vector3(0.000343606f, -0.0039237f, -0.244973f)*0.714f,
		vectZ = new Vector3(-0.00573797f, -0.999855f, 0.0160065f)*0.714f
		//vectX = new Vector3(-0.4099097f, -0.0013532f,  0.0039963f),
		//vectY = new Vector3( 0.0034272f, -0.0015333f, -0.4099142f),
		//vectZ = new Vector3(-0.0005608f,  0.1680141f, -0.0006332f),
	};


	public Vector3 getWallCoordinate(Vector3 pos, Vector3 dir)
	{
		Vector3 position = new Vector3(pos[0], pos[1], pos[2]);
		Vector3 direction = new Vector3(dir[0], dir[1], dir[2]);

		Vector3 l;
		wilder_screen s = wilderScreen;
		l = position - s.orig;
		
		Vector3 ox2 = Vector3.Cross(s.vectY, direction);
		Vector3 oy2 = Vector3.Cross(direction, s.vectX);
		Vector3 oz2 = Vector3.Cross(s.vectX, s.vectY);

		float d1  = (s.vectX.x * ox2.x)+  (s.vectX.y * ox2.y) + (s.vectX.z * ox2.z);
		float d2 =  (s.vectY.x * oy2.x) + (s.vectY.y * oy2.y) + (s.vectY.z * oy2.z);
		float d3 = (direction.x * oz2.x) + (direction.y * oz2.y) + (direction.z * oz2.z);

		// FIXME
		// if (Math.abs(d1) == 0 || Math.abs(d2) == 0) continue;

		Vector3 p;
		p.x = ((l.x * ox2.x) + (l.y * ox2.y) + (l.z * ox2.z))/d1;
		p.y = ((l.x * oy2.x) + (l.y * oy2.y) + (l.z * oy2.z))/d2;
		p.z = ((l.x * oz2.x) + (l.y * oz2.y) + (l.z * oz2.z))/d3;

		
		float sign = -1;
		if (dir[1] < 0) {
			sign = 1;
		}
		//Debug.Log("getWallCoordinate " + p.x + " " + p.y);
		return new Vector3(p.x, p.y, sign);
	}


	// one screen
	private float wall_width = 512f;
	private float wall_height = 256f;
	private float one_screen_width = 512f;
	private float one_screen_height = 256f;
	private float bezel_width = 10f;
	private float bezel_height = 25f;
	private float top_bezel = 0f;
	private float left_bezel = 0f;
	private int col = 1;
	private int row = 1;

	public WallDesktop(int c = 1, int r = 1){
	
		wall_width = one_screen_width*c + bezel_width*(c-1);
		wall_height = one_screen_height*r + bezel_height*(r-1);
		col = c; row = r;
	}

	public float getWallWidth(){
		return wall_width;
	}

	public float getWallHeight(){
		return wall_height;
	}

	public float getOneScreenWidth(){
		return one_screen_width;
	}

	public float getOneScreenHeight(){
		return one_screen_height;
	}

	public float getBezelWidth(){
		return bezel_width;
	}

	public float getBezelHeight(){
		return bezel_height;
	}

	public int getGridCol(){
		return col; 
	}

	public int getGridRow(){
		return row;
	}

	// check !!!
	public float getPixelSizeMM() { return 0.1822917f; } // mm
	public float getBottomHeight(){ return 1.0f; } // mm 

	public string getViconHost(){
		// FIXME ... should put a number adress
		// if not when the machine is not on -> crash !!
		//return "vicon.wild.lri.fr";
		return "192.168.0.14";
		// return "192.168.2.3"; 
	}

}