using System;
using UnityEngine;
using System.Collections;

public class WallWilder : Wall {

	struct  wilder_screen
	{	
		public Vector3  orig;
		public Vector3  vectX;
		public Vector3  vectY;
		public Vector3  vectZ;
	}

	static wilder_screen wilderScreen = new wilder_screen {
		orig = new Vector3(2974.2597f, 8.09393995f, 2215.8808f),  // x,z,y
		vectX = new Vector3(-0.4099097f, -0.0013532f,  0.0039963f),
		vectY = new Vector3( 0.0034272f, -0.0015333f, -0.4099142f),
		vectZ = new Vector3(-0.0005608f,  0.1680141f, -0.0006332f),
	};


	public Vector3 getWallCoordinate(Vector3 pos, Vector3 dir)
	{
		Vector3 position = new Vector3(pos[0], pos[1], pos[2]);
		Vector3 direction = new Vector3(dir[0], dir[1], dir[2]);

		Vector3 l;
		wilder_screen s = wilderScreen;
		l = position - s.orig;
		
		Vector3 ox2 = Vector3.Cross(s.vectY, direction);
		Vector3 oy2 = Vector3.Cross(direction, s.vectX);
		Vector3 oz2 = Vector3.Cross(s.vectX, s.vectY);

		float d1  = (s.vectX.x * ox2.x)+  (s.vectX.y * ox2.y) + (s.vectX.z * ox2.z);
		float d2 =  (s.vectY.x * oy2.x) + (s.vectY.y * oy2.y) + (s.vectY.z * oy2.z);
		float d3 = (direction.x * oz2.x) + (direction.y * oz2.y) + (direction.z * oz2.z);

		// FIXME
		// if (Math.abs(d1) == 0 || Math.abs(d2) == 0) continue;

		Vector3 p;
		p.x = ((l.x * ox2.x) + (l.y * ox2.y) + (l.z * ox2.z))/d1;
		p.y = ((l.x * oy2.x) + (l.y * oy2.y) + (l.z * oy2.z))/d2;
		p.z = ((l.x * oz2.x) + (l.y * oz2.y) + (l.z * oz2.z))/d3;

		
		float sign = -1;
		if (dir[1] < 0) {
			sign = 1;
		}
		//Debug.Log("getWallCoordinate " + p.x + " " + p.y);
		return new Vector3(p.x, p.y, sign);
	}

	public WallWilder(){

	}

	public float getWallWidth(){
		return 14400; //5920 mm 
	}

	public float getWallHeight(){
		return 4800; //1975 mm
	}

	public float getOneScreenWidth(){
		return 960;
	}

	public float getOneScreenHeight(){
		return 960;
	}

	public float getBezelWidth(){
		return 0;
	}

	public float getBezelHeight(){
		return 0;
	}

	public int getGridCol(){
		return 15;
	}

	public int getGridRow(){
		return 5;
	}

	// check !!!
	public float getPixelSizeMM() { return 0.411f; } // mm
	public float getBottomHeight(){ return 240.8f; } // mm 

	public string getViconHost(){
		return "192.168.2.3"; 
	}

}