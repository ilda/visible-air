using System;
using UnityEngine;
using System.Collections;

public interface Wall {
	 float getWallWidth(); // pixels
	 float getWallHeight();

	 float getOneScreenWidth();
	 float getOneScreenHeight();

	 float getBezelWidth();
	 float getBezelHeight();

	 int getGridCol();
	 int getGridRow();
		
	 float getPixelSizeMM(); // mm
	 float getBottomHeight(); // mm 

	 string getViconHost();

	 Vector3 getWallCoordinate(Vector3 pos, Vector3 dir);
}
