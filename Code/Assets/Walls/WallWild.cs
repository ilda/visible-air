using System;
using UnityEngine;
using System.Collections;

public class WallWild : Wall {

	struct wild_screen
	{
		public string name;
		public Vector3  orig;
		public Vector3  vectX;
		public Vector3  vectY;
		public Vector3  vectZ;
	}

	static wild_screen a1l = new wild_screen { name = "A1L",
			orig = new Vector3(2724.89f, -23.0565f, 2327.97f),
			vectX = new Vector3(-0.250734f, 0.00142941f, -0.000593762f),
			vectY = new Vector3(0.000343606f, -0.0039237f, -0.244973f),
			vectZ = new Vector3(-0.00573797f, -0.999855f, 0.0160065f)
		};

	static wild_screen[] wildScreens = 
	{
		new wild_screen {name = "A1L",
			orig = new Vector3(2724.89f, -23.0565f, 2327.97f),
			vectX = new Vector3(-0.250734f, 0.00142941f, -0.000593762f),
			vectY = new Vector3(0.000343606f, -0.0039237f, -0.244973f),
			vectZ = new Vector3(-0.00573797f, -0.999855f, 0.0160065f)
		},
	   new wild_screen { name = "A1R",
			orig = new  Vector3(2035.59f, -20.0508f, 2325.92f),
			vectX = new  Vector3(-0.25084f, 0.00156104f, -0.000648227f),
			vectY = new  Vector3(-0.000375077f, -0.00556344f, -0.244721f),
			vectZ = new Vector3(-0.0062802f, -0.999714f, 0.0227369f)
		}, 
		new wild_screen { name = "B1L",
			orig = new Vector3(1346.12f, -14.4003f, 2324.88f),
			vectX = new Vector3(-0.251231f, 0.00102608f, -0.000182005f),
			vectY = new Vector3(-0.000387269f, -0.00809619f, -0.244722f),
			vectZ = new Vector3(-0.00410588f, -0.999442f, 0.0330712f),
		},
		new wild_screen { name = "B1R",
			orig = new Vector3(654.742f, -12.544f, 2324.31f),
			vectX = new Vector3(-0.251115f, -0.00085377f, -0.00136783f),
			vectY = new Vector3(0.000180339f, -0.00910465f, -0.245227f),
			vectZ = new Vector3(0.00319542f, -0.999295f, 0.0371036f),
		},
		new wild_screen { name = "C1L",
			orig = new Vector3(-33.4396f, -8.26626f, 2324.34f),
			vectX = new Vector3(-0.250831f, 0.00142842f, -0.000128828f),
			vectY = new Vector3(-0.000848488f, -0.0124016f, -0.244327f),
			vectZ = new Vector3(-0.00571335f, -0.99869f, 0.0507117f),
		},
		new wild_screen { name = "C1R",
			orig = new Vector3(-723.984f, -6.93461f, 2325.2f),
			vectX = new Vector3(-0.251028f, -0.00192314f, -0.000537896f),
			vectY = new Vector3(-0.000252166f, -0.00990074f, -0.244325f),
			vectZ = new Vector3(0.00756779f, -0.999146f, 0.0404805f),
		},
		new wild_screen { name = "D1L",
			orig = new Vector3(-1415.75f, -12.1276f, 2324.49f),
			vectX = new Vector3(-0.250778f, 0.000132057f, -0.00164591f),
			vectY = new Vector3(0.000666913f, -0.00636619f, -0.244891f),
			vectZ = new Vector3(-0.000696954f, -0.999655f, 0.0259852f),
		},
		new wild_screen { name = "D1R",
			orig = new Vector3(-2105.52f, -12.5942f, 2320.81f),
			vectX = new Vector3(-0.251055f, -0.00170364f, -0.0008372f),
			vectY = new Vector3(0.000813031f, -0.00555493f, -0.245003f),
			vectZ = new Vector3(0.00670834f, -0.99972f, 0.0226888f),
		},
		new wild_screen { name = "A2L",
			orig = new Vector3(2726f, -35.2377f, 1870.68f),
			vectX = new Vector3(-0.250937f, 0.00171195f, -0.00068275f),
			vectY = new Vector3(0.000455971f, -0.00705123f, -0.244937f),
			vectZ = new Vector3(-0.0068975f, -0.999562f, 0.0287625f),
		},
		new wild_screen { name = "A2R",
			orig = new Vector3(2035.46f, -36.5872f, 1868.4f),
			vectX = new Vector3(-0.251064f, -0.000252481f, -0.000497689f),
			vectY = new Vector3(-1.33222e-05f, -0.00739095f, -0.245094f),
			vectZ = new Vector3(0.000945435f, -0.999543f, 0.0301417f),
		},
		new wild_screen { name = "B2L",
			orig = new Vector3(1345.68f, -36.2888f, 1867.14f),
			vectX = new Vector3(-0.25083f, 0.00127345f, -0.000488749f),
			vectY = new Vector3(0.000124551f, -0.00663269f, -0.244645f),
			vectZ = new Vector3(-0.00512783f, -0.999619f, 0.0270986f),
		},
		new wild_screen { name = "B2R",
			orig = new Vector3(657.26f, -28.7251f, 1866.96f),
			vectX = new Vector3(-0.251028f, 0.000295282f, -4.56206e-05f),
			vectY = new Vector3(0.000550868f, -0.00643679f, -0.2448f),
			vectZ = new Vector3(-0.00118066f, -0.999652f, 0.0262822f),
		},
		new wild_screen { name = "C2L",
			orig = new Vector3(-33.0862f, -27.4255f, 1867.57f),
			vectX = new Vector3(-0.250735f, 0.000957064f, -6.54573e-05f),
			vectY = new Vector3(-0.000283293f, -0.0061093f, -0.244241f),
			vectZ = new Vector3(-0.00382234f, -0.999679f, 0.0250099f),
		},
		new wild_screen { name = "C2R",
			orig = new Vector3(-724.593f, -24.3664f, 1868.21f),
			vectX = new Vector3(-0.250715f, -0.000859036f, -0.000442043f),
			vectY = new Vector3(0.000544698f, -0.0079993f, -0.24427f),
			vectZ = new Vector3(0.00336677f, -0.999458f, 0.0327376f),
		},
		new wild_screen { name = "D2L",
			orig = new Vector3(-1415.46f, -25.2742f, 1867.12f),
			vectX = new Vector3(-0.249262f, 0.00026419f, -0.00172373f),
			vectY = new Vector3(0.000926509f, -0.00726258f, -0.244824f),
			vectZ = new Vector3(-0.00126443f, -0.999555f, 0.0296465f),
		},
		new wild_screen { name = "D2R",
			orig = new Vector3(-2104.31f, -25.3543f, 1862.54f),
			vectX = new Vector3(-0.25074f, -0.000570866f, -0.000542602f),
			vectY = new Vector3(0.000411347f, -0.0079938f, -0.244562f),
			vectZ = new Vector3(0.0022048f, -0.999464f, 0.0326724f),
		},
		new wild_screen { name = "A3L",
			orig = new Vector3(2726.77f, -47.8326f, 1413.37f),
			vectX = new Vector3(-0.250748f, 0.00195936f, -0.000760826f),
			vectY = new Vector3(0.000492677f, -0.00608814f, -0.244815f),
			vectZ = new Vector3(-0.00788679f, -0.99966f, 0.024844f),
		},
		new wild_screen { name = "A3R",
			orig = new Vector3(2036.55f, -49.3098f, 1410.61f),
			vectX = new Vector3(-0.251159f, 0.000179402f, -0.000254723f),
			vectY = new Vector3(0.0004955f, -0.00402168f, -0.245463f),
			vectZ = new Vector3(-0.000730814f, -0.999865f, 0.0163803f),
		},
		new wild_screen { name = "B3L",
			orig = new Vector3(1346.46f, -48.619f, 1410.04f),
			vectX = new Vector3(-0.250837f, 0.000780026f, -0.000580115f),
			vectY = new Vector3(9.94839e-05f, -0.00645475f, -0.244881f),
			vectZ = new Vector3(-0.00316953f, -0.999646f, 0.0263481f),
		},
		new wild_screen { name = "B3R",
			orig = new Vector3(658.426f, -42.9499f, 1409.48f),
			vectX = new Vector3(-0.251033f, -6.43444e-05f, -0.000159312f),
			vectY = new Vector3(0.000233188f, -0.00606288f, -0.244598f),
			vectZ = new Vector3(0.000240514f, -0.999693f, 0.0247797f),
		},
		new wild_screen { name = "C3L",
			orig = new Vector3(-33.5824f, -43.3294f, 1408.17f),
			vectX = new Vector3(-0.25002f, 0.00172385f, 0.000717275f),
			vectY = new Vector3(-0.000441425f, -0.00677786f, -0.244798f),
			vectZ = new Vector3(-0.00681261f, -0.999593f, 0.0276885f),
		},
		new wild_screen { name = "C3R",
			orig = new Vector3(-722.059f, -39.1004f, 1410.79f),
			vectX = new Vector3(-0.25109f, -0.00087844f, -0.000496957f),
			vectY = new Vector3(0.000149895f, -0.00655405f, -0.244776f),
			vectZ = new Vector3(0.00344425f, -0.999635f, 0.026768f),
		},
		new wild_screen { name = "D3L",
			orig = new Vector3(-1414.34f, -39.6854f, 1410.03f),
			vectX = new Vector3(-0.2507f, -0.00103607f, -0.00182245f),
			vectY = new Vector3(0.00153273f, -0.00494206f, -0.244847f),
			vectZ = new Vector3(0.00398496f, -0.999787f, 0.0202049f),
		},
		new wild_screen { name = "D3R",
			orig = new Vector3(-2103.24f, -40.7218f, 1405.68f),
			vectX = new Vector3(-0.250874f, 0.00152772f, -0.000625352f),
			vectY = new Vector3(0.000383257f, -0.00334842f, -0.244314f),
			vectZ = new Vector3(-0.00612302f, -0.999887f, 0.0136943f),
		},
		new wild_screen { name = "A4L",
			orig = new Vector3(2727.36f, -58.0259f, 956.085f),
			vectX = new Vector3(-0.250861f, 0.000996493f, -0.000721382f),
			vectY = new Vector3(0.000455595f, -0.00612815f, -0.245129f),
			vectZ = new Vector3(-0.00404286f, -0.999679f, 0.0249841f),
		},
		new wild_screen { name = "A4R",
			orig = new Vector3(2037.65f, -55.653f, 954.571f),
			vectX = new Vector3(-0.250829f, 0.000653973f, -0.000312719f),
			vectY = new Vector3(3.96108e-05f, -0.00819832f, -0.24468f),
			vectZ = new Vector3(-0.00264752f, -0.999435f, 0.0334869f),
		},
		new wild_screen { name = "B4L",
			orig = new Vector3(1346.9f, -58.2972f, 952.62f),
			vectX = new Vector3(-0.250906f, 0.00023815f, -0.000683583f),
			vectY = new Vector3(0.000276982f, -0.00826205f, -0.245204f),
			vectZ = new Vector3(-0.00104036f, -0.999431f, 0.0336742f),
		},
		new wild_screen { name = "B4R",
			orig = new Vector3(658.436f, -54.1127f, 952.617f),
			vectX = new Vector3(-0.250812f, 0.000524847f, -0.000415693f),
			vectY = new Vector3(-0.000136472f, -0.00696626f, -0.244461f),
			vectZ = new Vector3(-0.00213894f, -0.99959f, 0.0284859f),
		},
		new wild_screen { name = "C4L",
			orig = new Vector3(-33.1759f, -54.2834f, 951.758f),
			vectX = new Vector3(-0.251848f, -0.00153133f, -0.00151384f),
			vectY = new Vector3(0.000592275f, -0.00527658f, -0.244504f),
			vectZ = new Vector3(0.00594904f, -0.999742f, 0.0215896f),
		},
		new wild_screen { name = "C4R",
			orig = new Vector3(-721.516f, -49.4304f, 951.323f),
			vectX = new Vector3(-0.251299f, -0.000134699f, 0.000487291f),
			vectY = new Vector3(-0.00211014f, -0.00993195f, -0.242991f),
			vectZ = new Vector3(0.000614731f, -0.999143f, 0.0408333f),
		},
		new wild_screen { name = "D4L",
			orig = new Vector3(-1413.67f, -49.1106f, 952.295f),
			vectX = new Vector3(-0.25024f, 0.0006269f, -0.00176579f),
			vectY = new Vector3(0.000835445f, -0.00722243f, -0.244378f),
			vectZ = new Vector3(-0.00271247f, -0.999554f, 0.0295319f),
		},
		new wild_screen { name = "D4R",
			orig = new Vector3(-2101.87f, -45.5159f, 948.204f),
			vectX = new Vector3(-0.250996f, 0.000740249f, -0.00104408f),
			vectY = new Vector3(-2.43481e-05f, -0.00762653f, -0.244038f),
			vectZ = new Vector3(-0.0030777f, -0.999499f, 0.0312361f),
		}
	};

	void name2coord(string name, out float x, out float y, out int l, out int c, out int m)
	{
		x = 0; y = 0; l = 0; c = 0; m = 0;

		if (name.Length >= 3)
		{
			int n;
			if (!Int32.TryParse(name.Substring(1,1), out n)){
				return;
			}
			// 4 -> 0 ; 1 -> 3 
			//l = -n + 4;
			l = n -1;
			y = (l) * (getOneScreenHeight() + getBezelHeight()) - getBezelHeight()/2;
			if (name.Substring(1,1) == "A" && name.Substring(2,1)  == "L")
			{
				c = 0;
			}
			else if (name.Substring(0,1)  == "A" && name.Substring(2,1)  == "R")
			{
				c = 1;
			}
			else if (name.Substring(0,1)  == "B" && name.Substring(2,1)  == "L")
			{
				c = 2;
			}
			else if (name.Substring(0,1)  == "B" && name.Substring(2,1)  == "R")
			{
				c = 3;
			}
			else if (name.Substring(0,1)  == "C" && name.Substring(2,1)  == "L")
			{
				c = 4;
			}
			else if (name.Substring(0,1)  == "C" && name.Substring(2,1)  == "R")
			{
				c = 5;
			}
			else if (name.Substring(0,1)  == "D" && name.Substring(2,1)  == "L")
			{
				c = 6;
			}
			else if (name.Substring(0,1)  == "D" && name.Substring(2,1)  == "R")
			{
				c = 7;
			}
			x = (c) * (getOneScreenWidth() + getBezelWidth()) - getBezelWidth()/2;
		}
	}

	string getPointedScreen(
		Vector3 position, Vector3 direction, out Vector3 rela_coor, out Vector3 abs_coor)
	{
		string name = "";
		float mindist = -1;

		rela_coor = new Vector3(0,0,0); 
		abs_coor = new Vector3(0,0,0);

		for(int i = 0; i < 32; i++)
		{
			wild_screen s = wildScreens[i];

			Vector3 l;
			l = position - s.orig;
			
			Vector3 ox2 = Vector3.Cross(s.vectY, direction);
			Vector3 oy2 = Vector3.Cross(direction, s.vectX);
			Vector3 oz2 = Vector3.Cross(s.vectX, s.vectY);

			Vector3 p;
			p.x = ((l.x * ox2.x) + (l.y * ox2.y) + (l.z * ox2.z))/ 
				((s.vectX.x * ox2.x)+  (s.vectX.y * ox2.y) + (s.vectX.z * ox2.z));
			p.y = ((l.x * oy2.x) + (l.y * oy2.y) + (l.z * oy2.z))/ 
				((s.vectY.x * oy2.x) + (s.vectY.y * oy2.y) + (s.vectY.z * oy2.z));
			p.z = ((l.x * oz2.x) + (l.y * oz2.y) + (l.z * oz2.z)) / 
				((direction.x * oz2.x) + (direction.y * oz2.y) + (direction.z * oz2.z));

			// indeed we are in pixel here and moreover we swaped z and y !!! 
			Vector3 c;
			//c.x = (WILD_ONE_SCREEN_W * s.vectX.x/2.0);
			//c.y = (WILD_ONE_SCREEN_H * s.vectY.y/2.0);
			// indeed we are in pixel here ??
			c.x = (getOneScreenWidth()/2.0f);
			c.y = (getOneScreenHeight()/2.0f);

			// do not care ?
			//c.z = s.orig.z + (WILD_ONE_SCREEN_W * s.vectZ.x/2.0) + (WILD_ONE_SCREEN_W * s.vectZ.y/2.0);
			
			float d = (c.x - p.x)*(c.x - p.x) + (c.y - p.y)*(c.y - p.y);

			if (mindist < 0 || d < mindist)
			{
				mindist = d;

				name = s.name;
				rela_coor.x = p.x;
				rela_coor.y = p.y;
				rela_coor.z = p.z;
				
				// FIXME
				abs_coor.x =  p.x + s.orig.x/s.vectX.x;
				abs_coor.y =  p.y + s.orig.z/s.vectY.z;
				abs_coor.z =  p.z + s.orig.y/s.vectZ.y;	
			}
			
		}
		return name;
	}

	public Vector3 getAbsPointedCoordinate(Vector3 position, Vector3 direction)
	{
		Vector3 ret;
		Vector3 dummy;
		
		getPointedScreen(position, direction, out dummy, out ret);
		
		return ret;
	}

	public Vector3 getWallCoordinate(Vector3 pos, Vector3 dir)
	{
		Vector3 gpos = new Vector3(pos[0], pos[1], pos[2]);
		Vector3 gdir = new Vector3(dir[0], dir[1], dir[2]);

		Vector3 gabs;
		Vector3 grela;
		
		string sn = getPointedScreen(gpos, gdir, out grela, out gabs);
		
		float wx,wy;
		int wl,wc,wm;
		float retx, rety;
		name2coord(sn, out wx, out wy, out wl, out wc, out wm);
		retx = wx + grela.x;
		rety = wy + grela.y;

		// aprox screen cursor pos in vicon coor
		// float sx = - 0.25 * (*ret[0] - ((float)getWallWidth()/2.0));
		// float sy = 0;
		// float sz = 500 + 0.25* ((float)getWallHeight() - *ret[1]);
		// float dist_to_wall = sqrt((px-sx)*(px-sx) + (py-sy)*(py-sy) +  (pz-sz)*(pz-sz));

		// FIXME !!
		int sign = +1;
		return new Vector3(retx, rety, sign);
	}

	public WallWild(){
		
	}

	public float getWallWidth(){
		return 21768f; //2560*8 + 2*92*7;
	}

	public float getWallHeight(){
		return 7042f; //=1600*4 + 2*107*3;
	}

	public float getOneScreenWidth(){
		return 2560f;
	}

	public float getOneScreenHeight(){
		return 1600f;
	}

	public float getBezelWidth(){
		return 2f*92f;
	}

	public float getBezelHeight(){
		return 2f*107f;
	}

	public int getGridCol(){
		return 8;
	}

	public int getGridRow(){
		return 4;
	}

	public float getPixelSizeMM() { return 0.25f; } // mm
	public float getBottomHeight(){ return 500f; } // mm 

	public string getViconHost(){
		return "vicon.wild.lri.fr";
	}

}