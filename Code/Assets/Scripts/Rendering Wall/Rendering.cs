using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

[NetworkSettings(channel = 0, sendInterval = 0.01f)]
public class Rendering : MonoBehaviour // NetworkBehaviour 
{
    public Circle Cshape;
    public Canvas canvas;
    private GameObject RecivedInfo;
    public bool toMiniWall = false;

    private Setup setup;
    private InputManager inputMan;
    private Wall wall;

    private float screenRatio = 1;
    private float pixToUnit = 1;

    public List<mShape> ListShapes = new List<mShape>();
    public int nbshape = 0;
    public Transform Polygon;
    public bool toWall;
    public Vector2 refStratselection;
    public bool syncroPosMiniwall;

    public class mShape {
        public Circle circ;
        public Transform shape;
        public Vector3 translation;
        public float rotation;
        public float rescale;
        public Vector3 initPos;
        public float initRotate;
        public float initScale;

      

        public mShape(Circle c, int n, Vector3 pos, Vector3 scale, bool AR){
            circ = c;
            circ.SetShape(n);
            circ.SetColor(Color.white);
            circ.transform.position = pos;
            circ.transform.localScale = scale;
        }

        public mShape(Transform Polygon, Circle c,Vector3 pos, Vector3 scale, bool AR)
        {
           
            circ = c;
            shape = Instantiate(Polygon, pos , new Quaternion(0,0,0,0));
            circ.SetColor(Color.white);
            circ.transform.position = pos;
            circ.transform.localScale = scale;
            Debug.Log("test");
        }
    }

    float sw, sh;
    float abss = 1.0f;
    float imscale = 1.5f;
    float wimscale = 1.5f;
    float pixelSizeInMM = 0.1822917f; // my laptop (olivier)


    void Awake(){Debug.Log("Awake Rendering (test) ");
    }

    void Start()
    {
        canvas.GetComponent<LineRenderer>().startWidth = canvas.GetComponent<LineRenderer>().endWidth = 0.05f;
      
        Debug.Log("Start Rendering ");
        RecivedInfo = GameObject.Find("RecivedInfo");
        setup = GameObject.Find("NetworkManager").GetComponent<Setup>();
        wall = setup.wallC;

        if (setup.is_server)
        {
            inputMan = GameObject.Find("Main Camera").GetComponent<InputManager>();
        }
        Color red = new Color(164 / 255f, 4 / 255f, 26 / 255f, 1);
        Color bleu = new Color(39 / 255f, 119 / 255f, 195 / 255f, 1);
        if (setup.myname != "ArClient")
        {
           
            if (setup.is_server)
            {
                sw = Screen.width;
                sh = Screen.height; // sw / sh == wall.getWallWidth() / wall.getWallHeight()
                screenRatio = sh / sw;
                pixToUnit = Camera.main.orthographicSize / (sh / 2.0f);
                abss = Screen.width/ wall.getWallWidth();
                imscale = imscale * abss;
                Debug.Log("SERVER RENDERING: " + sw + " " + sh + " " + pixToUnit + " " + screenRatio + " abss: "+ abss);
            }
            else
            {
                sw = wall.getWallWidth();
                sh = wall.getWallHeight();
                Debug.Log("sw : " + sw + " sh : " + sh);
                screenRatio = sh / sw;
                float shnobezel = 8640; // FIXME sh - 3 * wall.getWallHeight()
                pixToUnit = (float)wall.getGridRow() * (float)Camera.main.orthographicSize / (shnobezel / 2.0f);
                //pixToUnit = (float)wall.getGridRow() * (float)Camera.main.orthographicSize / (sh / 2.0f);

                pixelSizeInMM = wall.getPixelSizeMM();
                abss = 1.0f;
                Debug.Log("WALL RENDERING: " + sw + " " + sh + " " + pixToUnit + " " + screenRatio);
            }

            // add shapes
            // a circle in the middle
            float sizemm = 100f; // mm
            float sizepix = sizemm / pixelSizeInMM; // pix
            float sizeunit = sizepix * pixToUnit;
            sizeunit = sizeunit*abss;
            //Debug.Log(sizeunit / sw);

            float x = (sw / 2f) * pixToUnit - sizeunit / 2f;
            float y = (sh / 2f) * pixToUnit - sizeunit * 0.5f;
            float xrandom, yrandom , sizerandom;
            /*for (int i = 0; i< 25; i++)
            {
                xrandom = Random.Range(-x, x);
                yrandom = Random.Range(-y, y);
                sizerandom = Random.Range(sizeunit, 3 * sizeunit);
                
                if (i < 5)
                { 
                    createPolygone(3, xrandom, yrandom, sizerandom, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), false);
                }
               else if (i < 10)
                {
                    createPolygone(5, xrandom, yrandom, sizerandom, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), false);
                }
                else if (i < 15)
                {
                    createPolygone(6, xrandom, yrandom, sizerandom, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), false);
                }
                else if ( i < 20)
                {
                    createSquare(xrandom, yrandom, sizerandom, red, false);
                }
                else
                {
                    createCircle(xrandom, yrandom, sizerandom, bleu, false);
                }
            }*/

            Debug.Log(sizeunit + " "+ sw + " " + sizeunit / sw);
            createPolygone(3, x/2, y/2, sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), false);
            createPolygone(3, -x/6, y/8, 2*sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), false);
            createPolygone(3, x/7, -y /4 , 2/3f*sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), false);
            createPolygone(3, x/12, 0, 3*sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), false);
            createPolygone(3, -x/3, -y/2, sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), false);

            createPolygone(5, -x/5, 2/3 * y, sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), false);
            createPolygone(5, -2/3 * x, y/2, 1.5f *sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), false);
            createPolygone(5, x * 7/ 8, 0, sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), false);
            createPolygone(5, 5/7 * x, -y *8/ 9, 4/5f* sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), false);
            createPolygone(5, x/15,  - y /5, sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), false);

            createPolygone(6, -x * 11/12, -y * 7 / 8, sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), false);
            createPolygone(6, -x * 1 / 12, -y * 6 / 8, 1.9f*sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), false);
            createPolygone(6, -x * 7 / 12, -y * 5 / 8, sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), false);
            createPolygone(6, x * 8 / 12, y * 1 / 8, 2.1f*sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), false);
            createPolygone(6, -x * 10 / 12, y * 6 / 8, sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), false);
            
            createSquare(-x * 3 / 5, -y * 7 / 15, sizeunit, red, false);
            createSquare(-x * 1 / 5, -y * -3 / 15, 0.8f*sizeunit, red, false);
            createSquare(x * 2 / 5, -y *7 / 15, 1.8f*sizeunit, red, false);
            createSquare(-x * 4 / 5, -y * 1 / 15, sizeunit, red, false);
            createSquare(x * 3 / 5, -y * -6 / 15, sizeunit, red, false);


            createCircle(0.3f * x, 0.3f * y, sizeunit, bleu, false);
            createCircle(-0.7f * x, 0.7f * y, sizeunit, bleu, false);
            createCircle(0.6f * x, 0.6f * y, sizeunit, bleu, false);
            createCircle(0.8f * x, -0.25f * y, sizeunit, bleu, false);
            createCircle(-0.7f * x, -0.7f * y, sizeunit, bleu, false);
            
            /*
            createCircle(0, 0, sizeunit, bleu, false);

            


            // A square in each corner of the screen
            float x = (sw / 2f) * pixToUnit - sizeunit/2f;
            float y = (sh / 2f) * pixToUnit - sizeunit*0.5f;

            createCircle(x/8f, y/8f, sizeunit, bleu,false);

            createSquare(x, y, sizeunit, red, false);
            createSquare(x, -y, sizeunit, red, false);
            createSquare(-x, y, sizeunit, red, false);
            createSquare(-x, -y, sizeunit, red, false);
            createSquare(x / 2, y / 2, sizeunit, Color.yellow, false);
            createSquare(x / 4, y / 4, sizeunit, Color.yellow, false);

            createPolygone(3, -x / 2, y / 2, sizeunit, new Color(139/255f, 219/255f, 189/255f, 1), false);
            createPolygone(6, -x / 2, -y / 2, sizeunit, new Color(180/255f, 120/255f, 145/255f, 1), false);
            createPolygone(5, x / 2, -y / 2, sizeunit, new Color(93/255f, 166/255f, 11/255f, 1), false);

            createPolygone(3, -x / 4, y / 4, sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), false);
            createPolygone(6, -x / 4, -y / 4, sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), false);
            createPolygone(5, x / 4, -y / 4, sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), false);
            */
            /*
            mShape ms = new mShape( Polygon,(Circle)Instantiate(Cshape), new Vector3(x, y, 0), new Vector3(sizeunit, sizeunit, sizeunit), false);
            ms.circ.SetColor(Color.black);
            ms.circ.transform.parent = canvas.transform;
            ListShapes.Add(ms);
            ms.circ.setID(nbshape); nbshape++;
            mShape border = new mShape(Polygon,(Circle)Instantiate(Cshape), new Vector3(0, 0, 0), new Vector3(sizeunit * 1.2f, sizeunit * 1.2f, sizeunit * 1.2f), false);
            border.circ.transform.parent = ms.circ.transform;
            border.circ.transform.localPosition = new Vector3(0, 0, 0.01f);
            */
            // real sizes
            Debug.Log("SIZE: " + 1 / pixToUnit + "px " + pixelSizeInMM / pixToUnit + "mm");
           
        }

        if (setup.myname == "ArClient")
        {
            sw = GameObject.Find("Wall").transform.localScale.x;
            sh = GameObject.Find("Wall").transform.localScale.y;
            Debug.Log("sw : " + sw + " sh : " + sh);
            screenRatio = sh / sw;
            pixToUnit = Camera.main.orthographicSize / (sh / 2.0f);
            Debug.Log("WALL RENDERING: " + sw + " " + sh + " " + pixToUnit + " " + screenRatio);
            // add shapes
            // a circle in the middle
           
            float sizeunit = 0.1036f;
            Debug.Log(sizeunit);
            Debug.Log(sizeunit / sw);
            //sizeunit =  0.1f;//0.0707f;//Mathf.Sqrt(0.050f); //  0.1f; 10cm square

            // A square in each corner of the screen
            float x = (sw / 2f) - sizeunit / 2f;
            float y = (sh / 2f) - sizeunit * 0.5f;

            createPolygone(3, x / 2, y / 2, sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), true);
            createPolygone(3, -x / 6, y / 8, 2 * sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), true);
            createPolygone(3, x / 7, -y / 4, 2 / 3f * sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), true);
            createPolygone(3, x / 12, 0, 3 * sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), true);
            createPolygone(3, -x / 3, -y / 2, sizeunit, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), true);

            createPolygone(5, -x / 5, 2 / 3 * y, sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), true);
            createPolygone(5, -2 / 3 * x, y / 2, 1.5f * sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), true);
            createPolygone(5, x * 7 / 8, 0, sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), true);
            createPolygone(5, 5 / 7 * x, -y * 8 / 9, 4 / 5f * sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), true);
            createPolygone(5, x / 15, -y / 5, sizeunit, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), true);

            createPolygone(6, -x * 11 / 12, -y * 7 / 8, sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), true);
            createPolygone(6, -x * 1 / 12, -y * 6 / 8, 1.9f * sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), true);
            createPolygone(6, -x * 7 / 12, -y * 5 / 8, sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), true);
            createPolygone(6, x * 8 / 12, y * 1 / 8, 2.1f * sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), true);
            createPolygone(6, -x * 10 / 12, y * 6 / 8, sizeunit, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), true);

           // sizeunit = 0.07071f;
            createSquare(-x * 3 / 5, -y * 7 / 15, sizeunit, red, true);
            createSquare(-x * 1 / 5, -y * -3 / 15, 0.8f * sizeunit, red, true);
            createSquare(x * 2 / 5, -y * 7 / 15, 1.8f * sizeunit, red, true);
            createSquare(-x * 4 / 5, -y * 1 / 15, sizeunit, red, true);
            createSquare(x * 3 / 5, -y * -6 / 15, sizeunit, red, true);

           // sizeunit = 0.1f;
            createCircle(0.3f * x, 0.3f * y, sizeunit, bleu, true);
            createCircle(-0.7f * x, 0.7f * y, sizeunit, bleu, true);
            createCircle(0.6f * x, 0.6f * y, sizeunit, bleu, true);
            createCircle(0.8f * x, -0.25f * y, sizeunit, bleu, true);
            createCircle(-0.7f * x, -0.7f * y, sizeunit, bleu, true);
            /*
           
            float xrandom, yrandom, sizerandom;
            for (int i = 0; i < 25; i++)
            {
                xrandom = Random.Range(-x, x);
                yrandom = Random.Range(-y, y);
                sizerandom = Random.Range(sizeunit, 3 * sizeunit);

                if (i < 5)
                {
                    createPolygone(3, xrandom, yrandom, sizerandom, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), true);
                }
                else if (i < 10)
                {
                    createPolygone(5, xrandom, yrandom, sizerandom, new Color(93 / 255f, 166 / 255f, 11 / 255f, 1), true);
                }
                else if (i < 15)
                {
                    createPolygone(6, xrandom, yrandom, sizerandom, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), true);
                }
                else if (i < 20)
                {
                    createSquare(xrandom, yrandom, sizerandom, red, true);
                }
                else
                {
                    createCircle(xrandom, yrandom, sizerandom, bleu, true);
                }
            }
            */
            /*
            // Shader sprite = Shader.Find("Sprites/Default");
            createCircle(x/8, y/8, sizeunit, bleu, true);
            createSquare(x, y, 0.08f, Color.red, true );
            //ListShapes[1].circ.m_Material.shader = sprite;
            //

            createSquare(x, -y, 0.08f, Color.red, true);
            createSquare(-x, y, 0.08f, Color.red, true);
            createSquare(-x, -y, 0.08f, Color.red, true);
            createSquare(x/2, y/2, 0.08f, Color.yellow, true);
            createSquare(x / 4, y / 4, sizeunit, Color.yellow, false);

            createPolygone(3, -x / 2, y / 2, 0.08f, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), true);
            createPolygone(6, -x / 2, -y / 2, 0.08f, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), true);
            createPolygone(5, x / 2, -y / 2, 0.08f, Color.green, true);

            createPolygone(3, -x / 4, y / 4, 0.08f, new Color(139 / 255f, 219 / 255f, 189 / 255f, 1), true);
            createPolygone(6, -x / 4, -y / 4, 0.08f, new Color(180 / 255f, 120 / 255f, 145 / 255f, 1), true);
            createPolygone(5, x / 4, -y / 4, 0.08f, Color.green, true);
            */
        }
    }

    public void moveTarget(Vector3 postarget)
    {
        if (setup.is_server)
        {
            RecivedInfo.GetComponent<RecivedInfoNetwork>().coordTargetOnWall = postarget;
            RecivedInfo.GetComponent<RecivedInfoNetwork>().moveTarget = true;
            //Debug.Log(postarget);
        }
    }

    void Update()
    {
    }

    void createSquare(float x, float y , float sizeunit , Color c , bool ar)
    {

        if (!ar)
        {
            mShape ms = new mShape((Circle)Instantiate(Cshape), 4, new Vector3(x, y, 0), new Vector3(sizeunit, sizeunit, sizeunit), ar);
            ms.circ.SetColor(c);
            ms.circ.transform.parent = canvas.transform;
            ListShapes.Add(ms);
            ms.circ.setID(nbshape); nbshape++;
            mShape border = new mShape((Circle)Instantiate(Cshape), 4, new Vector3(0, 0, 0), new Vector3(sizeunit * 1.2f, sizeunit * 1.2f, sizeunit * 1.2f), ar);
            border.circ.transform.parent = ms.circ.transform;
            border.circ.transform.localPosition = new Vector3(0, 0, 0.01f);
        }
        else
        {
            //Shader sprite = Shader.Find("Sprites/Default");

            c.a = 0.75f;
            mShape ms = new mShape((Circle)Instantiate(Cshape), 4, new Vector3(x, y, 0), new Vector3(sizeunit, sizeunit, sizeunit), ar);
           
            // ms.circ.m_Material.shader = sprite;
            ms.circ.SetColor(c);
          
            ms.circ.transform.parent = canvas.transform;
            ms.circ.transform.rotation = new Quaternion(0, 0, 0, 0);
            ms.circ.transform.localPosition = new Vector3(x, y, -0.002f);
            ListShapes.Add(ms);
            ms.circ.setID(nbshape); nbshape++;
            Color b = Color.white;
            b.a = 0.75f;
            mShape border = new mShape((Circle)Instantiate(Cshape), 4, new Vector3(0, 0, 0), new Vector3(sizeunit * 1.2f, sizeunit * 1.2f, sizeunit * 1.2f), ar);
           // border.circ.m_Material.shader = sprite;
            border.circ.SetColor(b);
            border.circ.transform.parent = ms.circ.transform;
            border.circ.transform.localPosition = new Vector3(0, 0, 0.01f);
            border.circ.transform.rotation = new Quaternion(0, 0, 0, 0);
            ms.circ.transform.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            border.circ.transform.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            border.circ.GetComponent<MeshCollider>().enabled = false;
            ms.circ.transform.gameObject.SetActive(false);
        }
    }

    void createPolygone(int nbpoly , float x, float y, float sizeunit, Color c, bool ar)
    {

        if (!ar)
        {
            mShape ms = new mShape((Circle)Instantiate(Cshape), nbpoly, new Vector3(x, y, 0), new Vector3(sizeunit, sizeunit, sizeunit), ar);
            ms.circ.SetColor(c);
            ms.circ.transform.parent = canvas.transform;
            ListShapes.Add(ms);
            ms.circ.setID(nbshape); nbshape++;
            mShape border = new mShape((Circle)Instantiate(Cshape), nbpoly, new Vector3(0, 0, 0), new Vector3(sizeunit * 1.2f, sizeunit * 1.2f, sizeunit * 1.2f), ar);
            border.circ.transform.parent = ms.circ.transform;
            border.circ.transform.localPosition = new Vector3(0, 0, 0.01f);
        }
        else
        {
            //Shader sprite = Shader.Find("Sprites/Default");

            c.a = 0.75f;
            mShape ms = new mShape((Circle)Instantiate(Cshape), nbpoly, new Vector3(x, y, 0), new Vector3(sizeunit, sizeunit, sizeunit), ar);

            // ms.circ.m_Material.shader = sprite;
            ms.circ.SetColor(c);

            ms.circ.transform.parent = canvas.transform;
            ms.circ.transform.rotation = new Quaternion(0, 0, 0, 0);
            ms.circ.transform.localPosition = new Vector3(x, y, -0.002f);
            ListShapes.Add(ms);
            ms.circ.setID(nbshape); nbshape++;
            Color b = Color.white;
            b.a = 0.75f;
            mShape border = new mShape((Circle)Instantiate(Cshape), nbpoly, new Vector3(0, 0, 0), new Vector3(sizeunit * 1.2f, sizeunit * 1.2f, sizeunit * 1.2f), ar);
            // border.circ.m_Material.shader = sprite;
            border.circ.SetColor(b);
            border.circ.transform.parent = ms.circ.transform;
            border.circ.transform.localPosition = new Vector3(0, 0, 0.01f);
            border.circ.transform.rotation = new Quaternion(0, 0, 0, 0);
            ms.circ.transform.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            border.circ.transform.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            border.circ.GetComponent<MeshCollider>().enabled = false;
            ms.circ.transform.gameObject.SetActive(false);
        }
    }

    void createCircle( float x, float y, float sizeunit, Color c , bool ar)
    {

        if (!ar)
        {
            mShape ms = new mShape((Circle)Instantiate(Cshape), 100, new Vector3(x, y, 0), new Vector3(sizeunit, sizeunit, sizeunit), ar);
            ListShapes.Add(ms);
            ms.circ.SetColor(c);
            ms.circ.setID(nbshape); nbshape++;
            ms.circ.transform.parent = canvas.transform;
            mShape border = new mShape((Circle)Instantiate(Cshape), 100, new Vector3(x, y, 0), new Vector3(sizeunit * 1.2f, sizeunit * 1.2f, sizeunit * 1.2f), ar);
            border.circ.transform.parent = ms.circ.transform;
            border.circ.transform.localPosition = new Vector3(0, 0, 0.01f);
        }
        else
        {
            mShape ms = new mShape((Circle)Instantiate(Cshape), 100, new Vector3(x, y, 0), new Vector3(sizeunit, sizeunit, sizeunit), ar);
            ListShapes.Add(ms);
            ms.circ.SetColor(c);
            ms.circ.setID(nbshape); nbshape++;
            ms.circ.transform.parent = canvas.transform;
            ms.circ.transform.rotation = new Quaternion(0, 0, 0, 0);
            ms.circ.transform.localPosition = new Vector3(x, y, -0.002f * ms.circ.getID());
            mShape border = new mShape((Circle)Instantiate(Cshape), 100, new Vector3(x, y, 0), new Vector3(sizeunit * 1.2f, sizeunit * 1.2f, sizeunit * 1.2f), ar);
            border.circ.transform.parent = ms.circ.transform;
            border.circ.transform.localPosition = new Vector3(0, 0, 0.01f);
            border.circ.transform.rotation = new Quaternion(0, 0, 0, 0);


            ms.circ.transform.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            border.circ.transform.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            border.circ.GetComponent<MeshCollider>().enabled = false;
            ms.circ.transform.gameObject.SetActive(false);
        }
    }

    void createTriangle()
    {

    }
}