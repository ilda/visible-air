using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Rendering;

public class Circle : MonoBehaviour {


	public Material m_Material;

	public Color ColorIn ;

    public float xshape , yshape;

    Color CurrentColor;
    Color InitialColor;

    float initialScale;
	bool CurrentColorSet = false;
    int id;
    bool isSelected;

	bool setShape = false;
	int newShape = 40;
    public bool onMiniWall;

    public Vector3 newposMiniwall;
    public bool newpos;
    public bool changeParent;

    public void SetColorTest(){
		m_Material.color = Color.blue;
	}

	public void SetColor(Color c){
        if (m_Material != null){
		  m_Material.color = c;
        }
        InitialColor = c;
        CurrentColor = c;
        CurrentColorSet =  true;
	}

    public void ChangeColor(Color c)
    {
        if (m_Material != null)
        {
            m_Material.color = c;
        }
        CurrentColor = c;
        CurrentColorSet = true;
    }

    public Color GetInitialColorColor()
    {
        return InitialColor;
    }

    public void SetShape(int n){
        setShape = true;
        newShape = n;

    }


	void setCircle(int n){
		MeshFilter mf = GetComponent<MeshFilter>();
		Mesh mesh =  mf.mesh;

        mesh.Clear();

        if (n < 3) {
		   return;
        }

    	List<Vector3> verticiesList = new List<Vector3>();
    	float x = 0;
    	float y = 0;
    	float radius = 1f;
        float add = 0f;
        if (n==4){
            add = Mathf.PI/4;
        }

    	for (int i = 0; i < n; i ++)
    	{
        	x = radius * Mathf.Sin(add + ((2f * Mathf.PI * i) / (float)n))/2f;
        	y = radius * Mathf.Cos(add + ((2f * Mathf.PI * i) / (float)n))/2f;
        	verticiesList.Add(new Vector3(x, y, 0f));
    	}
        if (n==3){
            verticiesList.Add(new Vector3(x, y, 0f));
            n = 4;
        }
    	Vector3[] verticies = verticiesList.ToArray();
    	
		//triangles
    	List<int> trianglesList = new List<int>();
    	for(int i = 0; i < (n-2); i++)
    	{
        	trianglesList.Add(0);
        	trianglesList.Add(i+1);
        	trianglesList.Add(i+2);
    	}
    	int[] triangles = trianglesList.ToArray();

    	//normals
    	List<Vector3> normalsList = new List<Vector3> ();
    	for (int i = 0; i < verticies.Length; i++)
    	{
        	normalsList.Add(-Vector3.forward);
    	}
    	Vector3[] normals = normalsList.ToArray();

    	//initialise
    	mesh.vertices = verticies;
    	mesh.triangles = triangles;
    	mesh.normals = normals;
    	mesh.RecalculateBounds();

    	//polyCollider
    	// PolygonCollider2D  polyCollider = GetComponent<PolygonCollider2D>();
    	// polyCollider.pathCount = 1;

    	// List<Vector2> pathList = new List<Vector2> { };
    	// for (int i = 0; i < n; i++)
    	// {
     	//		pathList.Add(new Vector2(verticies[i].x, verticies[i].y));
    	// }
    	// Vector2[] path = pathList.ToArray();

    	// polyCollider.SetPath(0, path);
	}

	void Start () {
		m_Material = GetComponent<Renderer>().material;
		if (CurrentColorSet){
			m_Material.color = CurrentColor;
		}
		else{
			CurrentColor = ColorIn;
			CurrentColorSet = true;
			m_Material.color = ColorIn;
		}
	}

    public int getID()
    {
        return id;
    }

    public void setID(int i)
    {
        id = i;
    }

    public void selectShape()
    {
        this.isSelected = true;
        this.transform.GetChild(0).GetComponent<Circle>().ChangeColor(Color.green);
    }

    public void unSelectShape()
    {
        this.isSelected = false;
        this.transform.GetChild(0).GetComponent<Circle>().ChangeColor(Color.white);
    }

    public bool getIsSelected()
    {
       return this.isSelected;
    }

    public void setIsSelected(bool b)
    {
        this.isSelected = b;
    }

    public float getinitialScale()
    {
        return this.initialScale;
    } 

    public void setInitialScale(float s)
    {
        this.initialScale = s;
    }
    

    void Update(){
		if (setShape){
			setShape = false;
			setCircle(newShape);
		}
    }


}