using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Info : MonoBehaviour
{
    CustomController controller;
    const float SEEPD = 6f;
    private RaycastHit hit;
    Transform cam;
    public Transform infoTouchPad;
    public Transform infoTrigger;

    private bool infoButtonDown, showInfo = false;

    Vector3 desiredScale = Vector3.zero;
    // Start is called before the first frame update

    void Awake()
    {
        //controller = GetComponent<VarjoExample.Controller>();
        controller = transform.GetComponent<CustomController>();
    }
    void Start()
    {
        cam = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (!controller.primaryButton && infoButtonDown)
        {
            infoButtonDown = false;
        }

        if (controller.primaryButton && !infoButtonDown)
        {
            infoButtonDown = true;
            showInfo = !showInfo;

            Debug.Log("click ! " + showInfo);
        }

        infoTouchPad.localScale = Vector3.Lerp(infoTouchPad.localScale, desiredScale, Time.deltaTime * SEEPD);
        infoTrigger.localScale = Vector3.Lerp(infoTrigger.localScale, desiredScale, Time.deltaTime * SEEPD);
        Ray ray = new Ray(cam.position, cam.forward);
        if (Physics.Raycast(ray, out hit, 2f))
        {
            // Debug.Log(hit.transform.gameObject.name);
            // infoTouchPad.gameObject.SetActive(true);
            // infoTrigger.gameObject.SetActive(true);
            if (showInfo)
            {
                desiredScale = Vector3.one;
            }

        }
        else
        {
            //  infoTouchPad.gameObject.SetActive(false);
            //  infoTrigger.gameObject.SetActive(true);
            desiredScale = Vector3.zero;
        }
    }
}