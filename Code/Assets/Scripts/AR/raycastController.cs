using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Varjo.XR;

public class raycastController : MonoBehaviour
{
    Transform image = null;
    public Transform cameraPlayer;
    CustomController controller;


    public enum typeOfVisu {controller, headset, other };
    public typeOfVisu ArView;

    public enum Pointing {cursor, raycast};
    public Pointing pointingAr;

    public enum HeadsetvisuPosition { TopLeft, TopMid, TopRight,
                                      MidLeft, Mid, MidRight,
                                      BotLeft, BotMid, BotRight  };
    public HeadsetvisuPosition PositionHeadsetAr;

    public enum RaycastSizeList { small, medium, big };
    public RaycastSizeList RaycastSize;

    GameObject focusObject = null;
    private RaycastHit hit;

    void Awake()
    {
        //controller = GetComponent<VarjoExample.Controller>();
       controller = transform.GetComponent<CustomController>();
    }

    // Start is called before the first frame update
    void Start()
    {
       // VarjoMixedReality.EnableDepthEstimation();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, 2f))
        {
            if (pointingAr == Pointing.raycast)
            {
                //transform.GetComponent<LineRenderer>().SetPosition(0, transform.position);
                //transform.GetComponent<LineRenderer>().SetPosition(1, hit.point);
            }

            if (hit.transform.tag == "Data" && controller.primary2DAxisClick)
            {
                image = hit.transform;
                image.GetChild(0).gameObject.SetActive(true);


                if (focusObject == null)
                {
                    focusObject = Instantiate(hit.transform.gameObject, new Vector3(0, 0, 0), Quaternion.identity);
                    if (ArView == typeOfVisu.controller)
                    {
                        focusObject.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                        focusObject.transform.parent = transform;
                        focusObject.transform.localPosition = new Vector3(0.05f, 0.05f, 0);
                    }
                    else if (ArView == typeOfVisu.headset)
                    {
                        Vector3 pos = new Vector3(0f, 0f, 0.5f);
                        if (PositionHeadsetAr == HeadsetvisuPosition.Mid)
                        {
                            pos = new Vector3(0f, 0f, 0.5f);
                        }
                        else if (PositionHeadsetAr == HeadsetvisuPosition.MidLeft)
                        {
                            pos = new Vector3(-0.1f, 0f, 0.5f);
                        }
                        else if (PositionHeadsetAr == HeadsetvisuPosition.MidRight)
                        {
                            pos = new Vector3(0.1f, 0f, 0.5f);
                        }
                        else if (PositionHeadsetAr == HeadsetvisuPosition.TopLeft)
                        {
                            pos = new Vector3(-0.1f, 0.15f, 0.5f);
                        }
                        else if (PositionHeadsetAr == HeadsetvisuPosition.TopMid)
                        {
                            pos = new Vector3(0f, 0.15f, 0.5f);
                        }
                        else if (PositionHeadsetAr == HeadsetvisuPosition.TopRight)
                        {
                            pos = new Vector3(0.1f, 0.15f, 0.5f);
                        }
                        else if (PositionHeadsetAr == HeadsetvisuPosition.BotLeft)
                        {
                            pos = new Vector3(-0.1f, -0.15f, 0.5f);
                        }
                        else if (PositionHeadsetAr == HeadsetvisuPosition.BotMid)
                        {
                            pos = new Vector3(0f, -0.15f, 0.5f);
                        }
                        else if (PositionHeadsetAr == HeadsetvisuPosition.BotRight)
                        {
                            pos = new Vector3(0.1f, 0.15f, 0.5f);
                        }
                        focusObject.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                        focusObject.transform.parent = cameraPlayer;
                        focusObject.transform.localPosition = pos;
                    }
                }
                else
                {
                    focusObject.transform.LookAt(2* transform.position - Camera.main.transform.position);
                }
            }
            else
            {
                if (image != null)
                {
                    image.GetChild(0).gameObject.SetActive(false);
                }
                Destroy(focusObject);
                focusObject = null;
            }
        }
        else
        {
           // transform.GetComponent<LineRenderer>().SetPosition(0, transform.position);
           // transform.GetComponent<LineRenderer>().SetPosition(1, transform.position);
        }

        if (RaycastSize == RaycastSizeList.small)
        {
            transform.GetComponent<LineRenderer>().startWidth = 0.004f;
            transform.GetComponent<LineRenderer>().endWidth = 0.004f;
        }
        else if (RaycastSize == RaycastSizeList.medium)
        {
            transform.GetComponent<LineRenderer>().startWidth = 0.008f;
            transform.GetComponent<LineRenderer>().endWidth = 0.008f;
        }
        else if (RaycastSize == RaycastSizeList.big)
        {
            transform.GetComponent<LineRenderer>().startWidth = 0.016f;
            transform.GetComponent<LineRenderer>().endWidth = 0.016f;
        }
    }

    private void FixedUpdate()
    {
       
    }
}
