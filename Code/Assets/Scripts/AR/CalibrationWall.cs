using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalibrationWall : MonoBehaviour
{

    bool callibrate = false;
    public GameObject wall;
    CustomController controller;
    private bool wait, triggerDown , touchCalib = false;

    public enum CalibrationList {posX, posY, posZ, angleX, angleY, angleZ};
    public CalibrationList Calib;
    int CalibrationNb = 0;
    // Start is called before the first frame update
    void Start()
    {
        controller = transform.GetComponent<CustomController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C) && !callibrate)
        {
            print("Start Callibration !");
            callibrate = true;
        }

        else if (Input.GetKeyDown(KeyCode.C) && callibrate)
        {
            print("Callibration done !");
            callibrate = false;
            Calib = (CalibrationList)(0);
        }

        if (callibrate)
        {
            float change;
            if (!controller.triggerButton && triggerDown)
            {
                triggerDown = false;

                CalibrationNb = (CalibrationNb + 1) % 6;
                Debug.Log("change parametre" + (CalibrationList)(CalibrationNb));
                Calib = (CalibrationList)(CalibrationNb);
            }

            if (controller.triggerButton && !triggerDown)
            {
                triggerDown = true;
            }

            if (!controller.primary2DAxisTouch && touchCalib)
            {
                touchCalib = false;
            }

            if (controller.primary2DAxisTouch && !touchCalib)
            {
                touchCalib = true;

                if (controller.TrackPadPosition.x < 0)
                {
                    change = -0.05f;
                }
                else
                {
                    change = +0.05f;
                }
                Debug.Log("Change param " + Calib + " " + change);
                switch (Calib)
                {
                    case CalibrationList.posX:
                        Debug.Log("pos X");
                        wall.transform.position += new Vector3(change,0,0);
                        break;

                    case CalibrationList.posY:
                        Debug.Log("pos Y");
                        wall.transform.position += new Vector3(0, change, 0);
                        break;
                    case CalibrationList.posZ:
                        Debug.Log("pos Z");
                        wall.transform.position += new Vector3(0, 0, change);
                        break;
                    case CalibrationList.angleX:
                        Debug.Log("angle x");
                        wall.transform.Rotate(change, 0.0f, 0.0f, Space.Self);
                        break;
                    case CalibrationList.angleY:
                        Debug.Log("angle y");
                        wall.transform.Rotate( 0.0f, change, 0.0f, Space.Self);
                        break;
                    case CalibrationList.angleZ:
                        Debug.Log("angle z");
                        wall.transform.Rotate(0.0f, 0.0f, change, Space.Self);
                        break;


                    default:
                        Debug.Log("NOTHING");
                        break;
                }
            }
        }
        
    }
}
