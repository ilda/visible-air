using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ARManager : MonoBehaviour
{
    public Camera playerCamera;
    public GameObject miniWall;
    // intersecion raycast and object
    private bool m_HasPosition = false;
    private RaycastHit hit;
    private RaycastHit hithead;
    CustomController controller;
    public Transform cursorMiniwall;

    // State machine
    private bool isMoving , wait, triggerDown = false;
 
    public Vector3 forwardClic;

   //card to move

    private GameObject info;
    private GameObject wall;

    public enum Pointing { cursor, raycast };
    public enum RaycastSizeList { fullRay, mediumRay, shortRay, custom };
    public enum CircleSizeList { smallCircle, MediumCircle, BigCircle, custom };

    [Header("Type of Visu")]
    public Pointing pointingAr;

    [Header("Raycast")]
    public RaycastSizeList RaycastSize;
    public float raycastCoeff = 0;

    [Header("Circle")]
    public CircleSizeList CircleSize;
    public float circleCoeff;

    [Header("Material")]
    public Material classicLine;
    public Material dottedLine;

    public GameObject targetPos;

    [Header("Selection test")]
    public GameObject leftUpConer;
    public GameObject leftBottomConer;
    public GameObject RightTopCorner;
    public GameObject SelectionShape;
    private GameObject test;
    public bool next = false;
    public bool previous = false;
    bool resize = false;
    bool rotation = false;
    bool fromWalltominiwall = false;
    bool froMminiwalltoWall = false;


    Rendering rendering;
    public GameObject ARinformation;
    public GameObject frontArrow;
    public GameObject backArrow;
    public GameObject rightArrow;
    public GameObject leftArrow;
    public bool lookingAtController = false;

    public enum ARPosition { controller, fieldOfView};
    public ARPosition currentArPosition = ARPosition.controller;
    Vector3 positionContollerOnPress;
    Vector3 rotationControllerOnPress;


    bool touchpad;
    Vector2 touchpadInitialPos;
    Vector2 touchpadCurrentPos;
    bool interactOnMiniWall =false;

    float clunchRotation;
    float rotateValue; 
    float clunchResize;
    float resizeValue;

    public int touchId = -1;

    public enum stateContoller { nothing, pressed, waitDragShape, waitSelection, moveShape, selectionRect, waitResize, waitRotate, resize, rotate , touch, touchRotate, touchresize};
    public stateContoller currentState = stateContoller.nothing;

    float clunchTimer;
    bool waitClunch = false;

    Vector3 middleSelectionShape;
    Vector3 finalPos;
    Vector3 StartPos;
    Vector3 finalRotation;
    Vector3 StartRotation;


    float x1, x2, y1, y2;
    bool annimationToMiniWall = false;
    public GameObject trigger; 
    public Material helpTrigger;

    public GameObject miniControllerView;
    public GameObject miniControllerViewARinformation;
    public GameObject miniControllerViewfrontArrow;
    public GameObject miniControllerViewbackArrow;
    public GameObject miniControllerViewrightArrow;
    public GameObject miniControllerViewleftArrow;

    public Vector3 refPointSelectionShape;

    public enum ArHelpcontroller { menu, rotation, resize, trigger };
    public ArHelpcontroller currentARState = ArHelpcontroller.menu;

    // Start is called before the first frame update
    void Awake()
    {
        controller = transform.GetComponent<CustomController>();
    }

    private void Start()
    {
        info = GameObject.Find("Info");
        wall = GameObject.Find("Wall").transform.GetChild(0).gameObject;

        rendering = GameObject.Find("Rendering").GetComponent<Rendering>();

        leftUpConer.GetComponent<LineRenderer>().startWidth = 0.004f;
        leftUpConer.GetComponent<LineRenderer>().endWidth = 0.004f;

        leftBottomConer.GetComponent<LineRenderer>().startWidth = 0.004f;
        leftBottomConer.GetComponent<LineRenderer>().endWidth = 0.004f;

        RightTopCorner.GetComponent<LineRenderer>().startWidth = 0.004f;
        RightTopCorner.GetComponent<LineRenderer>().endWidth = 0.004f;

        SelectionShape.GetComponent<LineRenderer>().startWidth = 0.004f;
        SelectionShape.GetComponent<LineRenderer>().endWidth = 0.004f;


         test = new GameObject();
        test.transform.parent = wall.transform;
    }

    public bool ControllerInFieldOFView()
    {
        Vector3 v = playerCamera.WorldToViewportPoint(this.transform.position);
       // Debug.Log(v);
        if (v.x > 0.3 && v.x < 0.7 && v.y > 0.4 && v.y < 0.6 && v.z > 0.1 && v.z < 0.9)
        {
            lookingAtController = true;
            //Debug.Log("controller visible");
            return true;
            
        }
        else return false;
    }

    public void ChangePositionARHelp()
    {
        
       if (currentArPosition == ARPosition.controller)
        {
            /*
            //Debug.Log("conteoller to view");
            currentArPosition = ARPosition.fieldOfView;
            ARinformation.transform.parent = transform;
            ARinformation.transform.rotation = transform.rotation;
            ARinformation.transform.localPosition = new Vector3(0, 0, 0);
            ARinformation.transform.GetChild(0).localPosition = new Vector3(0, 0.1f, 0);
            */
            currentArPosition = ARPosition.fieldOfView;
            miniControllerView.SetActive(false);
            Debug.Log("false");

        }
       else
        {
            /*
           // Debug.Log("view to conteoller ");
            currentArPosition = ARPosition.controller;
            ARinformation.transform.parent = playerCamera.transform;
            ARinformation.transform.rotation = playerCamera.transform.rotation;
            ARinformation.transform.localPosition = new Vector3(0, 0, 0);
            ARinformation.transform.GetChild(0).localPosition = new Vector3(0.11f, 0f, 0.4f);

            // ARinformation.transform.LookAt(2 * transform.position - Camera.main.transform.position);
            */
            currentArPosition = ARPosition.controller;
            miniControllerView.SetActive(true);
            Debug.Log("true");
        }
    }
    
    // Update is called once per frame
    void Update()
    {
       if (ControllerInFieldOFView() && !lookingAtController)
        {
            // 
            lookingAtController = true;

        }

       if (!ControllerInFieldOFView() && lookingAtController)
        {
            ChangePositionARHelp();
            lookingAtController = false;
            Debug.Log("swap");
        }

            miniControllerView.transform.rotation = this.transform.rotation;
        
        /*
               if (currentArPosition == ARPosition.controller)
               {
                    ARinformation.transform.parent = playerCamera.transform;
                    ARinformation.transform.rotation = playerCamera.transform.rotation;
                    ARinformation.transform.localPosition = new Vector3(0,0,0);
                    ARinformation.transform.GetChild(0).localPosition = new Vector3(0.1f, 0.1f, 0.5f);
               }

                else if (currentArPosition == ARPosition.fieldOfView)
                {
                    ARinformation.transform.parent = transform;
                    ARinformation.transform.rotation = transform.rotation;
                    ARinformation.transform.localPosition = new Vector3(0.5f, 0, 0);
                    ARinformation.transform.GetChild(0).localPosition = new Vector3(0, 0.1f, 0);
                }
        */

        if (targetPos != null)
             {
            if (!targetPos.GetComponent<MeshRenderer>().isVisible)
            {
                //Debug.Log("Target not visible need ray");
              /* raycast 
                targetPos.GetComponent<LineRenderer>().positionCount = 2;
                targetPos.GetComponent<LineRenderer>().SetPosition(0, targetPos.transform.position);
                targetPos.GetComponent<LineRenderer>().SetPosition(1, transform.position);
              */
                // targetPos.GetComponent<LineRenderer>().SetPosition(2, transform.position);
            }

            else
            {
                //Debug.Log("Target visible no need for ray");
               // targetPos.GetComponent<LineRenderer>().positionCount = 0;
            }

            // halo 
            /*
            Ray rayHead = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
            if (Physics.Raycast(rayHead, out hithead)) //check if there is a hit
            {
                circleCoeff = Vector3.Distance(targetPos.transform.position, hithead.point);  //ditance target and user 
                                                                                              // Debug.Log(hithead.point);
                                                                                              // Debug.Log(circleCoeff);
                Vector3 posTarget = new Vector3(targetPos.transform.position.x, targetPos.transform.position.y, playerCamera.transform.position.z);
                targetPos.GetComponent<LineRenderer>().positionCount = 30;
                float angle = 20f;
                for (int i = 0; i < 30; i++)
                {
                    //float x = (posTarget.x + hithead.transform.position.x) / 2 + Mathf.Sin(Mathf.Deg2Rad * angle) * circleCoeff;
                    //float y = (posTarget.y + hithead.transform.position.y) / 2 + Mathf.Cos(Mathf.Deg2Rad * angle) * circleCoeff;
                    //float z = playerCamera.transform.position.z + 1;
                    float x = targetPos.transform.position.x + Mathf.Sin(Mathf.Deg2Rad * angle) * circleCoeff;
                    float y = targetPos.transform.position.y + Mathf.Cos(Mathf.Deg2Rad * angle) * circleCoeff;
                    float z = targetPos.transform.position.z - 0.1f;

                    targetPos.GetComponent<LineRenderer>().SetPosition(i, new Vector3(x, y, z));
                    angle += (360f / 29);
                }
            }
            */
            //halo headset
            /*
            Ray rayHead = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
            if (Physics.Raycast(rayHead, out hithead)) //check if there is a hit
            {
                circleCoeff = Vector3.Distance(targetPos.transform.position, hithead.point);  //ditance target and user 
                                                                                              // Debug.Log(hithead.point);
                                                                                              // Debug.Log(circleCoeff);
                float xTranslation, yTranslation = 0;
                if (targetPos.transform.position.x > hithead.point.x)
                {
                    xTranslation = circleCoeff - 0.2f;
                }
                else
                {
                    xTranslation = -circleCoeff + 0.2f;
                }

                if (targetPos.transform.position.y > hithead.point.y)
                {
                    yTranslation = circleCoeff;
                }
                else
                {
                    yTranslation = -circleCoeff;
                }
                playerCamera.GetComponent<LineRenderer>().positionCount = 30;
                Vector3 cameraPlayerPosForward = playerCamera.transform.position + playerCamera.transform.forward;
                float angle = 20f;
                for (int i = 0; i < 30; i++)
                {
                    //float x = (posTarget.x + hithead.transform.position.x) / 2 + Mathf.Sin(Mathf.Deg2Rad * angle) * circleCoeff;
                    //float y = (posTarget.y + hithead.transform.position.y) / 2 + Mathf.Cos(Mathf.Deg2Rad * angle) * circleCoeff;
                    //float z = playerCamera.transform.position.z + 1;
                    float x = cameraPlayerPosForward.x + xTranslation + Mathf.Sin(Mathf.Deg2Rad * angle) * circleCoeff;
                    float y = cameraPlayerPosForward.y + yTranslation /2 + Mathf.Cos(Mathf.Deg2Rad * angle) * circleCoeff;
                    float z = cameraPlayerPosForward.z + ;

                    playerCamera.GetComponent<LineRenderer>().SetPosition(i, new Vector3(x, y, z));
                    angle += (360f / 29);
                }
            }*/
            
            //wedge
            /*
            targetPos.GetComponent<LineRenderer>().positionCount = 3;
            targetPos.GetComponent<LineRenderer>().SetPosition(0, targetPos.transform.position);
            Vector3 cameraPlayerPosForward = playerCamera.transform.position + playerCamera.transform.forward;
            Debug.Log(cameraPlayerPosForward);
            targetPos.GetComponent<LineRenderer>().SetPosition(1,new Vector3(cameraPlayerPosForward.x, cameraPlayerPosForward.y - 0.1f, cameraPlayerPosForward.z + 0.1f));  
            targetPos.GetComponent<LineRenderer>().SetPosition(2,new Vector3(cameraPlayerPosForward.x, cameraPlayerPosForward.y + 0.1f, cameraPlayerPosForward.z + 0.1f));
            */

        }

        //Debug.Log(info.GetComponent<InfoNetwork>().coordTarget);

        if (info.GetComponent<InfoNetwork>().moveTarget)
        {
            Debug.Log(info.GetComponent<InfoNetwork>().coordTarget);

            // Vector3 coord = hit.transform.parent.transform.InverseTransformPoint(hit.point);
            // coord.x += (1 / 2f) * wall.transform.localScale.x;
            //  coord.y = Mathf.Abs(coord.y - (1 / 2f) * wall.transform.localScale.y);
            //Vector3 pos = new Vector3(info.GetComponent<InfoNetwork>().coordTarget.x / targetPos.transform.parent.parent.localScale.x, info.GetComponent<InfoNetwork>().coordTarget.y / targetPos.transform.parent.parent.localScale.y, 0) ;
            Vector3 pos = new Vector3(info.GetComponent<InfoNetwork>().coordTarget.x -0.5f, -(info.GetComponent<InfoNetwork>().coordTarget.y - 0.5f), 0) ;
            targetPos.transform.localPosition = pos;
            info.GetComponent<InfoNetwork>().moveTarget = false;
        }
       

        //Pointer
        m_HasPosition = UpdatePointer();

        if (m_HasPosition)
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                transform.GetComponent<LineRenderer>().material = dottedLine;
            }
            //Debug.Log(hit.point);
            Vector3 coord = hit.transform.parent.transform.InverseTransformPoint(hit.point);
            //Debug.Log(" after " + coord + "  " +hit.transform.parent.transform.name);
            coord.x += (1 / 2f) * wall.transform.localScale.x;
            coord.y = Mathf.Abs(coord.y - (1 / 2f) * wall.transform.localScale.y);

            if (hit.transform.name == "Canvas")
            {
                cursorMiniwall.gameObject.SetActive(true);
                cursorMiniwall.position = hit.point;
                RaycastSize = RaycastSizeList.fullRay;
                Vector3 coord2 = rendering.refStratselection;
                coord = hit.transform.parent.transform.InverseTransformPoint(hit.point);
                //Debug.Log("miniwall coord" +coord);
                //coord.x = refPointSelectionShape.x + coord.x + coord.x * (1 / 2f) * miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
                //coord.y = refPointSelectionShape.y + coord.y + coord.y - (1 / 2f) * miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y;
                coord.x = coord.x / miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
                coord.y = coord.y / miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y;

                coord.x += (1 / 2f); //* miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
                coord.y = Mathf.Abs(coord.y - (1 / 2f)); //* miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y) ;
                //Debug.Log("miniwall coord 2 " + coord);

                
                coord.x = coord2.x + (coord.x * miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x / 5.77f) ;
                coord.y = coord2.y + (coord.y * miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y / 1.685f);
               // Debug.Log("ref " + refPointSelectionShape.y + " " );
            }
            else
            {
                cursorMiniwall.gameObject.SetActive(false);
                RaycastSize = RaycastSizeList.mediumRay;
            }
            //Debug.Log(coord);
            if (pointingAr == Pointing.raycast) {
                transform.GetComponent<LineRenderer>().positionCount = 2;
                Vector3 sizeRayModif = new Vector3(0,0,0);
            if(RaycastSize == RaycastSizeList.fullRay)
            {
                sizeRayModif = hit.point + transform.forward * 0;
            }
            else if(RaycastSize == RaycastSizeList.mediumRay)
            {
                sizeRayModif = hit.point - transform.forward * 0.5f;
            }
            else if (RaycastSize == RaycastSizeList.shortRay)
            {
                sizeRayModif = transform.position + transform.forward * 1;
            }
            else if (RaycastSize == RaycastSizeList.custom)
            {
                sizeRayModif = hit.point - transform.forward * raycastCoeff;
            }
            transform.GetComponent<LineRenderer>().SetPosition(0, transform.position);
            transform.GetComponent<LineRenderer>().SetPosition(1, sizeRayModif);
                // transform.GetComponent<LineRenderer>().SetPosition(1, transform.position + transform.forward);
                // transform.GetComponent<LineRenderer>().SetPosition(1, hit.point);
            }
            else if(pointingAr == Pointing.cursor)
            {
                if (CircleSize == CircleSizeList.BigCircle)
                {
                    circleCoeff = 0.1f;
                }
                else if (CircleSize == CircleSizeList.MediumCircle)
                {
                    circleCoeff = 0.05f;
                }
                else if(CircleSize == CircleSizeList.smallCircle)
                {
                    circleCoeff = 0.01f;
                }
                
                transform.GetComponent<LineRenderer>().positionCount = 30;
                float angle = 20f;
                for (int i = 0; i < 30 ; i++)
                {
                    float x = hit.point.x + Mathf.Sin(Mathf.Deg2Rad * angle) * circleCoeff;
                    float y = hit.point.y + Mathf.Cos(Mathf.Deg2Rad * angle) * circleCoeff;
                    float z = hit.point.z;

                    transform.GetComponent<LineRenderer>().SetPosition(i, new Vector3(x, y, z));
                    angle += (360f / 29);
                }
            }
            info.GetComponent<InfoNetwork>().coordCursorOnWall = coord;
            //Debug.Log(coord);
            //Debug.Log("local hot : " + localHit + " / Hit : " + hit.point + " / position : " + hit.transform.position );
            /*
            if (transform.GetComponent<LineRenderer>().isVisible)
            {
                Debug.Log("visible ray");
            }
            */
           
        }
        else
        {
           // transform.GetComponent<LineRenderer>().positionCount = 0;
            //transform.GetComponent<LineRenderer>().SetPosition(0, transform.position);
            //transform.GetComponent<LineRenderer>().SetPosition(1, transform.position);
        }

        /*
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.GetComponent<Collider>().bounds.Contains(transform.position) && touchId == -1)
            {

                //Debug.Log("controller touch " + ms.circ.getID());
               // ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(Color.gray);
                ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Wait-Actions-resize");
               
                if ( currentState == stateContoller.nothing && controller.triggerButton)
                {
                    Debug.Log("grab");
                    currentState = stateContoller.touch;
                    touchId = ms.circ.getID();

                    ms.initPos = ms.circ.transform.localPosition;
                    ms.initRotate = ms.circ.transform.eulerAngles.z;
                    ms.initScale = ms.circ.transform.localScale.x;
                }

              /*  if (currentState == stateContoller.waitRotate && controller.triggerButton)
                {
                    Debug.Log("grab rotate");
                    currentState = stateContoller.touchRotate;
                    touchId = ms.circ.getID();
                    rotationControllerOnPress = transform.rotation.eulerAngles;
                    ms.initRotate = ms.circ.transform.eulerAngles.z;
                }

                if (currentState == stateContoller.waitResize && controller.triggerButton)
                {
                    Debug.Log("grab resize");
                    currentState = stateContoller.touchresize;
                    touchId = ms.circ.getID();
                    ms.initScale = ms.circ.transform.localScale.x;
                }
            }
            else
            {
               // ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(ms.circ.transform.GetChild(0).GetComponent<Circle>().);
            }
        }*/

        
        if ((currentState == stateContoller.touch || currentState == stateContoller.touchresize || currentState == stateContoller.touchRotate) && !controller.triggerButton)
            {
          
            if (currentState == stateContoller.touch)
            {
                Debug.Log("touch state");
                float x = (rendering.ListShapes[touchId].circ.transform.localPosition.x - rendering.ListShapes[touchId].initPos.x) /5.77f;
                float y = (rendering.ListShapes[touchId].circ.transform.localPosition.y - rendering.ListShapes[touchId].initPos.y) /1.685f;
                rendering.ListShapes[touchId].translation += new Vector3(x, y, 0);

                float r = (rendering.ListShapes[touchId].circ.transform.localEulerAngles.z - rendering.ListShapes[touchId].initRotate);
                rendering.ListShapes[touchId].rotation += r;
                Debug.Log("release" + rendering.ListShapes[touchId].translation + touchId);

                touchId = -1;
                if (interactOnMiniWall)
                {
                    Debug.Log("touch state cmd" +touchId);
                   // info.GetComponent<InfoNetwork>().translateOneShape = true;
                   //  info.GetComponent<InfoNetwork>().shapeId = touchId;
                   // info.GetComponent<InfoNetwork>().translateOneShapevalue = rendering.ListShapes[touchId].translation;
                }
                

            }
            
           // touchId = -1;
            //currentState = stateContoller.nothing;
            //send cmd if not on miniwall ?

        }

        if (touchId != -1)
        {
            if (currentState == stateContoller.touch)
            {
                rendering.ListShapes[touchId].circ.transform.position = new Vector3(transform.position.x, transform.position.y, rendering.ListShapes[touchId].circ.transform.position.z);
                rendering.ListShapes[touchId].circ.transform.localPosition = new Vector3(rendering.ListShapes[touchId].circ.transform.localPosition.x, rendering.ListShapes[touchId].circ.transform.localPosition.y, -0.02f);

                float rOnPress = rotationControllerOnPress.z;
                if (rOnPress > 0 && rOnPress < 180)
                {
                    // nothing
                }
                else
                {
                    rOnPress -= 360;
                }

                float rController = transform.rotation.eulerAngles.z;
                if (rController > 0 && rController < 180)
                {
                    // nothing
                }
                else
                {
                    rController -= 360;
                }


                rotateValue = Mathf.Abs(rController - rOnPress);
                if (rotateValue == 360)
                {
                    rotateValue = 0;
                }
                if (rotateValue > 90)
                {
                    rotateValue = 90;
                }
                if (rController < rOnPress)
                {
                    rotateValue = -rotateValue;
                }

                rotateValue = 2 * rotateValue;
                rendering.ListShapes[touchId].circ.transform.localEulerAngles = new Vector3(0, 0, rotateValue);
            }
        }
            /*
            if (currentState == stateContoller.touchRotate)
            {
                float rOnPress = rotationControllerOnPress.z;
                if (rOnPress > 0 && rOnPress < 180)
                {
                    // nothing
                }
                else
                {
                    rOnPress -= 360;
                }

                float rController = transform.rotation.eulerAngles.z;
                if (rController > 0 && rController < 180)
                {
                    // nothing
                }
                else
                {
                    rController -= 360;
                }


                rotateValue = Mathf.Abs(rController - rOnPress);
                if (rotateValue == 360)
                {
                    rotateValue = 0;
                }
                if (rotateValue > 90)
                {
                    rotateValue = 90;
                }
                if (rController < rOnPress)
                {
                    rotateValue = -rotateValue;
                }

                info.GetComponent<InfoNetwork>().rotate = true;
                info.GetComponent<InfoNetwork>().rotatevalue = 2 * rotateValue;
            }
            if(currentState == stateContoller.touchresize)
            {

            }
           
        }
        */
        if (currentState == stateContoller.resize || currentState == stateContoller.rotate ||
            currentState == stateContoller.waitResize || currentState == stateContoller.waitRotate)
        {
            transform.GetComponent<LineRenderer>().enabled = false;
        }
        else
        {
            transform.GetComponent<LineRenderer>().enabled = true;
        }
       // Debug.Log(Time.time - clunchTimer +" "+ clunchTimer);
         if (waitClunch  && Time.time - clunchTimer >= 1 && (currentState == stateContoller.rotate || currentState == stateContoller.resize))
        {
            Debug.Log("no clunch send modification");
            clunchResize = clunchRotation =rotateValue =resizeValue = 0;
            waitClunch = false;
            currentState = stateContoller.nothing;
            
            wait = triggerDown = resize = rotation = false;
            cursorMiniwall.GetComponent<Image>().sprite = Resources.Load<Sprite>("icon-plus-bleu");

            if (!interactOnMiniWall)
            {
               // froMminiwalltoWall = true;
                info.GetComponent<InfoNetwork>().released = true;
                info.GetComponent<InfoNetwork>().startSelection = false;
            }
            transform.GetComponent<LineRenderer>().startColor = transform.GetComponent<LineRenderer>().endColor = Color.white;
            // ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Actions");
             //ArHelpcontroller currentARState = ArHelpcontroller.menu;
            frontArrow.SetActive(false);
            backArrow.SetActive(false);
            rightArrow.SetActive(false);
            leftArrow.SetActive(false);
            ARinformation.SetActive(true);


            miniControllerViewfrontArrow.SetActive(false);
            miniControllerViewbackArrow.SetActive(false);
            miniControllerViewrightArrow.SetActive(false);
            miniControllerViewleftArrow.SetActive(false);
            miniControllerViewARinformation.SetActive(true);

        }
        if (!controller.triggerButton && triggerDown && (currentState != stateContoller.touch || currentState != stateContoller.touchresize|| currentState != stateContoller.touchRotate))
        {
            if (currentState == stateContoller.rotate || currentState == stateContoller.resize)
            { 
                waitClunch = true;
                clunchTimer = Time.time;
                triggerDown = false;
                clunchRotation = rotateValue;
                clunchResize = resizeValue;
                info.GetComponent<InfoNetwork>().waitClunch = true;
                cursorMiniwall.GetChild(0).GetComponent<CursorTimer>().StartTimer(0.05f);
                if (interactOnMiniWall)
                {
                  //  r = 0;
                   // clunchR = 0;
                   // Debug.Log(" r = 0");
                }
            }
           
            else
            {
                touchId = -1;
                //Debug.Log(Mathf.Abs(x1 - x2) + " " + Mathf.Abs(y1 - y2));
                //froMminiwalltoWall = true;
                miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2( Mathf.Abs(x1 - x2), Mathf.Abs(y1 - y2)) ;
                currentState = stateContoller.nothing;
                wait = triggerDown = resize = rotation = false;
                info.GetComponent<InfoNetwork>().released = true;
                transform.GetComponent<LineRenderer>().startColor = transform.GetComponent<LineRenderer>().endColor = Color.white;

               // leftUpConer.GetComponent<LineRenderer>().positionCount = 0;
                //leftBottomConer.GetComponent<LineRenderer>().positionCount = 0;
                //RightTopCorner.GetComponent<LineRenderer>().positionCount = 0;

                info.GetComponent<InfoNetwork>().startSelection = false;
            }

            leftUpConer.SetActive(false);
            leftBottomConer.SetActive(false);
            RightTopCorner.SetActive(false);
            SelectionShape.SetActive(false);

        }

        if (controller.triggerButton && !triggerDown && currentState != stateContoller.touch)
        {
            if(currentState == stateContoller.waitRotate || currentState == stateContoller.waitResize ||
                currentState == stateContoller.rotate || currentState == stateContoller.resize)
            {
                info.GetComponent<InfoNetwork>().clunch = true;
            }
            else if (touchId != -1)
            {
                currentState = stateContoller.touch;

                Rendering.mShape ms = rendering.ListShapes[touchId];
                ms.initPos = ms.circ.transform.localPosition;
                ms.initRotate = ms.circ.transform.eulerAngles.z;
                ms.initScale = ms.circ.transform.localScale.x;
            }
            else
            {
                currentState = stateContoller.pressed;
                info.GetComponent<InfoNetwork>().pressed = true;
               // ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Actions");
              //  ArHelpcontroller currentARState = ArHelpcontroller.menu;
                frontArrow.SetActive(false);
                backArrow.SetActive(false);
                rightArrow.SetActive(false);
                leftArrow.SetActive(false);
                ARinformation.SetActive(true);


                miniControllerViewfrontArrow.SetActive(false);
                miniControllerViewbackArrow.SetActive(false);
                miniControllerViewrightArrow.SetActive(false);
                miniControllerViewleftArrow.SetActive(false);
                miniControllerViewARinformation.SetActive(true);
            }
            waitClunch = false;
            wait = triggerDown = true;
            positionContollerOnPress = transform.position;
            rotationControllerOnPress = transform.rotation.eulerAngles;
            forwardClic = transform.forward;
            
            transform.GetComponent<LineRenderer>().startColor = transform.GetComponent<LineRenderer>().endColor = Color.green;
 
            Debug.Log("pressed");

            leftUpConer.GetComponent<LineRenderer>().positionCount = 2;
            leftUpConer.GetComponent<LineRenderer>().SetPosition(0, transform.position);
            leftUpConer.GetComponent<LineRenderer>().SetPosition(1, hit.point);
            leftUpConer.GetComponent<LineRenderer>().startColor = leftUpConer.GetComponent<LineRenderer>().endColor = Color.blue;
            leftUpConer.SetActive(false); 

            leftBottomConer.GetComponent<LineRenderer>().positionCount = 2;
            leftBottomConer.GetComponent<LineRenderer>().SetPosition(0, transform.position);
            leftBottomConer.GetComponent<LineRenderer>().SetPosition(1, hit.point);
            leftBottomConer.GetComponent<LineRenderer>().startColor = leftBottomConer.GetComponent<LineRenderer>().endColor = Color.blue;
            leftBottomConer.SetActive(false);

            RightTopCorner.GetComponent<LineRenderer>().positionCount = 2;
            RightTopCorner.GetComponent<LineRenderer>().SetPosition(0, transform.position);
            RightTopCorner.GetComponent<LineRenderer>().SetPosition(1, hit.point);
            RightTopCorner.GetComponent<LineRenderer>().startColor = RightTopCorner.GetComponent<LineRenderer>().endColor = Color.blue;
            RightTopCorner.SetActive(false);

            SelectionShape.GetComponent<LineRenderer>().positionCount = 4;
            SelectionShape.GetComponent<LineRenderer>().SetPosition(0, hit.point);
            SelectionShape.GetComponent<LineRenderer>().startColor = SelectionShape.GetComponent<LineRenderer>().endColor = Color.blue;
            SelectionShape.SetActive(false);

            refPointSelectionShape = hit.point;
            

            leftUpConer.transform.position = hit.point;
        }

        if (info.GetComponent<InfoNetwork>().startSelection && m_HasPosition)
        {
            currentState = stateContoller.selectionRect;
            transform.GetComponent<LineRenderer>().startColor = transform.GetComponent<LineRenderer>().endColor = Color.blue;
            leftUpConer.SetActive(true);
            leftBottomConer.SetActive(true);
            RightTopCorner.SetActive(true);
            SelectionShape.SetActive(true);
            // leftBottomConer.transform.position = new Vector3(leftUpConer.transform.position.x ,hit.point.y, 0);
            //RightTopCorner.transform.position = new Vector3(hit.point.x ,leftUpConer.transform.position.x , 0);

            x1 = leftUpConer.GetComponent<LineRenderer>().GetPosition(1).x;
            x2 = hit.point.x;

            y1 = leftUpConer.GetComponent<LineRenderer>().GetPosition(1).y;
            y2 = hit.point.y;
            leftUpConer.GetComponent<LineRenderer>().SetPosition(0, transform.position);
            //leftUpConer.GetComponent<LineRenderer>().SetPosition(1, new Vector3(leftUpConer.GetComponent<LineRenderer>().GetPosition(1).x, leftUpConer.GetComponent<LineRenderer>().GetPosition(1).y , hit.point.z));

            leftBottomConer.GetComponent<LineRenderer>().SetPosition(0, transform.position);
            leftBottomConer.GetComponent<LineRenderer>().SetPosition(1, new Vector3(leftUpConer.GetComponent<LineRenderer>().GetPosition(1).x, hit.point.y, leftUpConer.GetComponent<LineRenderer>().GetPosition(1).z)); //hit.point.z));

            RightTopCorner.GetComponent<LineRenderer>().SetPosition(0, transform.position);
            RightTopCorner.GetComponent<LineRenderer>().SetPosition(1, new Vector3( hit.point.x, leftUpConer.GetComponent<LineRenderer>().GetPosition(1).y, hit.point.z));

            SelectionShape.GetComponent<LineRenderer>().SetPosition(1, new Vector3(leftUpConer.GetComponent<LineRenderer>().GetPosition(1).x, hit.point.y, leftUpConer.GetComponent<LineRenderer>().GetPosition(1).z));
            SelectionShape.GetComponent<LineRenderer>().SetPosition(2, hit.point);
            SelectionShape.GetComponent<LineRenderer>().SetPosition(3, new Vector3(hit.point.x, leftUpConer.GetComponent<LineRenderer>().GetPosition(1).y, hit.point.z));
            SelectionShape.GetComponent<LineRenderer>().SetPosition(0, new Vector3(leftUpConer.GetComponent<LineRenderer>().GetPosition(1).x, leftUpConer.GetComponent<LineRenderer>().GetPosition(1).y, leftUpConer.GetComponent<LineRenderer>().GetPosition(1).z)) ;// hit.point.z));


            middleSelectionShape = new Vector3((refPointSelectionShape.x + hit.point.x) / 2,
                                               (refPointSelectionShape.y + hit.point.y) / 2,
                                               (refPointSelectionShape.z + hit.point.z) / 2
                                               );

            leftBottomConer.transform.position = new Vector3(leftUpConer.transform.position.x, hit.point.y, leftUpConer.transform.position.z);


            //Debug.Log(new Vector3(leftUpConer.GetComponent<LineRenderer>().GetPosition(1).x, hit.point.y, leftUpConer.GetComponent<LineRenderer>().GetPosition(1).z) + " / " + leftUpConer.transform.position);
        }
       
        /*if (transform.rotation.eulerAngles.z > 100 && transform.rotation.eulerAngles.z < 180 && !next)
        {
            Debug.Log("Next !");
            next = true;
            info.GetComponent<InfoNetwork>().next = true;
        }

        if (transform.rotation.eulerAngles.z > 180 && transform.rotation.eulerAngles.z < 260 && !previous)
        {
            Debug.Log("Previous !");
            previous = true;
            info.GetComponent<InfoNetwork>().previous = true;
        }
        */

        if (((transform.rotation.eulerAngles.z > 0 && transform.rotation.eulerAngles.z < 100) || transform.rotation.eulerAngles.z >260 ) && (previous || next))
        {
            Debug.Log("RESET !");
            previous = false;
            next = false;
        }

       if(currentState == stateContoller.waitResize && triggerDown )//&& !resize && Mathf.Abs(Mathf.Abs(positionContollerOnPress.z) - Mathf.Abs(transform.position.z) ) > 0.1f)
        {
            resize = true;
            //Debug.Log("resize selection");
            currentState = stateContoller.resize;
            //ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Resize");
           // currentARState = ArHelpcontroller.resize;
            this.gameObject.GetComponent<CustomController>().help = false;
            frontArrow.SetActive(true);
            backArrow.SetActive(true);
            rightArrow.SetActive(false);
            leftArrow.SetActive(false);
            ARinformation.SetActive(false);


            miniControllerViewfrontArrow.SetActive(true);
            miniControllerViewbackArrow.SetActive(true);
            miniControllerViewrightArrow.SetActive(false);
            miniControllerViewleftArrow.SetActive(false);
            miniControllerViewARinformation.SetActive(false);
        }
       

        if (currentState == stateContoller.resize && triggerDown) // mouvement max  -> 25 cm 
        {
            resizeValue = Mathf.Abs(transform.position.z - positionContollerOnPress.z );
            if(resizeValue * 100 > 25)
            {
                resizeValue = 25;
            }
            else
            {
                resizeValue = resizeValue * 100;
            }
            if (transform.position.z < positionContollerOnPress.z)
            {
                resizeValue = -resizeValue;
                resizeValue += clunchResize;
                //Debug.Log("reduce by " + r * 100/ 25f + "%");
                info.GetComponent<InfoNetwork>().resize = true;
                info.GetComponent<InfoNetwork>().resizeValue = 3 * resizeValue; // 2/  (-r);
            }
            else
            {
                // Debug.Log("augmente by " + r * 100 / 25f + "%");
                //r = r + clunchR;
                resizeValue += clunchResize;
                info.GetComponent<InfoNetwork>().resize = true;
                info.GetComponent<InfoNetwork>().resizeValue = 3* resizeValue;
            }
        }
        if (currentState == stateContoller.waitRotate && triggerDown)// && !rotation && Mathf.Abs(Mathf.Abs(rotationControllerOnPress.z) - Mathf.Abs(transform.rotation.eulerAngles.z)) > 5f)
        {
        }

        if (currentState == stateContoller.waitRotate && triggerDown )// && !rotation && Mathf.Abs(Mathf.Abs(rotationControllerOnPress.z) - Mathf.Abs(transform.rotation.eulerAngles.z)) > 5f)
        {
            rotation = true;
           // Debug.Log("rotation selection");
            currentState = stateContoller.rotate;
            //ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("RotateHelp");
            //ArHelpcontroller currentARState = ArHelpcontroller.rotation;
            this.gameObject.GetComponent<CustomController>().help = false;
            Debug.Log("rotate");
            frontArrow.SetActive(false);
            backArrow.SetActive(false);
            rightArrow.SetActive(true);
            leftArrow.SetActive(true);
            ARinformation.SetActive(false);


            miniControllerViewfrontArrow.SetActive(false);
            miniControllerViewbackArrow.SetActive(false);
            miniControllerViewrightArrow.SetActive(true);
            miniControllerViewleftArrow.SetActive(true);
            miniControllerViewARinformation.SetActive(false);
        }
        if (currentState == stateContoller.rotate && triggerDown) // mouvement max  -> 25 cm 
        {
            float rOnPress = rotationControllerOnPress.z;
            if(rOnPress > 0 &&  rOnPress< 180)
            {
                // nothing
            }
            else
            {
                rOnPress -= 360;
            }
            
            float rController = transform.rotation.eulerAngles.z;
            if (rController > 0 && rController < 180)
            {
                // nothing
            }
            else
            {
                rController -= 360;
            }
            
            
            rotateValue = Mathf.Abs(rController - rOnPress);
            if (rotateValue == 360)
            {
                rotateValue = 0;
            }
            if (rotateValue > 90)
            {
                rotateValue = 90;
            }
            if (rController < rOnPress)
            {
                rotateValue = -rotateValue;
            }
            // Debug.Log("roatate by " + r + "�");
            rotateValue += clunchRotation;
            info.GetComponent<InfoNetwork>().rotate = true;
            info.GetComponent<InfoNetwork>().rotatevalue = 2 * rotateValue;
           // Debug.Log(2 * r);
        }

        if (controller.primary2DAxisTouch && !touchpad)
        {
            touchpad = true;
            touchpadInitialPos = controller.TrackPadPosition;
            touchpadCurrentPos = controller.TrackPadPosition;
        }

        if (controller.primary2DAxisTouch)
        {
            touchpadCurrentPos = controller.TrackPadPosition;
            //Debug.Log(touchpadCurrentPos);
        }
        //Debug.Log(touchpadCurrentPos);
        if (!controller.primary2DAxisTouch && touchpad)
        {
            touchpad = false;
            /*  if (touchpadCurrentPos.x > touchpadInitialPos.x + 0.5f)
              {
                  Debug.Log("rotate mode");
                  currentState = stateContoller.waitRotate;
                  //send wait for rotate
                  info.GetComponent<InfoNetwork>().waitrotate = true;
                  //fromWalltominiwall = true;
                  ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Wait-Actions");
              }
              if (touchpadCurrentPos.x < touchpadInitialPos.x - 0.5f )
              {
                  Debug.Log("resize mode");

                  currentState = stateContoller.waitResize;
                  info.GetComponent<InfoNetwork>().waitresize = true;
                  //send wait for resize
                  //fromWalltominiwall = true;
                  ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Wait-Actions");
              }
            */
            Debug.Log(touchpadCurrentPos);
            if (touchpadCurrentPos.x > 0.7)
            {
                Debug.Log("rotate");
                currentState = stateContoller.waitRotate;
                //send wait for rotate
                info.GetComponent<InfoNetwork>().waitrotate = true;
                //fromWalltominiwall = true;
                //ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Wait-Actions-rotation");
                ArHelpcontroller currentARState = ArHelpcontroller.trigger;
                // trigger.GetComponent<MeshRenderer>().material = helpTrigger;
                this.gameObject.GetComponent<CustomController>().help = true;
                ARinformation.SetActive(false);
                cursorMiniwall.GetComponent<Image>().sprite = Resources.Load<Sprite>("rotate 1");
            }
            else if (touchpadCurrentPos.x < -0.7)
            {
                Debug.Log("resize");
                currentState = stateContoller.waitResize;
                info.GetComponent<InfoNetwork>().waitresize = true;
                ARinformation.SetActive(false);
                //send wait for resize
                //fromWalltominiwall = true;
                // ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Wait-Actions-resize");
                ArHelpcontroller currentARState = ArHelpcontroller.trigger;
                cursorMiniwall.GetComponent<Image>().sprite = Resources.Load<Sprite>("resize1");
               this.gameObject.GetComponent<CustomController>().help = true;
            }
            else if (touchpadCurrentPos.y > 0.5)
            {
                froMminiwalltoWall = true;
                Debug.Log("to wall");
               
            }
            else if (touchpadCurrentPos.y < -0.7)
            {
                Debug.Log("to mini wall ARManager");
                currentState = stateContoller.nothing;
                info.GetComponent<InfoNetwork>().toMiniWall = true;
                //send wait for resize
                //fromWalltominiwall = true;
                //ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Actions");
               // ArHelpcontroller currentARState = ArHelpcontroller.menu;
                frontArrow.SetActive(false);
                backArrow.SetActive(false);
                rightArrow.SetActive(false);
                leftArrow.SetActive(false);
                ARinformation.SetActive(true);

                miniControllerViewfrontArrow.SetActive(false);
                miniControllerViewbackArrow.SetActive(false);
                miniControllerViewrightArrow.SetActive(false);
                miniControllerViewleftArrow.SetActive(false);
                miniControllerViewARinformation.SetActive(true);
            }
           // Debug.Log(touchpadCurrentPos);
        }
        if (froMminiwalltoWall) 
        {
            StartCoroutine(toWall());
            froMminiwalltoWall = false;
        }
        if (rendering.toWall)
        {
            StartCoroutine(toWall());
            rendering.toWall = false;

        }

        if (rendering.toMiniWall)
        {
            StartCoroutine(ToMiniWall());
            rendering.toMiniWall = false;
        }

        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.onMiniWall && ms.circ.newpos)
            {
                Vector3 pos;
                pos.x = (ms.circ.newposMiniwall.x - 0.5f) * miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
                pos.y = -((1 - ms.circ.newposMiniwall.y) - 0.5f) * miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y;
                pos.z = 0;
                ms.circ.transform.localPosition = pos;
            }
        }
    }


    void OnCollisionEnter(Collision collision)
    {
        //Output the Collider's GameObject's name
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.gameObject == collision.collider.gameObject)
            {
               // Debug.Log("Enter " + collision.collider.name);
            }
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (currentState != stateContoller.touch)
        {
            //Output the Collider's GameObject's name
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.gameObject == collision.collider.gameObject)// && touchId == -1)
                {
                    //Debug.Log(" Stay " + collision.collider.name+ " id :" + ms.circ.getID());
                    touchId = ms.circ.getID();
                }
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        //Output the Collider's GameObject's name
        
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.gameObject == collision.collider.gameObject)
            {
                //Debug.Log("Exit " + collision.collider.name);
                //touchId = -1;
            }
        }
    }

    private IEnumerator toWall()
    {
        StartRotation.z = 0;
        yield return StartCoroutine(LerpPosition(finalPos, StartPos, finalRotation,StartRotation,  1f));

        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.onMiniWall)
            {
                ms.circ.transform.gameObject.SetActive(false);
                //   Vector3 pos = ms.circ.transform.localPosition;
                // pos.x = pos.x / miniWall.transform.localScale.x * wall.transform.parent.localScale.x;
                //pos.y = pos.y / miniWall.transform.localScale.y * wall.transform.parent.localScale.y;
                Quaternion r = ms.circ.transform.localRotation;

                //Debug.Log("pos.x " + pos.x + " wall scale " + wall.transform.parent.localScale.x + " mini wall scale " + miniWall.transform.localScale.x);
                ms.circ.transform.parent = rendering.canvas.transform;
                //ms.circ.transform.localPosition = pos;
                ms.circ.transform.rotation = ms.circ.transform.parent.rotation;
                ms.circ.transform.localRotation = r;
                ms.circ.onMiniWall = false;

                //ms.circ.xshape = (ms.circ.transform.position.x / miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x)  + 0.5f;
                //ms.circ.yshape = -(-1 -(ms.circ.transform.position.y / miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y) + 0.5f);

            }

        }
        miniWall.transform.GetChild(0).gameObject.SetActive(false);
        Debug.Log("onWall");
        interactOnMiniWall = false;
        currentState = stateContoller.nothing;
        wait = triggerDown = resize = rotation = false;
        info.GetComponent<InfoNetwork>().released = true;
        info.GetComponent<InfoNetwork>().toWall = true;
        transform.GetComponent<LineRenderer>().startColor = transform.GetComponent<LineRenderer>().endColor = Color.white;

        info.GetComponent<InfoNetwork>().startSelection = false;
       // ARinformation.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Help-Actions");
        ArHelpcontroller currentARState = ArHelpcontroller.menu;

        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.translation != new Vector3(0, 0, 0) || ms.rotation != 0)
            {
                Debug.Log("Id :" + ms.circ.getID() + "translation " + ms.translation);
                int id = ms.circ.getID();
                Vector3 translationShape = ms.translation;
                //CustomCommand.Cmd_TranslateOneShape(id, translationShape);
                info.GetComponent<InfoNetwork>().translateOneShape = true;
                info.GetComponent<InfoNetwork>().shapeId.Add(ms.circ.getID()); ;
                info.GetComponent<InfoNetwork>().translateOneShapevalue.Add(ms.translation);
                info.GetComponent<InfoNetwork>().rotateOneShapevalue.Add(ms.rotation);
                ms.translation = new Vector3(0, 0, 0);
                ms.rotation = 0;
                //yield return new WaitForSeconds(0.1f);
            }
        }


    }

    private IEnumerator ToMiniWall()
    {
        miniWall.transform.GetChild(0).gameObject.SetActive(true);
        interactOnMiniWall = true;
        miniWall.transform.parent = playerCamera.transform;
        miniWall.transform.rotation = new Quaternion(0, 0, 0, 0);
        miniWall.transform.localPosition = new Vector3(0, 0, 0.75f);
        //Debug.Log("start : " + miniWall.transform.position);
        finalPos = miniWall.transform.position;
        StartPos = middleSelectionShape;

        StartRotation = rendering.canvas.transform.eulerAngles;
        finalRotation = miniWall.transform.eulerAngles;
        annimationToMiniWall = true;

        miniWall.transform.GetChild(0).GetComponent<BoxCollider>().size = new Vector3(miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x,
                                                                                    miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y, 0.1f);
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.onMiniWall)
            {
                Vector3 pos;
                pos.x = (ms.circ.xshape - 0.5f) * miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
                pos.y = -((1 - ms.circ.yshape) - 0.5f) * miniWall.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y;
                pos.z = 0;
                Quaternion r = ms.circ.transform.localRotation;

                // Debug.Log("pos.x " + pos.x + " wall scale " + wall.transform.parent.localScale.x + " mini wall scale " + miniWall.transform.localScale.x);
                ms.circ.transform.parent = miniWall.transform.GetChild(0);
                ms.circ.transform.localPosition = pos;
                ms.circ.transform.rotation = ms.circ.transform.parent.rotation;
                ms.circ.transform.localRotation = r;
                ms.circ.transform.gameObject.SetActive(true);
            }
        }
        miniWall.transform.parent = null;

        finalRotation.z = 0;
        yield return StartCoroutine(LerpPosition(StartPos, finalPos, StartRotation, finalRotation,  1));
    }


    private bool UpdatePointer()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit)) //check if there is a hit
        {
            return true;
        }
        return false;
    }

   
    IEnumerator LerpPosition(Vector3 startPosition, Vector3 endPosition , Vector3 startrotation , Vector3 endRotation, float duration)
    {
        Debug.Log("start soration " + startrotation + " / end rotation " + endRotation);
        startrotation.x = endRotation.x;
        //endRotation.z = 0;
        Debug.Log("start soration 2 " + startrotation + " / end rotation " + endRotation);
        float time = 0;
        while (time < duration)
        {
            miniWall.transform.position = Vector3.Lerp(startPosition, endPosition, time / duration);
            miniWall.transform.eulerAngles = Vector3.Lerp(startrotation, endRotation, time / duration);
            Debug.Log(Vector3.Lerp(startrotation, endRotation, time / duration));
            time += Time.deltaTime;
            yield return null;
        }
        miniWall.transform.position = endPosition;
    }   
}
