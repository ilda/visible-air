using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorTimer : MonoBehaviour
{
    public float timerf = 0;
    float t = 1;
    bool timer = false;
    float circleCoeff = 0.4f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            StartTimer(0.4f);
        }
        if (timer)
        {
            t = Time.time - timerf;
            if (t > 1)
            {
                t = 1;
            }
            if (t < 0)
            {
                t = 0;
            }

            if (t < 1 && t >= 0.5)
            {
                //red
                transform.GetComponent<LineRenderer>().startColor = transform.GetComponent<LineRenderer>().endColor = Color.red;
            }
            else if (t >= 0.25)
            {
                //orange
                transform.GetComponent<LineRenderer>().startColor = transform.GetComponent<LineRenderer>().endColor = Color.yellow;
            }
            else
            {
                // green
                transform.GetComponent<LineRenderer>().startColor = transform.GetComponent<LineRenderer>().endColor = Color.green;
            }

            //float circleCoeff = 0.3f;  //ditance target and user 
            int nbpoint = (int)(30 * (1 - t));
            transform.GetComponent<LineRenderer>().positionCount = nbpoint;
            float angle = 20f;
            for (int i = 0; i < nbpoint; i++)
            {
                float x = transform.position.x + Mathf.Sin(Mathf.Deg2Rad * angle) * circleCoeff;
                float y = transform.position.y + Mathf.Cos(Mathf.Deg2Rad * angle) * circleCoeff;
                float z = transform.position.z;//0.1f;
                angle += (360f / 29);
                transform.GetComponent<LineRenderer>().SetPosition(i, new Vector3(x, y, z));
            }
        }
        else
        {
            transform.GetComponent<LineRenderer>().positionCount = 0;
        }

    }

    public void StartTimer(float cCoeff)
    {
        timerf = Time.time;
        timer = true;
        circleCoeff = cCoeff;
    }

    public void stopTimer() 
    {
        timer = false;
    }
}
