using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public enum NetworkDevice
{
    SERVERWALL,
    AR
};

public class MyNetworkManager : NetworkManager {
 
    Setup setup;
    InputManager im;

    public NetworkDevice networkDevice;

    void Start () {
        Debug.Log("Start NetworkManager ");
        /*
        if (this.networkDevice == NetworkDevice.SERVERWALL)
        {
            StartServer();
            //nc.RegisterHandler(MsgType.Connect, OnConnected);

            GameObject g = Instantiate(ServerCommand);
            NetworkServer.Spawn(g);
        }
        else
        {
            StartClient();
        }
        */
    }

 	// ------ on the server
 	// client connect to the server
 	public override void OnServerConnect(NetworkConnection conn)
    {
    	base.OnServerConnect(conn);
        Debug.Log("OnServerConnect " + conn);
    }

    public override void OnServerReady(NetworkConnection conn) {

        NetworkServer.SetClientReady(conn);
        base.OnServerReady( conn);

        Debug.Log("Client is set to the ready state (ready to receive state updates): " + conn);

    }

    private int count = 0;

    private void AllClientsConnected(){
        im =  GameObject.Find("Main Camera").GetComponent<InputManager>();
        Debug.Log("WE HAVE ALL THE CLIENTS: AllClientsConnected");
        if (im != null){
            Debug.Log("WE HAVE THE INPUT MANAGER");
            im.AllClientsConnected();       
        }
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
        Debug.Log("Client has requested to get his player added to the game " + playerControllerId);
        
        count++;
        if (setup == null){
            Debug.Log("Get A CLIENTS " + count);
            setup = GameObject.Find("NetworkManager").GetComponent<Setup>();
        }
        else {
            Debug.Log("Get A CLIENTS (setup!=null) " + count);
        }
        if (setup.num_clients > 0 && count == setup.num_clients){
            Debug.Log("WE HAVE ALL THE CLIENTS");
            Invoke ( "AllClientsConnected", 2);
        }
        
        GameObject player = (GameObject)Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        //NetworkServer.SpawnWithClientAuthority(player, conn);
        Debug.Log(conn);
    }

    // client disconnect to the server
    public override void OnServerDisconnect (NetworkConnection conn)
    {
    	base.OnServerDisconnect(conn);
        Debug.Log("OnServerDisconnect " + conn);
    }
 
 	// ---- on the client
 	// client connect to the server
 	public override void OnClientConnect(NetworkConnection conn)
	{
		base.OnClientConnect(conn);
    	Debug.Log("OnClientConnect");
	}

    public override void OnClientDisconnect (NetworkConnection conn)
    {
    	base.OnClientDisconnect(conn);
        Debug.Log("OnClientDisconnect "+conn);
        Application.Quit();
    }

    public void OnConnected(UnityEngine.Networking.NetworkMessage msg)
    {
        Debug.Log("New player " + msg);
    }
}