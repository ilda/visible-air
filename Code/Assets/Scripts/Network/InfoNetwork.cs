using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoNetwork : MonoBehaviour
{
    public Vector3 coordCursorOnWall;
    public bool startSelection;
    public bool released;
    public bool pressed;
    public bool next;
    public bool previous;

    public bool resize;
    public bool waitresize;
    public float resizeValue;
    
    public bool rotate;
    public bool waitrotate;
    public float rotatevalue;

    public bool waitClunch;
    public bool clunch;

    public bool toMiniWall;
    public bool toWall;

    public bool translateOneShape;
    public List<Vector3> translateOneShapevalue;
    public List<float> rotateOneShapevalue;
    public List<int> shapeId;



    public Vector3 coordTarget;
    public bool moveTarget = false;
}
