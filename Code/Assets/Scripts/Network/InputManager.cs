using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Timers;


[NetworkSettings(channel = 1, sendInterval = 0.01f)]
public class InputManager : NetworkBehaviour
{

    //[SyncVar]
    //public Vector3 mouse;
    //bool isServer = true;
    Setup setup;
    Wall wall;
    Rendering rendering;

    static private int cursorHW = 16;
    static private int cursorT = 1;
    static private int cursorL = 1;
    //private Texture2D cursor;

    public List<List<int>> ListSelectionsShapes = new List<List<int>>();
    public List<int> currentselection = new List<int>();
    public int currentSlectionInt = -1;

    public GameObject myPrefab;


    // -------------------------------------------------------------
    // client cursor
    public class CCursor
    {
        public float x, y;
        public Texture2D tex;
        public Color color;
        public Transform tranformCursor;

        public CCursor(float x_, float y_, Color c)
        {
            x = x_;
            y = y_;
            tex = CursorsTex.SimpleCursor(c, Color.black, cursorHW, cursorT, cursorL);
            // tex = RingsTex.RingWithArrow();
            //tex = RingsTex.ResizeArrow();

            //tex = RotateCursor.simpleRotateCursor();
            color = c;
            tranformCursor = new GameObject().transform;
        }
        public void move(float x_, float y_)
        {
            x = x_;
            y = y_;
        }

        public void ChangeColor(Color c)
        {
            tex = CursorsTex.SimpleCursor(c, Color.black, cursorHW, cursorT, cursorL);
        }

        public void Changetex(string t)
        {

            if (t == "rotate")
            {
                tex = RingsTex.RingWithArrow();
            }
            else if (t == "resize")
            {
                tex = RingsTex.ResizeArrow();
            }
            else
            {
                tex = CursorsTex.SimpleCursor(this.color, Color.black, cursorHW, cursorT, cursorL);
            }

        }
    };

    public Dictionary<int, CCursor> _ccursors;

    // -------------------------------------------------------------
    // cursor rendering
    void OnGUI()
    {
        if (setup.myname != "ArClient")
        {
            foreach (CCursor cc in _ccursors.Values)
            {
                float x, y, x1;
                if (isServer)
                {
                    x = cc.x * Screen.width;
                    y = cc.y * Screen.height;
                    // normally the server is not xinerama ... just for testing ...
                    x1 = x; //cc.x*(Screen.width- wall.getBezelWidth()) - Screen.width/2;
                }
                else
                {
                    x = -setup.x + cc.x * wall.getWallWidth();
                    y = -setup.y + cc.y * wall.getWallHeight();
                    x1 = -setup.x - wall.getBezelWidth() + cc.x * wall.getWallWidth() - Screen.width / 2;
                }
                if (setup.DoDrawUnderXineramaBezel())
                {
                    GUI.BeginGroup(new Rect(0, 0, Screen.width / 2, Screen.height));
                    GUI.DrawTexture(new Rect(x - cursorHW, y - cursorHW, 2 * cursorHW, 2 * cursorHW), cc.tex);
                    GUI.EndGroup();
                    GUI.BeginGroup(new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height));
                    GUI.DrawTexture(new Rect(x1 - cursorHW, y - cursorHW, 2 * cursorHW, 2 * cursorHW), cc.tex);
                    GUI.EndGroup();
                }
                else
                {
                    GUI.DrawTexture(new Rect(x - cursorHW, y - cursorHW, 2 * cursorHW, 2 * cursorHW), cc.tex);
                }
            }
        }
    }

    void mylocalCreateCCurqor(int uid, float x, float y, Color c)
    {
        if (setup.myname != "ArClient")
        {
            if (!_ccursors.ContainsKey(uid))
            {
                Debug.Log("RpcCreateCCursor" + uid + " " + x + " " + y + " ");
                CCursor cc = new CCursor(x, y, c);
                myPrefab = Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                myPrefab.transform.parent = cc.tranformCursor;
                _ccursors.Add(uid, cc);
            }
        }
    }
    [ClientRpc]
    void RpcCreateCCursor(int uid, float x, float y, Color c)
    {
        Debug.Log("RpcCreateCCursor" + uid + " " + x + " " + y + " ");
        CCursor cc = new CCursor(x, y, c);
        myPrefab = Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        myPrefab.transform.parent = cc.tranformCursor;
        _ccursors.Add(uid, cc);
    }

    [ClientRpc]
    void RpcMoveCCursorCreate(int uid, float x, float y, Color c)
    {
        if (!_ccursors.ContainsKey(uid))
        {
            Debug.Log("RpcCreateCCursor" + uid + " " + x + " " + y + " ");
            CCursor cc = new CCursor(x, y, c);
            myPrefab = Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            myPrefab.transform.parent = cc.tranformCursor;
            _ccursors.Add(uid, cc);
        }
        else
        {
            _ccursors[uid].move(x, y);
        }
        if (setup.myname == "ArClient")
        {
            //  Debug.Log("RpcMoveCCursorCreate");
        }
    }

    [ClientRpc]
    void RpcDeleteCCursor(int uid)
    {
        Debug.Log("RpcDeleteCCursor" + uid);
        _ccursors.Remove(uid);
    }

    [ClientRpc]
    void RpcChangeCCursorColor(int uid, Color c)
    {
        if (!_ccursors.ContainsKey(uid))
        {
            return;
        }
        else
        {
            _ccursors[uid].ChangeColor(c);
        }
        if (setup.myname == "ArClient")
        {
            // Debug.Log("RpcChangeCCursorColor");
        }
    }

    [ClientRpc]
    void RpcChangeCCursorTex(int uid, string t)
    {
        if (!_ccursors.ContainsKey(uid))
        {
            return;
        }
        else
        {
            if (t == "rotate")
            {
                _ccursors[uid].tex = RingsTex.RingWithArrow();
            }
            else if (t == "resize")
            {
                _ccursors[uid].tex = RingsTex.ResizeArrow();
            }
            else
            {
                _ccursors[uid].tex = CursorsTex.SimpleCursor(_ccursors[uid].color, Color.black, cursorHW, cursorT, cursorL);
            }
        }
        if (setup.myname == "ArClient")
        {
            Debug.Log("Rpc_ChangeCCursorTex" + t);
        }
    }

    // -------------------------------------------------------------
    // Server cursor
    public class SCursor
    {
        public CCursor ccursor;
        public int id;
        public int uid;
        public Color color;
        public float x, y;
        public int button;
        public bool hidden;
        // drag
        public bool drag;
        public bool doStartDrag;
        public bool doEndDrag;
        public Vector2 startDragPos;
        // stuff
        public bool clicked;
        public bool colorChanged;
        // last but not least
        public bool todelete;
        public bool overing = false;
        public bool changeShape = false;
        public int nbchangeShape = -1;
        public float timer;

        public float resize;
        public float rotate;

        public bool waitClunch;
        public bool clunch;
        public bool rotationToPerform = false;
        public bool resizedToPerform = false;
        public Vector2 topLeftConnerSelectionShape;
        public Vector2 bottomRightConnerSelectionShape;
        public Vector2 sizeSelectionShape;
        public bool toMiniWall;
        public bool toWall;

        public GameObject EmptyMoveShape;

        //state machine

        public enum stateCursor
        {
            nothing, pressed, waitDragShape, waitSelection, moveShape, selectionRect, nextSelection, previousSelection,
            waitResize, waitRotate, resize, rotate
        };
        public stateCursor currentState = stateCursor.nothing;

        public int shapeCliked = -1;
        public Vector2 preseedPos;
        public bool selectionOnRelease = false;


        public SCursor(int id_, float x_, float y_, Color c)
        {
            id = id_;
            x = x_; y = y_;
            ccursor = null; // new CCursor(x, y, c);
            color = c;
            todelete = false;
            drag = false;
            clicked = false;
        }

        public void ChangeColor(Color c)
        {
            colorChanged = true;
            color = c;
        }

    };

    // -------------------------------------------------------------
    // Server device
    class SDevice
    {
        public string name;
        Dictionary<int, SCursor> _scursors;

        public SDevice(string n)
        {
            name = n;
            _scursors = new Dictionary<int, SCursor>();
        }

        public string getName() { return name; }

        public Dictionary<int, SCursor> getSCursors() { return _scursors; }

        public SCursor getSCursor(int id)
        {
            if (_scursors.ContainsKey(id))
            {
                return _scursors[id];
            }
            return null;
        }

        public SCursor CreateCursor(int id, float x, float y, Color c)
        {
            SCursor sc = new SCursor(id, x, y, c);
            _scursors.Add(id, sc);
            return sc;
        }

        public void DeleteCursor(int id)
        {
            _scursors.Remove(id);
        }

    };

    private int _uidCreator = 0;
    private Dictionary<object, SDevice> _devices;

    // -----------------------------------------------------
    // start !!
    void Awake()
    {
        //mouse = new Vector3(0,0,0);
        Debug.Log("Awake InputManager ");
        if (isServer)
        {
            //_devices = new Dictionary<object, SDevice>();
        }
        else
        {
            //cursorHW = 16*4;
        }
        _devices = new Dictionary<object, SDevice>();
        _ccursors = new Dictionary<int, CCursor>();
    }

    void Start()
    {

        Debug.Log("Start InputManager ");

        setup = GameObject.Find("NetworkManager").GetComponent<Setup>();
        wall = setup.wallC;

        // smartiesMan = GameObject.Find("NetworkManager").GetComponent<SmartiesManager>();
        /* if (setup.myname != "ArClient")
         {
             rendering = GameObject.Find("Rendering").GetComponent<Rendering>();
         }
         */
        rendering = GameObject.Find("Rendering").GetComponent<Rendering>();

        Camera cameraLeft = null;
        Camera cameraRight = null;

        if (GameObject.Find("Main Camera/CameraLeft") != null && GameObject.Find("Main Camera/CameraRight") != null)
        {
            cameraLeft = GameObject.Find("Main Camera/CameraLeft").GetComponent<Camera>();
            cameraRight = GameObject.Find("Main Camera/CameraRight").GetComponent<Camera>();
        }
        if (setup.myname != "ArClient")
        {
            // setup the camera of a client
            if (!isServer)
            {
                float center_x, center_y;
                center_x = setup.x + (setup.width / 2) - (wall.getWallWidth() / 2) + (setup.width / 2);
                center_y = wall.getWallHeight() / 2 - setup.y + (setup.height / 2) - (setup.height / 2);
                Vector3 screenPos = Camera.main.WorldToScreenPoint(Camera.main.transform.position);
                Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(center_x, center_y, screenPos.z));
                Camera.main.transform.position = pos;
                Debug.Log("Central setup camera: " + pos.x + " " + pos.y + " " + pos.z);
                //Debug.Log("Central setup camera: "+ pos.x+" "+ pos.y+ " "+ pos.z);
                if (!setup.DoDrawUnderXineramaBezel())
                {
                    if (cameraLeft != null && cameraRight != null)
                    {
                        cameraLeft.enabled = false;
                        cameraRight.enabled = false;
                    }
                }
                else
                {
                    center_x = setup.width / 4;
                    center_y = 0;
                    screenPos = Camera.main.WorldToScreenPoint(cameraLeft.transform.position);
                    Debug.Log("cameraLeft: " + screenPos.x + " " + screenPos.y + " " + screenPos.z + " -- " + Screen.width);
                    pos = Camera.main.ScreenToWorldPoint(new Vector3(center_x, screenPos.y, screenPos.z));
                    cameraLeft.transform.position = pos;
                    //
                    center_x = wall.getBezelWidth() + 3 * setup.width / 4;
                    center_y = 0;
                    screenPos = Camera.main.WorldToScreenPoint(cameraRight.transform.position);
                    Debug.Log("cameraRight: " + screenPos.x + " " + screenPos.y + " " + screenPos.z);
                    pos = Camera.main.ScreenToWorldPoint(new Vector3(center_x, screenPos.y, screenPos.z));
                    //pos = new Vector3(-pos.x, pos.y, pos.z);
                    cameraRight.transform.position = pos;
                }
            }
            else
            {
                if (!setup.DoDrawUnderXineramaBezel())
                {
                    if (cameraLeft != null && cameraRight != null)
                    {
                        cameraLeft.enabled = false;
                        cameraRight.enabled = false;
                    }
                }
                else
                {
                    float center_x, center_y;
                    center_x = Screen.width / 4;
                    center_y = 0;
                    Vector3 screenPos = Camera.main.WorldToScreenPoint(cameraLeft.transform.position);
                    Debug.Log("cameraLeft: " + screenPos.x + " " + screenPos.y + " " + screenPos.z + " -- " + Screen.width);
                    Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(center_x, screenPos.y, screenPos.z));
                    cameraLeft.transform.position = pos;
                    //
                    center_x = 10 + 3 * Screen.width / 4;
                    center_y = 0;
                    screenPos = Camera.main.WorldToScreenPoint(cameraRight.transform.position);
                    Debug.Log("cameraRight: " + screenPos.x + " " + screenPos.y + " " + screenPos.z);
                    pos = Camera.main.ScreenToWorldPoint(new Vector3(center_x, screenPos.y, screenPos.z));
                    //pos = new Vector3(-pos.x, pos.y, pos.z);
                    cameraRight.transform.position = pos;
                }
            }

            // native cursor/mouse
            if (!isServer)
            {
                Cursor.visible = false;
                cursorHW = 32 * 4;
                cursorT = 5;
            }
            else
            {
                Cursor.visible = true;
            }
            if (isServer)
            {
                RegisterDevice("Mouse", this);
                CreateCursor(this, 0, 0.5f, 0.5f, Color.red);
            }

            if (setup.num_clients == 0)
            {
                // with clients we should do that when they are all connected
                // see MyNetworkManager
                //doChangeSceneAndTexture = true;
                AllClientsConnected();
            }
        }
        Debug.Log("InputManager Started");
    }

    // only the sever do this
    public void AllClientsConnected()
    {
        Debug.Log("InputManager All client CONNECTED called");
    }

    // -----------------------------------------------------
    // Device and Cursor stuff
    SDevice getDevice(object obj)
    {
        if (!_devices.ContainsKey(obj))
        {
            return null;
        }
        return _devices[obj];
    }

    SCursor getSCursor(object obj, int id)
    {
        SDevice sd = getDevice(obj);
        if (sd == null) return null;
        SCursor sc = sd.getSCursor(id);
        return sc;
    }

    CCursor getCCursor(object obj, int id)
    {
        SDevice sd = getDevice(obj);
        if (sd == null) return null;
        SCursor sc = sd.getSCursor(id);
        if (sc == null) return null;
        return sc.ccursor;
    }

    public void RegisterDevice(string name, object obj)
    {
        SDevice sd = getDevice(obj);
        if (sd != null) return;
        sd = new SDevice(name);
        Debug.Log("InputManager Register device: " + name + " " + obj);
        if (name == "Vicon")
        {
            // objHat = obj;
        }
        else if (name == "Smarties")
        {
            // objSmarties = obj;
        }
        else if (name == "Mouse")
        {
            // objMouse = obj;
        }
        _devices.Add(obj, sd);
    }

    public void CreateCursor(object obj, int id, float x, float y, Color c, bool hidden = false)
    {
        SDevice sd = getDevice(obj);
        if (sd == null) { return; }
        SCursor sc = sd.getSCursor(id);
        if (sc != null) return;
        Debug.Log("InputManager create cursor " + id + " for device: " + sd.name);
        sc = sd.CreateCursor(id, x, y, c);
        sc.hidden = hidden;
    }

    public void DeleteCursor(object obj, int id)
    {
        SDevice sd = getDevice(obj);
        if (sd == null) { return; }
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.todelete = true;
    }

    // -------------------------------------------

    public void StartMoveCursor(object obj, int id, float x, float y, bool drag, int button = 0)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        //Debug.Log("MoveCursor "+x+" "+y);
        sc.x = x; sc.y = y;
        sc.ChangeColor(Color.green);

        sc.clicked = false;
        sc.doStartDrag = true;
        sc.startDragPos = new Vector2(x, y);
        sc.drag = true;
        // Debug.Log(sc.startDragPos);
    }

    public void MoveCursor(object obj, int id, float x, float y)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        //Debug.Log("MoveCursor "+x+" "+y);
        sc.x = x; sc.y = y;
    }

    public void EndMoveCursor(object obj, int id, float x, float y)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        //Debug.Log("MoveCursor "+x+" "+y);
        //sc.x = x; sc.y = y;
        sc.ChangeColor(Color.cyan);

        if (sc.drag)
        {
            sc.doEndDrag = true;
        }
    }
    public void ClickCursor(object obj, int id, float x, float y)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.clicked = true;
    }
    public void Resize(object obj, int id, float resize)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.currentState = SCursor.stateCursor.resize;
        sc.resize = resize;
    }
    public void Rotate(object obj, int id, float rotate)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.currentState = SCursor.stateCursor.rotate;
        sc.rotate = rotate;
    }
    public void waitRotate(object obj, int id)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.currentState = SCursor.stateCursor.waitRotate;
    }
    public void waitResize(object obj, int id)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.currentState = SCursor.stateCursor.waitResize;
    }
    public void waitClunch(object obj, int id)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.waitClunch = true;
    }

    public void toMiniWall(object obj, int id)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.toMiniWall = true;
    }

    public void toWall(object obj, int id)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.toWall = true;
    }
    public void tranlateOnshape(object obj, int id, int shapeID, Vector3 value)
    {
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.getID() == shapeID)
            {

                float sw = Screen.width;
                float sh = Screen.height; // sw / sh == wall.getWallWidth() / wall.getWallHeight()
                float pixToUnit = Camera.main.orthographicSize / (sh / 2.0f);
                float x = (value.x * sw * pixToUnit);
                float y = (value.y * sh * pixToUnit);
                Vector3 pos = new Vector3(x, y, 0);
                ms.circ.transform.position += pos;
                Rpc_translateOneShape(shapeID, value);
            }
        }
    }
    public void rotateOnshape(object obj, int id, int shapeID, float value)
    {
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.getID() == shapeID)
            {
                ms.circ.transform.localEulerAngles = new Vector3(0, 0, value);
                Rpc_Rotate(ms.circ.getID(), value, false);
            }
        }
    }

    public void Clunch(object obj, int id)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.clunch = true;
    }



    public void PressedCursor(object obj, int id, float x, float y)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        if (sc.currentState != SCursor.stateCursor.waitRotate || sc.currentState != SCursor.stateCursor.waitResize)
        {
            sc.currentState = SCursor.stateCursor.pressed;
            sc.timer = Time.time;
            sc.preseedPos = new Vector2(x, y);
        }
        //  Debug.Log(sc.preseedPos);
    }

    public void nextSelectionCursor(object obj, int id)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.currentState = SCursor.stateCursor.nextSelection;
    }

    public void previousSelectionCursor(object obj, int id)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.currentState = SCursor.stateCursor.previousSelection;
    }

    // -------------------------------------------

    public void CursorClick(object obj, int id, float x, float y, int button = 0)
    {
        SCursor sc = getSCursor(obj, id);
        if (sc == null) return;
        sc.clicked = true;
        sc.button = button;
    }


    // -----------------------------------------------------
    // UPDATE

    void Update()
    {
        if (!isServer)
        {
            return;
        }
        // server cursor
        // Fixme: buton 1,2,3 ...
        float x = Input.mousePosition.x / Screen.width;
        float y = (Screen.height - Input.mousePosition.y) / Screen.height;

        GameObject lineSelection = GameObject.Find("LineSelection");

        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("left button down");
            StartMoveCursor(this, 0, x, y, false);
            PressedCursor(this, 0, x, y);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            EndMoveCursor(this, 0, x, y);
            ClickCursor(this, 0, x, y);
        }
        else
        {
            MoveCursor(this, 0, x, y);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            //   nextSelectionCursor(this, 0);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            //  previousSelectionCursor(this, 0);
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            // Wheel(this, 0, 1);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            // Wheel(this, 0, -1);
        }

        // all the cursors
        List<int> todeletesids = null;
        foreach (SDevice dev in _devices.Values)
        {
            foreach (SCursor sc in dev.getSCursors().Values)
            {
                if (sc.ccursor == null)
                {
                    sc.ccursor = new CCursor(sc.x, sc.y, sc.color);
                    _ccursors.Add(_uidCreator, sc.ccursor);
                    if (!sc.hidden)
                    {
                        RpcCreateCCursor(_uidCreator, sc.x, sc.y, sc.color);
                        myPrefab = Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                        myPrefab.transform.parent = sc.ccursor.tranformCursor;
                    }
                    sc.uid = _uidCreator;
                    _uidCreator++;
                }
                if (sc.todelete)
                {
                    if (todeletesids == null)
                    {
                        todeletesids = new List<int>();
                    }
                    todeletesids.Add(sc.id);
                    sc.ccursor = null;
                    _ccursors.Remove(sc.uid);
                    if (!sc.hidden)
                    {
                        RpcDeleteCCursor(sc.uid);
                    }
                }
                else if (sc.x != sc.ccursor.x || sc.y != sc.ccursor.y)
                {
                    if (!sc.hidden)
                    {
                        RpcMoveCCursorCreate(sc.uid, sc.x, sc.y, sc.color);
                    }
                    sc.ccursor.x = sc.x;
                    sc.ccursor.y = sc.y;
                }
                if (sc.colorChanged)
                {
                    sc.ccursor.ChangeColor(sc.color);
                    RpcChangeCCursorColor(sc.uid, sc.color);
                    sc.colorChanged = false;
                }
                if (sc.doStartDrag)
                {

                    sc.doStartDrag = false;
                }

                if (sc.doEndDrag)
                {

                    sc.color = Color.cyan;
                    sc.ccursor.ChangeColor(sc.color);
                    sc.doEndDrag = false;
                    sc.drag = false;
                }
                Vector2 scConvert = relatifToscreen(sc.x, sc.y);

                //STATE MACHINE

                if (Input.GetKeyDown("v"))
                {
                    Debug.Log(sc.x + " " + sc.y);
                    Vector2 pos = relatifToscreen(sc.x, sc.y);
                    Debug.Log(pos);
                    Vector2 pos2 = screenTorelatif(pos.x, pos.y);
                    Debug.Log(pos2);
                }
                if (sc.waitClunch)
                {
                    sc.waitClunch = false;
                    Rpc_StartTimer(sc.uid, sc.x, sc.y, sc.color);
                    sc.ccursor.tranformCursor.GetChild(0).GetComponent<CursorTimer>().StartTimer(0.4f);
                }

                if (wall != null)
                {
                    sc.ccursor.tranformCursor.position = relatifToscreen(sc.x, sc.y);
                    Rpc_posCursorTimer(sc.uid, sc.x, sc.y, sc.color);
                }

                /*
                if (sc.ccursor.tranformCursor.GetChild(0).GetComponent<CursorTimer>().timerf == 0)
                {
                    //sc.ccursor.Changetex("normal");
                    //RpcChangeCCursorTex(sc.uid, "normal");
                }
                */
                if (sc.clunch)
                {
                    sc.clunch = false;
                    sc.ccursor.tranformCursor.GetChild(0).GetComponent<CursorTimer>().stopTimer();
                    Rpc_stopTimer(sc.uid, sc.x, sc.y, sc.color);
                }

                if (sc.currentState == SCursor.stateCursor.pressed)  // cusor presed -> check if is it on a shape or not 
                {
                    // resetSelectionShape();
                    // Rpc_resetSelectionShape();
                    bool onShape = false;
                    sc.EmptyMoveShape = new GameObject(); Vector3 pos = new Vector3(scConvert.x, scConvert.y, 0);
                    sc.EmptyMoveShape.transform.position = pos;
                    foreach (Rendering.mShape ms in rendering.ListShapes)
                    {
                        if (ms.circ.GetComponent<Collider>().bounds.Contains(new Vector3(scConvert.x, scConvert.y, 0))) // on a shape
                        {
                            onShape = true;
                            sc.currentState = SCursor.stateCursor.waitDragShape;
                            sc.shapeCliked = ms.circ.getID();

                            ms.circ.transform.parent = sc.EmptyMoveShape.transform;

                            Debug.Log(sc.EmptyMoveShape.transform.position + " " + screenTorelatif(ms.circ.transform.position.x, ms.circ.transform.position.y));
                        }
                    }

                    if (!onShape) // not on shape unselect all + wait for selection rect
                    {
                        sc.currentState = SCursor.stateCursor.waitSelection;
                        // if previous selection  miniwall to wall  + unselect shape
                        //rendering.toWall = true;
                        //Rpc_toWall(screenTorelatif(sc.topLeftConnerSelectionShape.x, sc.topLeftConnerSelectionShape.y));
                        foreach (Rendering.mShape ms in rendering.ListShapes)
                        {
                            ms.circ.unSelectShape();
                            if (!ms.circ.onMiniWall)
                            {
                                Rpc_ChangeColorBorder(Color.white, ms.circ.getID());
                            }
                            else
                            {
                                Rpc_ChangeColorBorderPreview(Color.white, ms.circ.getID(), true);//preview only
                            }
                        }
                    }
                }

                if (sc.clicked)
                {
                    sc.clicked = false;
                    if (sc.rotationToPerform) { applideRotation(sc); }
                    if (sc.resizedToPerform) { applideResize(sc); }
                    if (!rendering.toMiniWall) { applideMovement(sc); }
                    sc.rotationToPerform = sc.resizedToPerform = false;
                    sc.rotate = 0;
                    sc.resize = 0;
                    sc.ccursor.Changetex("normal");
                    RpcChangeCCursorTex(sc.uid, "normal");
                    if (sc.currentState == SCursor.stateCursor.rotate || sc.currentState == SCursor.stateCursor.resize) { sc.currentState = SCursor.stateCursor.nothing; }
                    foreach (Rendering.mShape ms in rendering.ListShapes)
                    {
                        ms.circ.setInitialScale(ms.circ.transform.localScale.x);
                    }
                    Rpc_setScale();
                    if (sc.EmptyMoveShape.transform != null)
                    {
                        foreach (Transform child in sc.EmptyMoveShape.transform)
                        {
                            //child.transform.parent = rendering.canvas.transform;
                            child.transform.GetComponent<Circle>().changeParent = true;

                        }
                        foreach (Rendering.mShape ms in rendering.ListShapes)
                        {
                            if (ms.circ.changeParent)
                            {
                                ms.circ.changeParent = false;
                                ms.circ.transform.parent = rendering.canvas.transform;
                            }
                        }
                        Destroy(sc.EmptyMoveShape);
                    }

                    if (sc.selectionOnRelease)
                    {
                        Vector2 initialPos = relatifToscreen(sc.startDragPos.x, sc.startDragPos.y);

                        foreach (Rendering.mShape ms in rendering.ListShapes)
                        {
                            if (ShapeInSelection(ms.circ.transform.position.x, ms.circ.transform.position.y, initialPos.x, initialPos.y, scConvert.x, scConvert.y))
                            {
                                // ms.circ.selectShape();
                                // ms.circ.onMiniWall = true;
                                float xsize = Mathf.Abs(scConvert.x - initialPos.x);
                                float ysize = Mathf.Abs(scConvert.y - initialPos.y);
                                sc.sizeSelectionShape.x = xsize;
                                sc.sizeSelectionShape.y = ysize;

                                Debug.Log("Selection shape " + xsize + " " + ysize);
                                float xshape = Mathf.Abs(ms.circ.transform.position.x - initialPos.x) / xsize;
                                float yshape;
                                sc.topLeftConnerSelectionShape = initialPos;

                                sc.bottomRightConnerSelectionShape = scConvert;
                                if (initialPos.x > scConvert.x)
                                {
                                    sc.topLeftConnerSelectionShape.x = scConvert.x;
                                    sc.bottomRightConnerSelectionShape.x = initialPos.x;
                                }
                                if (initialPos.y < scConvert.y)
                                {
                                    sc.topLeftConnerSelectionShape.y = scConvert.y;
                                    sc.bottomRightConnerSelectionShape.y = initialPos.y;
                                    yshape = Mathf.Abs(ms.circ.transform.position.y - initialPos.y) / ysize;
                                }
                                else
                                {
                                    yshape = Mathf.Abs(ms.circ.transform.position.y - scConvert.y) / ysize;
                                }
                                //Debug.Log(sc.topLeftConnerSelectionShape + " " +initialPos + " " + scConvert);

                                Debug.Log("shape position x" + xshape);
                                Debug.Log("shape position y" + yshape);
                                // Debug.Log(" Y p1 " + p1y + " p2 " + p2y + " shape " + ms.circ.transform.position.y);
                                //Debug.Log(" X p1 " + p1x + " p2 " + scConvert.x + " shape " + ms.circ.transform.position.x);
                                Rpc_InSelectionShape(Color.green, ms.circ.getID(), xshape, yshape);
                                //Rpc_SelectionShapeToMiniWall(ms.circ.getID());
                                // Rpc_selectShape(Color.green, ms.circ.getID());
                                ms.circ.xshape = xshape;
                                ms.circ.yshape = yshape;
                            }
                        }
                        //rendering.toWall = true;
                        //Rpc_toMiniWall(screenTorelatif(sc.topLeftConnerSelectionShape.x, sc.topLeftConnerSelectionShape.y));
                    }
                    if (sc.currentState == SCursor.stateCursor.waitDragShape) // click release and wait to drag shape -> juste a click on a shape
                    {
                        Rendering.mShape ms = rendering.ListShapes[sc.shapeCliked];

                        if (ms.circ.getIsSelected() == false)
                        {
                            ms.circ.selectShape();
                            if (!ms.circ.onMiniWall)
                            {
                                Rpc_ChangeColorBorder(Color.green, ms.circ.getID());
                            }
                            else
                            {
                                Rpc_ChangeColorBorderPreview(Color.green, ms.circ.getID(), true);//preview only
                            }

                        }
                        else
                        {
                            ms.circ.unSelectShape();
                            if (!ms.circ.onMiniWall)
                            {
                                Rpc_ChangeColorBorder(Color.white, ms.circ.getID());
                            }
                            else
                            {
                                Rpc_ChangeColorBorderPreview(Color.white, ms.circ.getID(), true);//preview only
                            }

                        }
                    }

                    if (sc.currentState == SCursor.stateCursor.waitSelection) // click release and waitselection -> juste a click on a wall -> unselect every shape
                    {
                        /*
                        foreach (Rendering.mShape ms in rendering.ListShapes)
                        {
                            ms.circ.unSelectShape();
                            //Rpc_ChangeColorBorder(Color.white, ms.circ.getID() );
                            if (!ms.circ.onMiniWall)
                            {
                                Rpc_ChangeColorBorder(Color.white, ms.circ.getID());
                            }
                            else
                            {
                                Rpc_ChangeColorBorderPreview(Color.white, ms.circ.getID(), true);//preview only
                            }
                        }
                        */
                    }

                    sc.currentState = SCursor.stateCursor.nothing; // reset
                    sc.selectionOnRelease = false;
                }
                if (sc.toWall)
                {
                    sc.toWall = false;
                    rendering.toMiniWall = false;
                    foreach (Rendering.mShape ms in rendering.ListShapes)
                    {
                        if (ms.circ.onMiniWall)
                        {
                            ms.circ.onMiniWall = false;
                            float xshape = Mathf.Abs(ms.circ.transform.position.x - sc.topLeftConnerSelectionShape.x) / sc.sizeSelectionShape.x;
                            float yshape = 1 - (Mathf.Abs(ms.circ.transform.position.y - sc.topLeftConnerSelectionShape.y) / sc.sizeSelectionShape.y);

                            // ms.circ.selectShape();
                            Rpc_InSelectionShape(Color.green, ms.circ.getID(), xshape, yshape);
                            // Rpc_selectShape(Color.green, ms.circ.getID());
                            ms.circ.xshape = xshape;
                            ms.circ.yshape = yshape;

                            if (ms.circ.getIsSelected())
                            {
                                ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(Color.green);
                                Rpc_ChangeColorBorder(Color.green, ms.circ.getID());
                            }
                            else
                            {
                                ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(Color.white);
                                Rpc_ChangeColorBorder(Color.white, ms.circ.getID());
                            }
                        }
                    }
                    // resetSelectionShape();
                    // Rpc_resetSelectionShape();
                    //Rpc_toWall();
                    applideMovement(sc);
                    Rpc_msg("to wall imput Manager");
                }


                if (sc.toMiniWall)
                {
                    sc.toMiniWall = false;
                    // Rpc_msg("to mini wall imput Manager");
                    rendering.toMiniWall = true;
                    //ici selection sha
                    foreach (Rendering.mShape ms in rendering.ListShapes)
                    {
                        if (ShapeInSelection(ms.circ.transform.position.x, ms.circ.transform.position.y, sc.topLeftConnerSelectionShape.x, sc.topLeftConnerSelectionShape.y, sc.bottomRightConnerSelectionShape.x, sc.bottomRightConnerSelectionShape.y))
                        {
                            ms.circ.onMiniWall = true;
                            Rpc_SelectionShapeToMiniWall(ms.circ.getID());
                            ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(Color.blue);
                            Rpc_ChangeColorBorder(Color.blue, ms.circ.getID());
                            float xshape = Mathf.Abs(ms.circ.transform.position.x - sc.topLeftConnerSelectionShape.x) / sc.sizeSelectionShape.x;
                            float yshape = 1 - (Mathf.Abs(ms.circ.transform.position.y - sc.topLeftConnerSelectionShape.y) / sc.sizeSelectionShape.y);

                            // ms.circ.selectShape();
                            Rpc_InSelectionShape(Color.green, ms.circ.getID(), xshape, yshape);
                            // Rpc_selectShape(Color.green, ms.circ.getID());
                            ms.circ.xshape = xshape;
                            ms.circ.yshape = yshape;
                        }
                    }
                    Rpc_toMiniWall(screenTorelatif(sc.topLeftConnerSelectionShape.x, sc.topLeftConnerSelectionShape.y));
                }

                if ((sc.currentState == SCursor.stateCursor.waitDragShape && Vector2.Angle(sc.preseedPos, new Vector2(sc.x, sc.y)) > 1)
                    || sc.currentState == SCursor.stateCursor.moveShape) // move the shape 
                {

                    if (sc.currentState == SCursor.stateCursor.waitDragShape)
                    {
                        foreach (Rendering.mShape ms2 in rendering.ListShapes)
                        {
                            if (ms2.circ.getIsSelected())
                            {
                                ms2.circ.transform.parent = sc.EmptyMoveShape.transform;
                            }
                        }
                    }
                    Rendering.mShape ms = rendering.ListShapes[sc.shapeCliked];
                    if (!rendering.ListShapes[sc.shapeCliked].circ.getIsSelected())//!ms.circ.onMiniWall)
                    {
                        Rpc_msg("Move none selectied shape");
                        float newXshape = Mathf.Abs(rendering.ListShapes[sc.shapeCliked].circ.transform.position.x - sc.topLeftConnerSelectionShape.x) / sc.sizeSelectionShape.x;
                        float newYshape = 1 - (Mathf.Abs(rendering.ListShapes[sc.shapeCliked].circ.transform.position.y - sc.topLeftConnerSelectionShape.y) / sc.sizeSelectionShape.y);
                        Vector2 posShape = screenTorelatif(rendering.ListShapes[sc.shapeCliked].circ.transform.position.x, rendering.ListShapes[sc.shapeCliked].circ.transform.position.y);
                        
                        Vector3 pos = new Vector3(scConvert.x, scConvert.y, 0);
                        ms.circ.transform.position = pos;
                        if (!ms.circ.onMiniWall)
                        {
                            Rpc_Moveshape(rendering.ListShapes[sc.shapeCliked].circ.getID(), posShape.x, posShape.y, newXshape, newYshape);
                        }
                        else
                        {
                            Rpc_MoveshapePreview(rendering.ListShapes[sc.shapeCliked].circ.getID(), posShape.x, posShape.y, newXshape, newYshape, true);//preview only
                        }
                    }
                    else
                    {
                        Vector3 pos = new Vector3(scConvert.x, scConvert.y, 0);
                        //ms.circ.transform.position = pos;
                        sc.EmptyMoveShape.transform.position = pos;

                        // Rpc_msg(newXshape + " " + newYshape);
                        foreach (Rendering.mShape ms2 in rendering.ListShapes)
                        {

                            float newXshape = Mathf.Abs(ms2.circ.transform.position.x - sc.topLeftConnerSelectionShape.x) / sc.sizeSelectionShape.x;
                            float newYshape = 1 - (Mathf.Abs(ms2.circ.transform.position.y - sc.topLeftConnerSelectionShape.y) / sc.sizeSelectionShape.y);

                            if (ms2.circ.transform.parent == sc.EmptyMoveShape.transform)
                            {
                                Vector2 posShape = screenTorelatif(ms2.circ.transform.position.x, ms2.circ.transform.position.y);

                                if (!ms.circ.onMiniWall)
                                {
                                    Rpc_Moveshape(ms2.circ.getID(), posShape.x, posShape.y, newXshape, newYshape);
                                }
                                else
                                {
                                    Rpc_MoveshapePreview(ms2.circ.getID(), posShape.x, posShape.y, newXshape, newYshape, true);//preview only
                                }
                            }
                        }
                        //Rpc_Moveshape(sc.shapeCliked, sc.x, sc.y, newXshape, newYshape);

                    }
                    sc.currentState = SCursor.stateCursor.moveShape;

                }

                if ((sc.currentState == SCursor.stateCursor.waitSelection && Vector2.Angle(sc.preseedPos, new Vector2(sc.x, sc.y)) > 1) && !rendering.toMiniWall)
                {
                    Rpc_ARSelectionShape();
                }

                if (((sc.currentState == SCursor.stateCursor.waitSelection && Vector2.Angle(sc.preseedPos, new Vector2(sc.x, sc.y)) > 1)
                    || sc.currentState == SCursor.stateCursor.selectionRect) && !rendering.toMiniWall) // move the shape 
                {

                    Vector2 initialPos = relatifToscreen(sc.startDragPos.x, sc.startDragPos.y);
                    lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(0, new Vector3(initialPos.x, initialPos.y, 0));
                    lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(1, new Vector3(scConvert.x, initialPos.y, 0));

                    lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(0, new Vector3(scConvert.x, initialPos.y, 0));
                    lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(1, new Vector3(scConvert.x, scConvert.y, 0));

                    lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(0, new Vector3(scConvert.x, scConvert.y, 0));
                    lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(1, new Vector3(initialPos.x, scConvert.y, 0));

                    lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(0, new Vector3(initialPos.x, scConvert.y, 0));
                    lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(1, new Vector3(initialPos.x, initialPos.y, 0));

                    Rpc_Selectionshape(sc.startDragPos.x, sc.startDragPos.y, sc.x, sc.y);
                    sc.currentState = SCursor.stateCursor.selectionRect;
                    sc.selectionOnRelease = true;
                }


                // First test for multiple selection
                if (sc.currentState == SCursor.stateCursor.waitSelection && Time.time - sc.timer >= 1)
                {
                    Debug.Log("Save Selection");
                    List<int> selection = new List<int>();
                    foreach (Rendering.mShape ms in rendering.ListShapes)
                    {
                        if (ms.circ.getIsSelected())
                        {
                            selection.Add(ms.circ.getID());
                        }
                    }
                    if (selection.Count != 0)
                    {
                        ListSelectionsShapes.Add(selection);
                        // Debug.Log(ListSelectionsShapes[0][0]);
                    }
                    else
                    {
                        Debug.Log("not save !");
                    }
                    sc.currentState = SCursor.stateCursor.nothing;
                }


                if (sc.currentState == SCursor.stateCursor.nextSelection)
                {
                    currentSlectionInt = currentSlectionInt + 1;// fix this
                    if (currentSlectionInt > ListSelectionsShapes.Count - 1)
                    {
                        currentSlectionInt = currentSlectionInt - ListSelectionsShapes.Count;
                    }
                    currentselection = ListSelectionsShapes[currentSlectionInt];

                    foreach (Rendering.mShape ms in rendering.ListShapes)    // unselect all
                    {
                        ms.circ.unSelectShape();
                        Rpc_ChangeColorBorder(Color.white, ms.circ.getID());
                    }

                    foreach (Rendering.mShape ms in rendering.ListShapes)    // select the selection 
                    {

                        foreach (int i in currentselection)    // select the selection 
                        {
                            if (i == ms.circ.getID())
                            {
                                ms.circ.selectShape();
                                Rpc_ChangeColorBorder(Color.green, ms.circ.getID());
                            }
                        }
                    }
                    sc.currentState = SCursor.stateCursor.nothing;
                }

                if (sc.currentState == SCursor.stateCursor.previousSelection)
                {
                    currentSlectionInt = currentSlectionInt - 1;// fix this
                    if (currentSlectionInt < 0)
                    {
                        currentSlectionInt = currentSlectionInt + ListSelectionsShapes.Count;
                    }
                    currentselection = ListSelectionsShapes[currentSlectionInt];

                    foreach (Rendering.mShape ms in rendering.ListShapes)    // unselect all
                    {
                        ms.circ.unSelectShape();
                        Rpc_ChangeColorBorder(Color.white, ms.circ.getID());
                    }

                    foreach (Rendering.mShape ms in rendering.ListShapes)    // select the selection 
                    {

                        foreach (int i in currentselection)    // select the selection 
                        {
                            if (i == ms.circ.getID())
                            {
                                ms.circ.selectShape();
                                Rpc_ChangeColorBorder(Color.green, ms.circ.getID());
                            }
                        }
                    }
                    sc.currentState = SCursor.stateCursor.nothing;
                }


                if (sc.currentState == SCursor.stateCursor.waitResize)
                {
                    sc.ccursor.Changetex("resize");
                    RpcChangeCCursorTex(sc.uid, "resize");
                }
                if (sc.currentState == SCursor.stateCursor.resize)
                {
                    sc.resizedToPerform = true;
                    foreach (Rendering.mShape ms in rendering.ListShapes)    // select the selection 
                    {
                        if (ms.circ.getIsSelected())
                        {
                            if (ms.circ.getinitialScale() * (100 + sc.resize) / 100 < 100 && ms.circ.getinitialScale() * (100 + sc.resize) / 100 > 0.1) // DEF SIZE MIN & MAX
                            {
                                // ms.circ.transform.localScale = new Vector3(ms.circ.getinitialScale() * (100 + sc.resize) / 100,
                                //                                            ms.circ.getinitialScale() * (100 + sc.resize) / 100, ms.circ.transform.localScale.z);
                                if (rendering.toMiniWall)
                                {
                                    Rpc_Resize(ms.circ.getID(), sc.resize, true);
                                }
                                else
                                {
                                    Rpc_Resize(ms.circ.getID(), sc.resize, false);
                                }

                            }
                        }
                    }
                }

                if (sc.currentState == SCursor.stateCursor.waitRotate)
                {
                    sc.ccursor.Changetex("rotate");
                    RpcChangeCCursorTex(sc.uid, "rotate");
                }
                if (sc.currentState == SCursor.stateCursor.rotate)
                {
                    Rpc_msg(" currentState rotate");
                    sc.rotationToPerform = true;
                    foreach (Rendering.mShape ms in rendering.ListShapes)    // select the selection 
                    {
                        if (ms.circ.getIsSelected())
                        {
                            ms.circ.transform.localEulerAngles = new Vector3(0, 0, sc.rotate);
                            if (rendering.toMiniWall)
                            {
                                Rpc_Rotate(ms.circ.getID(), sc.rotate, true);
                            }
                            else
                            {
                                Rpc_Rotate(ms.circ.getID(), sc.rotate, false);
                            }

                        }
                    }
                }



                {
                    /*
                    if (sc.clicked)
                    {
                        //Debug.Log("click");

                        /*
                        if (rendering.ListShapes[rendering.nbshape - 1].circ.GetComponent<Collider>().bounds.Contains(new Vector3(scConvert.x, scConvert.y, 0)))
                        {
                           // rendering.ListShapes[rendering.nbshape - 1].circ.ChangeColor(Color.green);
                           // Rpc_ChangeColor(Color.green, sc.nbchangeShape);
                            float randomX = UnityEngine.Random.Range(0f, 1f);
                            float randomY = UnityEngine.Random.Range(0f, 1f);

                            Vector2 newPos = relatifToscreen(randomX, randomY);
                            rendering.ListShapes[rendering.nbshape - 1].circ.transform.position = new Vector3(newPos.x, newPos.y , 0);
                            Rpc_Moveshape(rendering.nbshape - 1, randomX, randomY);
                            rendering.moveTarget(new Vector3(randomX, randomY, 0));
                        }
                        */
                    /*
                        bool UnselectShapes = true;
                        foreach (Rendering.mShape ms in rendering.ListShapes)
                        {
                            if (ms.circ.GetComponent<Collider>().bounds.Contains(new Vector3(scConvert.x, scConvert.y, 0)))
                            {
                                UnselectShapes = false;
                                //Debug.Log(ms.circ.getID());
                                if (ms.circ.getIsSelected() == false)
                                {
                                    ms.circ.selectShape();
                                    Rpc_ChangeColorBorder(Color.green, ms.circ.getID());
                                }
                                else
                                {
                                    ms.circ.unSelectShape();
                                    Rpc_ChangeColorBorder(Color.white, ms.circ.getID());
                                }

                            }
                        }

                        if (UnselectShapes)
                        {
                            foreach (Rendering.mShape ms in rendering.ListShapes)
                            {
                                ms.circ.unSelectShape();
                                Rpc_ChangeColorBorder(Color.white, ms.circ.getID());
                            }

                        }

                        sc.clicked = false;
                    }

                    rendering.canvas.transform.Find("debugText").GetComponent<TextMesh>().text = "Pointer x : " + sc.x + " / y :" + sc.y + "\n"
                                                                                                + " screen x : " + scConvert.x + " / y :" + scConvert.y;
                    if (rendering != null)
                    {
                        sc.changeShape = false;
                        foreach (Rendering.mShape ms in rendering.ListShapes)
                        {
                            if (sc.drag && sc.nbchangeShape == ms.circ.getID() && ms.circ.getID() != rendering.nbshape - 1)
                            {
                                Vector3 pos = new Vector3(scConvert.x, scConvert.y, 0);
                                ms.circ.transform.position = pos;
                                Rpc_Moveshape(sc.nbchangeShape, sc.x, sc.y);

                                if (ms.circ.getID() == rendering.nbshape - 1)
                                {

                                    //rendering.moveTarget(new Vector3(sc.x, sc.y ,0));
                                    // Debug.Log(new Vector3(sc.x, sc.y, 0));
                                }
                            }
                            if (ms.circ.GetComponent<Collider>().bounds.Contains(new Vector3(scConvert.x, scConvert.y, 0)))
                            {
                                if (!sc.overing)
                                {
                                    ms.circ.ChangeColor(Color.green);
                                    sc.overing = true;
                                    sc.nbchangeShape = ms.circ.getID();
                                    Rpc_ChangeColor(Color.green, sc.nbchangeShape);
                                }
                                sc.changeShape = true;
                            }
                        }
                        if (sc.overing && !sc.changeShape)
                        {
                            foreach (Rendering.mShape ms in rendering.ListShapes)
                            {
                                if (ms.circ.getID() == sc.nbchangeShape)
                                {
                                    ms.circ.ChangeColor(ms.circ.GetInitialColorColor());
                                    Rpc_ChangeColor(ms.circ.GetInitialColorColor(), sc.nbchangeShape);
                                    sc.overing = false;
                                    if (!sc.drag)
                                    {
                                        sc.nbchangeShape = -1;
                                    }
                                }

                            }
                        }

                        if (sc.drag)
                        {
                            Vector2 initialPos = relatifToscreen(sc.startDragPos.x, sc.startDragPos.y);
                            lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(0, new Vector3(initialPos.x, initialPos.y, 0));
                            lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(1, new Vector3(scConvert.x, initialPos.y, 0));

                            lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(0, new Vector3(scConvert.x, initialPos.y, 0));
                            lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(1, new Vector3(scConvert.x, scConvert.y, 0));

                            lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(0, new Vector3(scConvert.x, scConvert.y, 0));
                            lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(1, new Vector3(initialPos.x, scConvert.y, 0));

                            lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(0, new Vector3(initialPos.x, scConvert.y, 0));
                            lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(1, new Vector3(initialPos.x, initialPos.y, 0));

                            Rpc_Selectionshape(sc.startDragPos.x, sc.startDragPos.y, sc.x, sc.y);

                            foreach (Rendering.mShape ms in rendering.ListShapes)
                            {
                                if (ShapeInSelection(ms.circ.transform.position.x, ms.circ.transform.position.y, initialPos.x, initialPos.y, scConvert.x, scConvert.y))
                                {
                                    ms.circ.selectShape();
                                    Rpc_ChangeColorBorder(Color.green, ms.circ.getID());
                                }
                            }


                        }
                    }
                    */
                }
                if (todeletesids != null)
                {
                    foreach (int id in todeletesids)
                    {
                        dev.DeleteCursor(id);
                    }
                    todeletesids = null;
                }
            }

        }
    }

    public bool ShapeInSelection(float xshape, float yshape, float xmin, float ymin, float xmax, float ymax)
    {

        if (xmin > xmax)
        {
            float temp = xmax;
            xmax = xmin;
            xmin = temp;
        }
        if (ymin > ymax)
        {
            float temp = ymax;
            ymax = ymin;
            ymin = temp;
        }
        if (xshape >= xmin && xshape <= xmax && yshape >= ymin && yshape <= ymax)
        {
            return true;
        }
        return false;
    }

    public void resetSelectionShape()
    {
        GameObject lineSelection = GameObject.Find("LineSelection");
        lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
        lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));

        lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
        lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));

        lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
        lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));

        lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
        lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));
    }

    public void applideRotation(SCursor sc)
    {
        foreach (Rendering.mShape ms in rendering.ListShapes)    // select the selection 
        {
            if (ms.circ.getIsSelected())
            {
                Rpc_Rotate(ms.circ.getID(), sc.rotate, false);
                ms.circ.transform.localEulerAngles = new Vector3(0, 0, sc.rotate);
            }
        }
    }

    public void applideResize(SCursor sc)
    {
        foreach (Rendering.mShape ms in rendering.ListShapes)    // select the selection 
        {
            if (ms.circ.getIsSelected())
            {
                ms.circ.transform.localScale = new Vector3(ms.circ.getinitialScale() * (100 + sc.resize) / 100,
                                                                           ms.circ.getinitialScale() * (100 + sc.resize) / 100, ms.circ.transform.localScale.z);
                Rpc_Resize(ms.circ.getID(), sc.resize, false);
            }
        }
    }

    public void applideMovement(SCursor sc)
    {
        foreach (Rendering.mShape ms in rendering.ListShapes)    // select the selection 
        {
            //ms.circ.transform.position = pos;
            Vector2 pos = screenTorelatif(ms.circ.transform.position.x, ms.circ.transform.position.y);
            Rpc_Moveshape(ms.circ.getID(), pos.x, pos.y, 0, 0);
        }
    }

    [ClientRpc]
    void Rpc_translateOneShape(int shapeID, Vector3 value)
    {
        if (setup.myname != "ArClient")
        {
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == shapeID)
                {
                    float sw = wall.getWallWidth();
                    float sh = wall.getWallHeight();
                    float shnobezel = 8640;
                    float pixToUnit = (float)wall.getGridRow() * (float)Camera.main.orthographicSize / (shnobezel / 2.0f);

                    float x = value.x * sw * pixToUnit;
                    float y = value.y * sh * pixToUnit;
                    Vector3 pos = new Vector3(x, y, 0);
                    ms.circ.transform.position += pos;
                }
            }
        }
        else
        {
            Debug.Log("move one shape" + value);
        }
    }

    [ClientRpc]
    void Rpc_toMiniWall(Vector2 startSelection)
    {
        if (setup.myname == "ArClient")
        {
            rendering.toMiniWall = true;
            rendering.refStratselection = startSelection;
            Debug.Log(startSelection);
        }
    }

    [ClientRpc]
    void Rpc_toWall()
    {
        if (setup.myname == "ArClient")
        {
            //rendering.toWall = true;
            Debug.Log("To wall input manager");
        }
    }

    [ClientRpc]
    void Rpc_StartTimer(int uid, float x, float y, Color c)
    {
        if (setup.myname != "ArClient")
        {
            mylocalCreateCCurqor(uid, x, y, c);
            _ccursors[uid].tranformCursor.GetChild(0).GetComponent<CursorTimer>().StartTimer(0.5f);

            // Debug.Log("Rpc_StartTimer");
        }
    }
    [ClientRpc]
    void Rpc_stopTimer(int uid, float x, float y, Color c)
    {
        if (setup.myname != "ArClient")
        {
            mylocalCreateCCurqor(uid, x, y, c);
            _ccursors[uid].tranformCursor.GetChild(0).GetComponent<CursorTimer>().stopTimer();

            // Debug.Log("Rpc_stopTimer");
        }
    }

    [ClientRpc]
    void Rpc_posCursorTimer(int uid, float x, float y, Color c)
    {
        if (setup.myname != "ArClient")
        {
            mylocalCreateCCurqor(uid, x, y, c);
            _ccursors[uid].tranformCursor.position = moveTimerCursor(x, y);

            Debug.Log("Rpc_posCursorTimer");
        }
    }
    [ClientRpc]
    void Rpc_Resize(int i, float resize, bool preview)
    {
        if (preview)
        {
            if (setup.myname == "ArClient")
            {
                //Debug.Log("Rpc_Resize");
                foreach (Rendering.mShape ms in rendering.ListShapes)
                {
                    if (ms.circ.getID() == i)
                    {
                        ms.circ.transform.localScale = new Vector3(ms.circ.getinitialScale() * (100 + resize) / 100,
                                                                   ms.circ.getinitialScale() * (100 + resize) / 100, ms.circ.transform.localScale.z);
                    }
                }
            }
        }
        else
        {
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    ms.circ.transform.localScale = new Vector3(ms.circ.getinitialScale() * (100 + resize) / 100,
                                                               ms.circ.getinitialScale() * (100 + resize) / 100, ms.circ.transform.localScale.z);
                }
            }
        }
    }
    [ClientRpc]
    void Rpc_Rotate(int i, float rotate, bool preview)
    {
        if (preview)
        {
            if (setup.myname == "ArClient")
            {
                //Debug.Log("Rotate");
                foreach (Rendering.mShape ms in rendering.ListShapes)
                {
                    if (ms.circ.getID() == i)
                    {
                        ms.circ.transform.localEulerAngles = new Vector3(0, 0, rotate);
                    }
                }
            }
        }
        else
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    ms.circ.transform.localEulerAngles = new Vector3(0, 0, rotate);
                }
            }

    }
    [ClientRpc]
    void Rpc_ARSelectionShape()
    {
        if (setup.myname == "ArClient")
        {
            GameObject info = GameObject.Find("Info");
            info.GetComponent<InfoNetwork>().startSelection = true;
            //Debug.Log("Rpc_ARSelectionShape");
        }
    }

    [ClientRpc]
    void Rpc_setScale()
    {
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            ms.circ.setInitialScale(ms.circ.transform.localScale.x);
        }
        if (setup.myname == "ArClient")
        {
            //  Debug.Log("Rpc_setScale");
        }
    }
    [ClientRpc]
    void Rpc_resetSelectionShape()
    {
        if (setup.myname != "ArClient")
        {
            GameObject lineSelection = GameObject.Find("LineSelection");
            lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
            lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));

            lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
            lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));

            lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
            lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));

            lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
            lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));
        }
    }

    [ClientRpc]
    void Rpc_ChangeColor(Color c, int i)
    {

        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.getID() == i)
            {
                ms.circ.ChangeColor(c);
            }
        }
    }

    [ClientRpc]
    void Rpc_ChangeColorBorderPreview(Color c, int i, bool preview)
    {
        if (setup.myname == "ArClient" && preview)
        {
            //  Debug.Log("Rpc_ChangeColorBorder");
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    if (c == Color.white)
                    {
                        //ms.circ.transform.gameObject.SetActive(false);
                        ms.circ.unSelectShape();
                    }
                    else if (c == Color.green)
                    {
                        //ms.circ.transform.gameObject.SetActive(true);
                        ms.circ.selectShape();
                    }
                    c.a = 0.75f;
                    ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(c);
                }
            }
        }
    }

    [ClientRpc]
    void Rpc_ChangeColorBorder(Color c, int i)
    {
        if (setup.myname != "ArClient")
        {
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(c);
                }
            }
        }

        if (setup.myname == "ArClient" && c != Color.blue)
        {
            //  Debug.Log("Rpc_ChangeColorBorder");
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    if (c == Color.white)
                    {
                        //ms.circ.transform.gameObject.SetActive(false);
                        ms.circ.unSelectShape();
                    }
                    else if (c == Color.green)
                    {
                        //ms.circ.transform.gameObject.SetActive(true);
                        ms.circ.selectShape();
                    }
                    c.a = 0.75f;
                    ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(c);
                }
            }
        }
    }

    [ClientRpc]
    void Rpc_InSelectionShape(Color c, int i, float xshape, float yshape)
    {
        if (setup.myname == "ArClient")
        {
            // Debug.Log("Rpc_InSelectionShape");
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    // ms.circ.transform.gameObject.SetActive(true);
                    //ms.circ.selectShape();
                    //ms.circ.onMiniWall = true;

                    // Debug.Log(" x "+ xshape + " y  " + yshape);
                    ms.circ.xshape = xshape;
                    ms.circ.yshape = yshape;
                    //rendering.toMiniWall = true;
                    Debug.Log("Rpc_InSelectionShape" + xshape + " " + yshape);
                }
            }
        }
    }

    [ClientRpc]
    void Rpc_selectShape(Color c, int i)
    {
        if (setup.myname != "ArClient")
        {
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(c);
                }
            }
        }

        if (setup.myname == "ArClient")
        {
            //  Debug.Log("Rpc_selectShape");
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    c.a = 0.75f;
                    ms.circ.transform.GetChild(0).GetComponent<Circle>().ChangeColor(c);
                }
            }
        }
    }

    [ClientRpc]
    void Rpc_msg(string s)
    {
        if (setup.myname == "ArClient")
        {
            Debug.Log(s);
        }
    }

    [ClientRpc]
    void Rpc_SelectionShapeToMiniWall(int i)
    {
        if (setup.myname == "ArClient")
        {
            // Debug.Log("Rpc_SelectionShapeToMiniWall");
            foreach (Rendering.mShape ms in rendering.ListShapes)
            {
                if (ms.circ.getID() == i)
                {
                    ms.circ.onMiniWall = true;
                    //rendering.toMiniWall = true;
                    //Debug.Log("to mini wall");
                }
            }
        }
    }

    [ClientRpc]
    void Rpc_MoveshapePreview(int i, float x, float y, float posxMiniwall, float posyMiniWall, bool preview)
    {
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.getID() == i)
            {
                if (setup.myname == "ArClient" && preview)
                {
                    if (!ms.circ.onMiniWall)
                    {
                        Vector3 pos = new Vector3(((x - 0.5f) * 5.77f), -((y - 0.5f) * 1.685f), -0.002f);
                        ms.circ.transform.localPosition = pos;
                    }

                    else
                    {
                        Vector3 pos = new Vector3(posxMiniwall, posyMiniWall, -0.002f);
                        ms.circ.newposMiniwall = pos;
                        ms.circ.newpos = true;
                    }
                }

            }
        }
    }
    [ClientRpc]
    void Rpc_Moveshape(int i, float x, float y, float posxMiniwall, float posyMiniWall)
    {
        foreach (Rendering.mShape ms in rendering.ListShapes)
        {
            if (ms.circ.getID() == i)
            {
                if (setup.myname != "ArClient")
                {
                    float sw = wall.getWallWidth();
                    float sh = wall.getWallHeight();
                    float screenRatio = sh / sh;
                    float shnobezel = 8640;
                    float pixToUnit = (float)wall.getGridRow() * (float)Camera.main.orthographicSize / (shnobezel / 2.0f);
                    x = (x - 0.5f) * sw * pixToUnit;
                    y = -(y - 0.5f) * sh * pixToUnit;
                    Vector3 pos = new Vector3(x, y, -0.002f * ms.circ.getID());
                    ms.circ.transform.position = pos;
                }
                else
                {
                    if (!ms.circ.onMiniWall)
                    {
                        Vector3 pos = new Vector3(((x - 0.5f) * 5.77f), -((y - 0.5f) * 1.685f), -0.002f);
                        ms.circ.transform.localPosition = pos;
                    }

                    else
                    {
                        Vector3 pos = new Vector3(posxMiniwall, posyMiniWall, -0.002f);
                        ms.circ.newposMiniwall = pos;
                        ms.circ.newpos = true;
                        Debug.Log(pos);
                        //move in the miniwall
                        //rendering.syncroPosMiniwall = true;
                    }
                }

            }
        }

        if (setup.myname == "ArClient")
        {
            // Debug.Log("Rpc_Moveshape");
        }

    }

    [ClientRpc]
    void Rpc_Selectionshape(float x, float y, float x2, float y2)
    {
        if (setup.myname != "ArClient")
        {
            GameObject lineSelection = GameObject.Find("LineSelection");

            float sw = wall.getWallWidth();
            float sh = wall.getWallHeight();
            float screenRatio = sh / sh;
            float shnobezel = 8640;
            float pixToUnit = (float)wall.getGridRow() * (float)Camera.main.orthographicSize / (shnobezel / 2.0f);
            x = (x - 0.5f) * sw * pixToUnit;
            y = -(y - 0.5f) * sh * pixToUnit;

            x2 = (x2 - 0.5f) * sw * pixToUnit;
            y2 = -(y2 - 0.5f) * sh * pixToUnit;

            lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(0, new Vector3(x, y, 0));
            lineSelection.transform.GetChild(0).GetComponent<LineRenderer>().SetPosition(1, new Vector3(x2, y, 0));

            lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(0, new Vector3(x2, y, 0));
            lineSelection.transform.GetChild(1).GetComponent<LineRenderer>().SetPosition(1, new Vector3(x2, y2, 0));

            lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(0, new Vector3(x2, y2, 0));
            lineSelection.transform.GetChild(2).GetComponent<LineRenderer>().SetPosition(1, new Vector3(x, y2, 0));

            lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(0, new Vector3(x, y2, 0));
            lineSelection.transform.GetChild(3).GetComponent<LineRenderer>().SetPosition(1, new Vector3(x, y, 0));
            //Debug.Log("Rpc_Selectionshape");
        }
        else
        {
            //Debug.Log("Rpc_Selectionshape");
        }
    }

    Vector2 moveTimerCursor(float x, float y)
    {
        if (wall != null)
        {
            float sw = wall.getWallWidth();
            float sh = wall.getWallHeight();
            float screenRatio = sh / sh;
            float shnobezel = 8640;
            float pixToUnit = (float)wall.getGridRow() * (float)Camera.main.orthographicSize / (shnobezel / 2.0f);
            x = (x - 0.5f) * sw * pixToUnit;
            y = -(y - 0.5f) * sh * pixToUnit;
            Vector2 pos = new Vector2(x, y);
            return pos;
        }
        else
        {
            return new Vector2(0, 0);
        }
    }

    Vector2 screenTorelatif(float x, float y)
    {
        float sw = Screen.width;
        float sh = Screen.height; // sw / sh == wall.getWallWidth() / wall.getWallHeight()
        float pixToUnit = Camera.main.orthographicSize / (sh / 2.0f);
        x = (x / sw / pixToUnit) + 0.5f;
        y = -(y / sh / pixToUnit) + 0.5f;

        return new Vector2(x, y);
    }
    Vector2 relatifToscreen(float x, float y)
    {
        float sw = Screen.width;
        float sh = Screen.height; // sw / sh == wall.getWallWidth() / wall.getWallHeight()
        float pixToUnit = Camera.main.orthographicSize / (sh / 2.0f);

        x = (x - 0.5f) * sw * pixToUnit;
        y = -(y - 0.5f) * sh * pixToUnit;

        return new Vector2(x, y);
    }
}