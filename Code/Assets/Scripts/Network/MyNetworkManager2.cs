using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



public class MyNetworkManager2 : NetworkManager
{ 
    public enum NetworkDevice
    {
        SERVER,
        WALL,
        VR
    };

    public NetworkDevice networkDevice;
    public GameObject ServerCommand;

    private void Start()
    {
        if(this.networkDevice == NetworkDevice.SERVER)
        {
            NetworkClient nc  = StartHost();
            nc.RegisterHandler(MsgType.Connect, OnConnected);

          //  GameObject g = Instantiate(ServerCommand);
          //  NetworkServer.Spawn(g);
        }
        else
        {
            StartClient();
        }
        
    }
    // Server callbacks
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        var player = (GameObject)GameObject.Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        Debug.Log("Client has requested to get his player added to the game");
    }

    public void OnConnected(UnityEngine.Networking.NetworkMessage msg)
    {
        Debug.Log("New player " + msg);
    }
}
