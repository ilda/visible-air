using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR;
using Varjo.XR;

public class CustomController : MonoBehaviour
{
    [Header("Select hand")]
    public XRNode XRNode = XRNode.LeftHand;

    [Header("Controllers parts")]
    public GameObject controller;
    public GameObject bodyGameobject;
    public GameObject touchPadGameobject;
    public GameObject touchPadPosGameobject;
    public GameObject menubuttonGameobject;
    public GameObject triggerGameobject;
    public GameObject systemButtonGameobject;
    public GameObject gripButtonGameobject;

    [Header("Controller material")]
    public Material controllerMaterial;

    [Header("Controller button highlight material")]
    public Material buttonPressedMaterial;
    public Material touchpadTouchedMaterial;


    public Material helpTrigger;

    [Header("Visible only for debugging")]
    public bool triggerButton;
    public bool gripButton;
    public bool primary2DAxisTouch;
    public bool primary2DAxisClick;
    public bool primaryButton;
    public float trigger;
    public bool help;

    public GameObject miniControllerViewTrigger;

    private List<InputDevice> devices = new List<InputDevice>();
    private InputDevice device;

    private Quaternion deviceRotation; //Controller rotation
    private Vector3 devicePosition; //Controller position
    private Vector3 deviceAngularVelocity; // Controller angular velocity
    private Vector3 deviceVelocity; // Controller velocity
    private Vector3 triggerRotation; // Controller trigger rotation
    private Vector2 trackpad2DPosition;

    public bool TriggerButton { get { return triggerButton; } set { } }

    public bool GripButton { get { return gripButton; } set { } }

    public bool Primary2DAxisTouch { get { return primary2DAxisTouch; } set { } }

    public bool Primary2DAxisClick { get { return primary2DAxisClick; } set { } }

    public bool PrimaryButton { get { return primaryButton; } set { } }

    public Vector3 DeviceVelocity { get { return deviceVelocity; } }

    public Vector3 DeviceAngularVelocity { get { return deviceAngularVelocity; } }

    public float Trigger { get { return trigger; } set { } }

    public Vector2 TrackPadPosition { get { return trackpad2DPosition; } set { } }

    void OnEnable()
    {
        if (!device.isValid)
        {
            GetDevice();
        }
    }

    void Update()
    {
        if (!device.isValid)
        {
            GetDevice();
        }


        device.TryGetFeatureValue(CommonUsages.primary2DAxis, out trackpad2DPosition);

        // Get values for device position, rotation and buttons.
        if (device.TryGetFeatureValue(CommonUsages.devicePosition, out devicePosition))
        {
            transform.localPosition = devicePosition;
        }

        if (device.TryGetFeatureValue(CommonUsages.deviceRotation, out deviceRotation))
        {
            transform.localRotation = deviceRotation;
        }

        if (device.TryGetFeatureValue(CommonUsages.trigger, out trigger))
        {
            ControllerInput();
        }

        if (device.TryGetFeatureValue(CommonUsages.triggerButton, out triggerButton))
        {
            ControllerInput();
        }

        if (device.TryGetFeatureValue(CommonUsages.gripButton, out gripButton))
        {
            ControllerInput();
        }

        if (device.TryGetFeatureValue(CommonUsages.primary2DAxisTouch, out primary2DAxisTouch))
        {
            ControllerInput();
        }

        if (device.TryGetFeatureValue(CommonUsages.primary2DAxisClick, out primary2DAxisClick))
        {
            ControllerInput();
        }

        if (device.TryGetFeatureValue(CommonUsages.primaryButton, out primaryButton))
        {
            ControllerInput();
        }

        device.TryGetFeatureValue(CommonUsages.deviceAngularVelocity, out deviceAngularVelocity);

        device.TryGetFeatureValue(CommonUsages.deviceVelocity, out deviceVelocity);
    }

    void GetDevice()
    {
        InputDevices.GetDevicesAtXRNode(XRNode, devices);
        device = devices.FirstOrDefault();
    }

    void ControllerInput()
    {
        //Set trigger rotation from input
        triggerRotation.Set(trigger * -30f, 0, 0);
        triggerGameobject.transform.localRotation = Quaternion.Euler(triggerRotation);

        //Set controller button inputs
        if(help)
        {
            triggerGameobject.GetComponent<MeshRenderer>().material = helpTrigger;
            miniControllerViewTrigger.GetComponent<MeshRenderer>().material = helpTrigger;
        }
        else if (!triggerButton)
        {
            triggerGameobject.GetComponent<MeshRenderer>().material = controllerMaterial;
            miniControllerViewTrigger.GetComponent<MeshRenderer>().material = controllerMaterial;
        }
        else
        {
            triggerGameobject.GetComponent<MeshRenderer>().material = buttonPressedMaterial;
            miniControllerViewTrigger.GetComponent<MeshRenderer>().material = buttonPressedMaterial;
        }

        if (!gripButton)
        {
            gripButtonGameobject.GetComponent<MeshRenderer>().material = controllerMaterial;
        }
        else
        {
            gripButtonGameobject.GetComponent<MeshRenderer>().material = buttonPressedMaterial;
        }

        if (!primary2DAxisTouch)
        {
            touchPadGameobject.GetComponent<MeshRenderer>().material = controllerMaterial;
           // touchPadPosGameobject.SetActive(false);
        }
        else if (primary2DAxisTouch && primary2DAxisClick)
        {
            touchPadGameobject.GetComponent<MeshRenderer>().material = buttonPressedMaterial;
        }
        else if (primary2DAxisTouch)
        {
            //touchPadGameobject.GetComponent<MeshRenderer>().material = buttonPressedMaterial;
             touchPadGameobject.GetComponent<MeshRenderer>().material = controllerMaterial;
             touchPadPosGameobject.SetActive(true);
             touchPadPosGameobject.transform.localPosition = new Vector3(-trackpad2DPosition.x, 0.01f, -trackpad2DPosition.y) * 0.02f;
        }

        if (!primaryButton)
        {
            menubuttonGameobject.GetComponent<MeshRenderer>().material = controllerMaterial;
        }
        else
        {
            menubuttonGameobject.GetComponent<MeshRenderer>().material = buttonPressedMaterial;
        }
    }
}
