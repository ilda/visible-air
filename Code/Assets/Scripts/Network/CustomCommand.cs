using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomCommand : NetworkBehaviour
{
    private GameObject info;
    private GameObject RecivedInfo;
    private Vector3 CoordCursorOnWall;
    private InputManager im;
    Setup setup;
    bool deviceAR;

    // Start is called before the first frame update
    void Start()
    {
        info = GameObject.Find("Info");
        RecivedInfo = GameObject.Find("RecivedInfo");
        im = GameObject.Find("Server(Clone)").GetComponent<InputManager>();
        setup = GameObject.Find("NetworkManager").GetComponent<Setup>();

        if (setup.myname == "ArClient")
        {
            deviceAR = true;
            Cmd_CreateCursor();
            Debug.Log("create cursor");
        }
        else
        {
            deviceAR = false;
        }
    }

    public override void OnStartAuthority()
    {
       // Debug.Log(this.name + " has Authority " + this.hasAuthority);
    }

    public override void OnStartClient()
    {
        //Debug.Log(this.name + " startClient ");
    }

    // Update is called once per frame
    void Update()
    {
        if (RecivedInfo == null)
        {
            RecivedInfo = GameObject.Find("RecivedInfo");
        }
        
        if (deviceAR && hasAuthority)
        {
            Vector3 coord = info.GetComponent<InfoNetwork>().coordCursorOnWall;
            Cmd_CoordCursorOnWall(coord);
            //Debug.Log(" Coord : " + coord);

            if (info.GetComponent<InfoNetwork>().pressed && deviceAR)
            {
                Cmd_pressed();
                info.GetComponent<InfoNetwork>().pressed = false;
            }

            if (info.GetComponent<InfoNetwork>().released && deviceAR)
            {
                Cmd_release();
                info.GetComponent<InfoNetwork>().released = false;
            }

            if (info.GetComponent<InfoNetwork>().next && deviceAR)
            {
                Cmd_next();
                info.GetComponent<InfoNetwork>().next = false;
            }

            if (info.GetComponent<InfoNetwork>().previous && deviceAR)
            {
                Cmd_previous();
                info.GetComponent<InfoNetwork>().previous = false;
            }
            if (info.GetComponent<InfoNetwork>().resize && deviceAR)
            {
                Cmd_Resize(info.GetComponent<InfoNetwork>().resizeValue);
                info.GetComponent<InfoNetwork>().resize = false;
            } 
            if (info.GetComponent<InfoNetwork>().rotate && deviceAR)
            {
                Cmd_Rotate(info.GetComponent<InfoNetwork>().rotatevalue);
                info.GetComponent<InfoNetwork>().rotate = false;
            }  
            if (info.GetComponent<InfoNetwork>().waitresize && deviceAR)
            {
                Cmd_waitResize();
                info.GetComponent<InfoNetwork>().waitresize = false;
            } 
            if (info.GetComponent<InfoNetwork>().waitrotate && deviceAR)
            {
                Cmd_waitRotate();
                info.GetComponent<InfoNetwork>().waitrotate = false;
            }

            if (info.GetComponent<InfoNetwork>().waitClunch && deviceAR)
            {
                Cmd_WaitClunch();
                info.GetComponent<InfoNetwork>().waitClunch = false;
            }

            if (info.GetComponent<InfoNetwork>().clunch && deviceAR)
            {
                Cmd_Clunch();
                info.GetComponent<InfoNetwork>().clunch = false;
            }

            if (info.GetComponent<InfoNetwork>().toMiniWall && deviceAR)
            {
                Cmd_ToMiniWall();
                info.GetComponent<InfoNetwork>().toMiniWall = false;
            } 
            if (info.GetComponent<InfoNetwork>().translateOneShape && deviceAR)
            {
                for( int i = 0; i < info.GetComponent<InfoNetwork>().shapeId.Count; i++)
                {
                    Cmd_TranslateOneShape(info.GetComponent<InfoNetwork>().shapeId[i], info.GetComponent<InfoNetwork>().translateOneShapevalue[i]);
                    Cmd_RotateOneShape(info.GetComponent<InfoNetwork>().shapeId[i], info.GetComponent<InfoNetwork>().rotateOneShapevalue[i]);
                }
                info.GetComponent<InfoNetwork>().translateOneShape = false;
                info.GetComponent<InfoNetwork>().shapeId.Clear();
                info.GetComponent<InfoNetwork>().translateOneShapevalue.Clear();
            }

            if (info.GetComponent<InfoNetwork>().toWall && deviceAR)
            {
                Cmd_ToWall();
                info.GetComponent<InfoNetwork>().toWall = false;
            }

        }

        if (RecivedInfo != null && im != null)
        {
            RecivedInfo.GetComponent<RecivedInfoNetwork>().coordCursorOnWall = CoordCursorOnWall;
            im.MoveCursor(this, 1, CoordCursorOnWall.x, CoordCursorOnWall.y);
        }

        if (isServer && RecivedInfo.GetComponent<RecivedInfoNetwork>().moveTarget)
        {
            Vector3 posTarget = RecivedInfo.GetComponent<RecivedInfoNetwork>().coordTargetOnWall;
            Rpc_MoveTarget(posTarget);
            RecivedInfo.GetComponent<RecivedInfoNetwork>().moveTarget = false;
            Debug.Log("Rpc move target");
            Debug.Log(posTarget);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Cmd_Test();
        }
    }


    [Command]
    public void Cmd_ToWall()
    {
        im.toWall(this, 1);
    }

    [Command]
    public void Cmd_TranslateOneShape(int shapeID, Vector3 value)
    {
        im.tranlateOnshape(this, 1, shapeID, value);
    }

    [Command]
    public void Cmd_RotateOneShape(int shapeID, float value)
    {
        im.rotateOnshape(this, 1, shapeID, value);
    }

    [Command]
    public void Cmd_ToMiniWall()
    {
        im.toMiniWall(this, 1);
    }

    [Command]
    public void Cmd_WaitClunch()
    {
        im.waitClunch(this, 1);
    }

    [Command]
    public void Cmd_Clunch()
    {
        im.Clunch(this, 1);
    }

    [Command]
    public void Cmd_Resize(float resize)
    {
        im.Resize(this, 1, resize);
        //Debug.Log("cmd resize");
    } 
    [Command]
    public void Cmd_waitResize()
    {
        im.waitResize(this, 1);
        //Debug.Log("cmd resize");
    }  
    [Command]
    public void Cmd_waitRotate()
    {
        im.waitRotate(this, 1);
        //Debug.Log("cmd resize");
    }  
    [Command]
    public void Cmd_Rotate(float rotate)
    {
        im.Rotate(this, 1, rotate);
    }

    [Command]
    public void Cmd_CreateCursor()
    {
        im.RegisterDevice("VRClient", this);
        im.CreateCursor(this, 1, 0.4f, 0.5f, Color.cyan);
 
    }
    [Command]
    public void Cmd_Test()
    {
        Debug.Log("Command received");
        Rpc_Test();
    }

    [ClientRpc]
    public void Rpc_Test()
    {
        Debug.Log("Rpc received");
    }

    [Command]
    public void Cmd_CoordCursorOnWall(Vector3 coord)
    {
       // Debug.Log("recive coord" + this.isServer);
        CoordCursorOnWall = coord;
    }

    [ClientRpc]
    public void Rpc_changePoseCursor()
    {
        //Debug.Log("recived postition cursor ");
    }

    [Command]
    public void Cmd_StartDrag()
    {
            im.StartMoveCursor(this, 1, CoordCursorOnWall.x, CoordCursorOnWall.y, true);
    }

    [Command]
    public void Cmd_pressed()
    {
        im.PressedCursor(this, 1, CoordCursorOnWall.x, CoordCursorOnWall.y);
        im.StartMoveCursor(this, 1, CoordCursorOnWall.x, CoordCursorOnWall.y, false);
        //StartMoveCursor(this, 1, CoordCursorOnWall.x, CoordCursorOnWall.y, true);
    }

    [Command]
    public void Cmd_release()
    {
          //  Debug.Log("Command received");
            im.ClickCursor(this, 1, CoordCursorOnWall.x, CoordCursorOnWall.y);
            im.EndMoveCursor(this, 1, CoordCursorOnWall.x, CoordCursorOnWall.y);
    }

    [Command]
    public void Cmd_next()
    {
        im.nextSelectionCursor(this, 1);
    }

    [Command]
    public void Cmd_previous()
    {
        //  Debug.Log("Command received");
        im.previousSelectionCursor(this, 1);
    }

    [ClientRpc]
    public void Rpc_MoveTarget(Vector3 pos)
    {
        //Debug.Log(pos);
        if (setup.myname == "ArClient")
        {
            info.GetComponent<InfoNetwork>().coordTarget = pos;
            info.GetComponent<InfoNetwork>().moveTarget = true;
            Debug.Log("move pos target" + info.GetComponent<InfoNetwork>().coordTarget);
            
        }
    }
}
