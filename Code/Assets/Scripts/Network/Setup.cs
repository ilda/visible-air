using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Setup :  MonoBehaviour
{
	public string myname = "server";
	public bool is_server = true;
	int port = 7777;
	string ip="127.0.0.1";
	
	public bool fs = false;

	// server window
    public float width = 1024; // size of the window
    public float height = 512;
    public float x = 0; // position in the scene
    public float y = 0;
    private float wx = 0; // position of the window in its screen
    private float wy = 0;  // FIXME unable to use this !!!!
    public float pixsizemm = 0.4097222f;  // the server do not care that much
    // wilder 0.4097222f
    // opale 0.1791667f

    // FOR WILDEST ....
    public bool xinerama = false;
	public bool bezel = false;
    private bool do_draw_under_xinerama_bezel = false;

    public string wall = "WILDEST";
    public Wall wallC;

	public bool smarties = true;
    public bool touch = true;
	// public List<string> vobjects = new List<string>(new string[] {"WildFabTwo", "HatTwo"});
    // public string viconHost = "192.168.2.3"; // wilder

    public int num_clients = 0;

    // for an expe ...
    public bool expeStrict = false;
    public string participant = "P01";
    public int startTrial = 1;

    public GameObject ServerCommand;

    void Awake() {
		Debug.Log("Setup !");
		string[] args = System.Environment.GetCommandLineArgs ();
        Debug.Log(myname);

        if(myname =="server")
        {
            is_server = true;
        }
        else
        {
            is_server = false;
        }
        for (int i = 0; i < args.Length; i++) {
         	//Debug.Log ("ARG " + i + ": " + args [i]);
         	if (args [i] == "-s") {
         		is_server = false;
         		i++;
         		//if (i < args.Length){
             		ip = args [i];
             		Debug.Log ("IP ARG " + i + ": " + args [i]+ " " + ip);
                //}
                
             	if (myname == "server"){
             		myname = "a client";
                }
             	smarties = false;
             	// vobjects = null;
                touch = false;
         	}
            else if (args [i] == "-f" || args [i] == "-fs") {
         		fs = true;
            }
            else if (args [i] == "-w" || args [i] == "-screen-width") {
         		width = int.Parse(args [i + 1]);
         		i++;
            }
            else if (args [i] == "-h"|| args [i] == "-screen-height") {
         		height = int.Parse(args [i + 1]);
         		i++;
            }
            else if (args [i] == "-x") {
         		x = int.Parse(args [i + 1]);
         		i++;
            }
            else if (args [i] == "-y") {
         		y = int.Parse(args [i + 1]);
         		i++;
            }
            else if (args [i] == "-wx") {
                wx = int.Parse(args [i + 1]);
                i++;
            }
            else if (args [i] == "-wy") {
                wy = int.Parse(args [i + 1]);
                i++;
            }
            else if (args [i] == "-nc") {
                num_clients = int.Parse(args [i + 1]);
                i++;
            }
            else if (args [i] == "-nosmarties") {
         		smarties = false;
            }
            else if (args [i] == "-novicon") {
         		// vobjects = null;
            }
            else if (args [i] == "-notouch") {
                touch = false;
            }
            else if (args [i] == "-e") {
                expeStrict = true;
                touch = false;
            }
            else if (args [i] == "-p") {
                participant = args [i + 1];
                i++;
            }
            else if (args [i] == "-t") {
                startTrial = int.Parse(args [i + 1]);
                i++;
            }
            else if (args [i] == "-wall") {
                wall = args [i + 1];
                i++;
            }
            else if (args [i] == "-xine" || args [i] == "-xinerama") {
                xinerama = true;
            }
            else if (args [i] == "-bezel") {
                bezel = true;
            }
            else if (args [i] == "-xinebezel") {
                bezel = true;
                xinerama = true;
            }
     	}

        if (wall == "WILD"){
            wallC = new WallWild();
            touch = false;
            if (is_server){
                // todo 
                // vobjects = new List<string>(new string[] {"WildFabTwo", "HatTwo"});
            }
        }
        else if (wall == "WILDER"){
            wallC = new WallWilder();
            touch = false;
            if (is_server){
                // vobjects = new List<string>(new string[] {"WildFabTwo", "HatTwo"});
            }
        }
        else if (wall == "WILDEST"){
            wallC = new WallWildest(bezel, xinerama);
            if (bezel && xinerama && !is_server){
                do_draw_under_xinerama_bezel = true;
            }
            touch = false;
            if (is_server){
                // vobjects = new List<string>(new string[] {"HatOne"});
            }
        }
        else if (wall == "DESKTOP"){ // for testing ....
            wallC = new WallDesktop();
            touch = false;
            if (bezel && xinerama){
                do_draw_under_xinerama_bezel = true;
            }
            Debug.Log("DESKTOP WALL");
            if (is_server){
                //vobjects = new List<string>(new string[] {"PointerGreen","PointerOrangeN"});
                //vobjects = new List<string>(new string[] {"WildFabTwo", "HatTwo"});
                //vobjects = null;
            }
        }

     	if (fs){
     		Screen.fullScreen = true;
     		width = Screen.width;
     		height = Screen.height;
     	} else {
     		Screen.fullScreen = false;
     		Screen.SetResolution((int)width, (int)height, false);
     	}

     	Debug.Log("ARG PARSED ! " + wall);
     }

     void Start(){
     	
        if (is_server){
     		//Network.maxConnections = 100;
     		//Debug.Log ("Server start to listen on "+port);
     		//MyNetworkManager.singleton.networkPort = port;
            MyNetworkManager.singleton.StartServer();

            GameObject g = Instantiate(ServerCommand);
            NetworkServer.Spawn(g);
        }
     	else {
     		//Debug.Log ("client try server connection "+ip+":"+port);
     		MyNetworkManager.singleton.networkAddress = ip;
            //MyNetworkManager.singleton.networkPort = port;
           	MyNetworkManager.singleton.StartClient();
     	}
       
	}

    public bool DoDrawUnderXineramaBezel(){
        return do_draw_under_xinerama_bezel;
    }

	// get that on the server when a clien connect ...
    public void OnConnected(NetworkMessage netMsg)
    {
        Debug.Log("Connected to server");
    }
}